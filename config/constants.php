<?php
return [
    'title' => [
        'home_page' => 'EDUTECH - TRƯỜNG HỌC TRỰC TUYẾN SỐ 1 VIỆT NAM',
        'introduce_page' => 'Giới thiệu',
        'contact_page' => 'Liên hệ',
        'register_page' => 'Đăng ký',
        'login_page' => 'Đăng nhập',
        'account_page' => 'Tài khoản',
        'transfer_page' => 'Chuyển thẻ',
        'list_level_page' => 'Danh sách các khóa học',
        'personnel_page' => 'Nhân sự và tổ chức',
        'news_page' => 'Tin tức'       
    ],
    'data' => [
        'default_number_of_courses' => 6,
        'link_create_user' => 'https://edutech.org.vn/api/create-user',
        'link_call_back' => 'https://edutech.org.vn/account',
        'oherecore' => 'http://i.ohere.me',        
        'link_change_password' => 'https://edutech.org.vn/api/change-password',                  
    ],
    'order_status' => [
        'transfer_unpaid' => 'transfer_unpaid',
        'transfer_paid' => 'transfer_paid',
        'active_status' => 'active',        
    ],
    'lesson_status' => [
        'approved' => 'approved',
        'pending' => 'pending',
        'reject' => 'reject',
    ]
];