<?php

use Illuminate\Database\Seeder;

class GeneralPageConfigTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('general_page_config')->delete();
        
        \DB::table('general_page_config')->insert(array (
            0 => 
            array (
                'id' => 1,
                'page_id' => 32,
                'title' => 'Giới thiệu về chúng tôi',
                'content' => '<section class="flat-row pd-top-93">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="title-section color-title">
<h1 class="title">TH&Ocirc;NG TIN CHUNG VỀ VIỆN SEAFIT</h1>
</div>
</div>
<!-- col-md-12 --></div>
<!-- row --></div>
<!-- container -->
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="about-us style2 wrap-box pdleft">
<div class="text-about-us">
<p><strong>VIỆN NGHI&Ecirc;N CỨU T&Agrave;I CH&Iacute;NH, ĐẦU TƯ V&Agrave; HỢP T&Aacute;C , THƯƠNG MẠI Đ&Ocirc;NG NAM &Aacute; </strong>hay c&oacute; t&ecirc;n tiếng Anh l&agrave; <strong>Southeast Asian Finance, Investment and Cooper Trade</strong>. <strong>Research Institute</strong> c&oacute; t&ecirc;n viết tắt l&agrave; <strong>SEAFIT</strong> - th&agrave;nh lập v&agrave;o năm 2002. Trải qua nhiều năm hoạt đ&ocirc;ng th&igrave; trung t&acirc;m c&oacute; 3 trụ sở ch&iacute;nh: Căn hộ 4 tầng &ndash; Khu đ&ocirc; thị Nam An Kh&aacute;nh (An Thọ &ndash; An Kh&aacute;nh &ndash; Ho&agrave;i Đức &ndash; H&agrave; Nội). V&agrave; c&oacute; 4 trụ sở giao dịch:</p>
<p>&nbsp; <strong>- Văn ph&ograve;ng số 1 khu vực ph&iacute;a Bắc:</strong> Nh&agrave; số 15C &ndash; Khu tập thể Học viện H&agrave;nh ch&iacute;nh Quốc gia, Ng&otilde; 193 &ndash; Đường Trung K&iacute;nh &ndash; Phường Trung H&ograve;a &ndash; Quận Cầu Giấy &ndash; TP H&agrave; Nội.</p>
<p>&nbsp; <strong>- Văn ph&ograve;ng số 2 khu vực ph&iacute;a Bắc:</strong> Nh&agrave; số 60 &ndash; Ng&otilde; 158 &ndash; Đường Phan B&aacute; V&agrave;nh &ndash; Quận Bắc Từ Li&ecirc;m &ndash; TP H&agrave; Nội.</p>
<p>&nbsp; <strong>- Văn ph&ograve;ng số 3 khu v ực ph&iacute;a Bắc:</strong> Khu Trung t&acirc;m văn ho&aacute; thể thao &ndash; số 2, đường Đức Diễn, phường Ph&uacute;c Diễn, quận Bắc Từ Li&ecirc;m, TP H&agrave; Nội.</p>
<p>&nbsp; <strong>- Ph&acirc;n viện khu vực Miền Trung:</strong> Th&agrave;nh lập năm 2003 Trụ sở văn ph&ograve;ng đặt tại Phường Thạch Thang &ndash; Quận Hải Ch&acirc;u &ndash; TP Đ&agrave; Nẵng.</p>
<p>&nbsp; <strong>- Ph&acirc;n viện khu vực T&acirc;y Bắc:</strong> Th&agrave;nh lập năm 2019 Trụ sở văn ph&ograve;ng đặt tại TP.Y&ecirc;n B&aacute;i, tỉnh Y&ecirc;n B&aacute;i.</p>
<p>&nbsp; <strong>- Ph&acirc;n viện khu vực Miền Nam:</strong> Th&agrave;nh lập năm 2018 Trụ sở văn ph&ograve;ng đặt tại TP.Hồ Ch&iacute; Minh.</p>
<p>&nbsp; <strong>- Ph&acirc;n viện khu vực T&acirc;y Nguy&ecirc;n:</strong> Th&agrave;nh lập năm 2019 Trụ sở đặt tại TP.Bu&ocirc;n M&ecirc; Thuột.</p>
<p><strong>Điện thoại:</strong> 024.2023.5566</p>
<p><strong>E-mail:</strong> seafit.edu@gmail.com</p>
<p><strong>Website:</strong> www.seafit.org.vn</p>
<p><strong>Đặc san Khoa học:</strong> T&agrave;i ch&iacute;nh &ndash; Đầu tư Đ&ocirc;ng Nam &Aacute;. Giấy ph&eacute;p xuất bản số 70/GP-XBĐS của Cục B&aacute;o ch&iacute; Bộ Th&ocirc;ng tin v&agrave; Truyền th&ocirc;ng.</p>
<p><strong>Trang tin điện tử:</strong> Ph&aacute;p luật T&agrave;i ch&iacute;nh v&agrave; Đầu tư. Giấy ph&eacute;p số 251 /GP- TTĐT của Cục ph&aacute;t thanh, Truyền h&igrave;nh v&agrave; Th&ocirc;ng tin điện tử - Bộ Th&ocirc;ng tin v&agrave; Truyền th&ocirc;ng.</p>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="flat-row">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="title-section color-title">
<h1 class="title">Cơ cấu tổ chức</h1>
</div>
</div>
<!-- col-md-12 --></div>
<!-- row --></div>
<!-- container -->
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="about-us style2 wrap-box pdleft">
<div class="text-about-us">
<p><strong>1.L&atilde;nh đạo Viện v&agrave; c&aacute;c Hội đồng:</strong></p>
<p>&nbsp; - Hội đồng Viện (Chủ tịch v&agrave; c&aacute;c th&agrave;nh vi&ecirc;n Hội đồng Viện)</p>
<p>&nbsp; - Ban L&atilde;nh đạo Viện (Viện trưởng, c&aacute;c ph&oacute; viện trưởng)</p>
<p>&nbsp; - Hội đồng khoa học (Chủ tịch, ph&oacute; chủ tịch HĐKH, Tổng thư k&yacute;, c&aacute;c th&agrave;nh vi&ecirc;n Hội đồng khoa học của Viện)</p>
<p><strong>2.Khối Văn ph&ograve;ng:</strong></p>
<p>&nbsp; - Ph&ograve;ng Tổ chức &ndash; H&agrave;nh ch&iacute;nh &ndash; ph&aacute;p chế.</p>
<p>&nbsp; - Ph&ograve;ng Kế hoạch - T&agrave;i ch&iacute;nh.</p>
<p>&nbsp; - Ph&ograve;ng Đ&agrave;o tạo, Quản l&yacute; Khoa học v&agrave; Hợp t&aacute;c quốc tế.</p>
<p><strong>3 Khối đơn vị thuộc Viện:</strong></p>
<p>&nbsp; - Ban quản l&yacute; Chương tr&igrave;nh v&agrave; Dự &aacute;n (Dự &aacute;n EDUTECH).</p>
<p>&nbsp; - Trung t&acirc;m Đ&agrave;o tạo, Ph&aacute;t triển v&agrave; Cung ứng nh&acirc;n lực quốc tế.</p>
<p>&nbsp; - Trung t&acirc;m Đ&agrave;o tạo v&agrave; tư vấn T&agrave;i ch&iacute;nh &ndash; Kế to&aacute;n &ndash; Thuế.</p>
<p>&nbsp; - Trung t&acirc;m Thực nghiệm Ứng dụng C&ocirc;ng nghệ sinh học Lebio.</p>
<p>&nbsp; - Trung t&acirc;m Nghi&ecirc;n cứu ứng dụng v&agrave; Chuyển giao c&ocirc;ng nghệ m&ocirc;i trường.</p>
<p>&nbsp; - Trung t&acirc;m C&ocirc;ng nghệ Th&ocirc;ng tin v&agrave; Truyền th&ocirc;ng (Trang tin điện tử tổng hợp: Ph&aacute;p luật T&agrave;i ch&iacute;nh v&agrave; đầu tư ; Đặc san khoa học: T&agrave;i ch&iacute;nh v&agrave; Đầu tư Đ&ocirc;ng</p>
<p><strong>4 Khối đơn vị trực thuộc:</strong></p>
<p>&nbsp; - Ph&acirc;n viện Nghi&ecirc;n cứu T&agrave;i ch&iacute;nh, Đầu tư v&agrave; Hợp t&aacute;c, Thương mại Đ&ocirc;ng Nam &Aacute; khu vực Miền Trung : Trụ sở đặt tại TP. Đ&agrave; Nẵng.</p>
<p>&nbsp; - Ph&acirc;n viện Nghi&ecirc;n cứu T&agrave;i ch&iacute;nh, Đầu tư v&agrave; Hợp t&aacute;c, Thương mại Đ&ocirc;ng Nam &Aacute; khu vực Miền Nam : Trụ sở đặt tại TP. Hồ Ch&iacute; Minh.</p>
<p>&nbsp; - Ph&acirc;n viện Nghi&ecirc;n cứu T&agrave;i ch&iacute;nh, Đầu tư v&agrave; Hợp t&aacute;c, Thương mại Đ&ocirc;ng Nam &Aacute; khu vực T&acirc;y Bắc : Trụ sở đặt tại TP. Y&ecirc;n B&aacute;i.</p>
<p>&nbsp; - Ph&acirc;n viện Nghi&ecirc;n cứu T&agrave;i ch&iacute;nh, Đầu tư v&agrave; Hợp t&aacute;c, Thương mại Đ&ocirc;ng Nam &Aacute; khu vực T&acirc;y Nguy&ecirc;n: Trụ sở đặt tại TP. Bu&ocirc;n M&ecirc; Thuột.</p>
<p>&nbsp; - C&ocirc;ng ty TNHH Nghi&ecirc;n cứu, Ph&aacute;t triển v&agrave; Khai th&aacute;c T&agrave;i Nguy&ecirc;n Xanh.</p>
<p class="organization-img"><img src="https://edutech.org.vn/upload/images/co%20cau%20to%20chuc%20.jpg" alt="" /></p>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="flat-row">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="title-section color-title">
<h1 class="title">V&iacute; tr&iacute; ph&aacute;p l&yacute;</h1>
</div>
</div>
<!-- col-md-12 --></div>
<!-- row --></div>
<!-- container -->
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="about-us style2 wrap-box pdleft">
<div class="text-about-us">
<p><strong>Viện được th&agrave;nh lập v&agrave; hoạt động tr&ecirc;n cơ sở ph&aacute;p l&yacute; sau:</strong></p>
<p>&nbsp; a) Quyết định số 147/QĐ-TWH ng&agrave;y 11/12/2002 của Chủ tịch Trung ương Hội Nghi&ecirc;n cứu Khoa học Đ&ocirc;ng Nam &Aacute; &ndash; Việt Nam về việc th&agrave;nh lập Viện Nghi&ecirc;n cứu T&agrave;i ch&iacute;nh, Đầu tư v&agrave; Hợp t&aacute;c, Thương mại Đ&ocirc;ng Nam &Aacute;.</p>
<p>&nbsp; b) Quyết định số 148/QĐ-TWH ng&agrave;y 11/12/2002 của Chủ tịch Trung ương Hội Nghi&ecirc;n cứu Khoa học Đ&ocirc;ng Nam &Aacute; &ndash; Việt Nam về việc ph&ecirc; duyệt Điều lệ Tổ chức v&agrave; hoạt động của Viện Nghi&ecirc;n cứu T&agrave;i ch&iacute;nh, Đầu tư v&agrave; Hợp t&aacute;c, Thương mại Đ&ocirc;ng Nam &Aacute;.</p>
<p>&nbsp; c) Chứng nhận đăng k&yacute; hoạt động khoa học v&agrave; c&ocirc;ng nghệ (cấp lần thứ 5) số A &ndash; 069 của Bộ trưởng Bộ Khoa học v&agrave; C&ocirc;ng nghệ ng&agrave;y 20/08/2018.</p>
<p>d) Quyết định số 506/QĐ-TWH ng&agrave;y 24/02/2006 của Chủ tịch Trung ương Hội Nghi&ecirc;n cứu khoa học Đ&ocirc;ng Nam &Aacute; - Việt Nam về việc bổ sung chức năng , nhiệm vụ Tổ chức v&agrave; hoạt động của Viện Nghi&ecirc;n cứu T&agrave;i ch&iacute;nh, Đầu tư v&agrave; Hợp t&aacute;c, Thương mại Đ&ocirc;ng Nam &Aacute;.</p>
<p>&nbsp; e) Quyết định số 37/2016/QĐ-TWH ng&agrave;y 15/7/2016 của Chủ tịch Trung ương Hội Nghi&ecirc;n cứu khoa học Đ&ocirc;ng Nam &Aacute; &ndash; Việt Nam về việc bổ sung chức năng v&agrave; nhiệm vụ cho Viện Nghi&ecirc;n cứu T&agrave;i ch&iacute;nh, Đầu tư v&agrave; Hợp t&aacute;c, Thương mại</p>
<p>&nbsp; g) Quyết định số 42/2017/QĐ-TWH ng&agrave;y 21/7/20167 của Chủ tịch Trung ương Hội Nghi&ecirc;n cứu khoa học Đ&ocirc;ng Nam &Aacute; &ndash; Việt Nam về việc c&ocirc;ng nhận Hội đồng Viện Nghi&ecirc;n cứu T&agrave;i ch&iacute;nh, Đầu tư v&agrave; Hợp t&aacute;c, Thương mại Đ&ocirc;ng Nam &Aacute;</p>
<p><strong>Cơ quan chủ quản: Viện l&agrave; đơn vị sự nghiệp khoa học trực thuộc Trung ương Hội nghi&ecirc;n cứu Khoa</strong></p>
<p><strong>Tư c&aacute;ch ph&aacute;p nh&acirc;n:</strong></p>
<p>&nbsp; - Viện Nghi&ecirc;n cứu T&agrave;i ch&iacute;nh, Đầu tư v&agrave; Hợp t&aacute;c, Thương mại Đ&ocirc;ng Nam &Aacute; c&oacute; tư c&aacute;ch ph&aacute;p nh&acirc;n, c&oacute; con dấu v&agrave; t&agrave;i khoản ri&ecirc;ng , hoạt động theo phương thức tự chủ về t&agrave;i ch&iacute;nh, hạch to&aacute;n kinh tế độc lập.</p>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="flat-row">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="title-section color-title">
<h1 class="title">Chức năng v&agrave; nhiệm vụ</h1>
</div>
</div>
<!-- col-md-12 --></div>
<!-- row --></div>
<!-- container -->
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="about-us style2 wrap-box pdleft">
<div class="text-about-us">
<p><strong>I.C&aacute;c chức năng v&agrave; nhiệm của viện SEAFIT:</strong></p>
<p>&nbsp; - Nghi&ecirc;n cứu khoa học trong c&aacute;c lĩnh vực t&agrave;i ch&iacute;nh, tiền tệ, ng&acirc;n h&agrave;ng. Nghi&ecirc;n cứu cơ chế, ch&iacute;nh s&aacute;ch hoạt động đầu tư v&agrave; hợp t&aacute;c. Lưu th&ocirc;ng thương mại khu vực ASEAN v&agrave; quốc tế.</p>
<p>&nbsp; - Nghi&ecirc;n cứu v&agrave; ứng dụng cơ chế quản l&yacute; kinh tế, ch&iacute;nh s&aacute;ch thuế, t&agrave;i ch&iacute;nh, gi&aacute; cả thị trường , ch&iacute;nh s&aacute;ch ph&aacute;t triển nguồn nh&acirc;n lực, nguồn lực đầu tư trong nước v&agrave; quốc tế.</p>
<p>&nbsp; - Nghi&ecirc;n cứu khoa học gi&aacute;o dục khu vực ASEAN v&agrave; quốc tế. Nghi&ecirc;n cứu khoa học v&agrave; triển khai ứng dụng c&aacute;c tiến bộ khoa học &ndash; c&ocirc;ng nghệ trong c&aacute;c lĩnh vực kinh tế, gi&aacute;o dục, x&acirc;y dựng, ph&aacute;t triển đ&ocirc; thị, giao th&ocirc;ng, n&ocirc;ng nghiệp, n&ocirc;ng th&ocirc;n, sức khỏe, m&ocirc;i trường v&agrave; ph&aacute;t triển cộng đồng.</p>
<p>&nbsp; - Tư vấn quản l&yacute; doanh nghiệp, tư vấn dự &aacute;n đầu tư, thương mại. Tư vấn trong lĩnh vực gi&aacute;o dục, gi&aacute;o dục nghề nghiệp v&agrave; đ&agrave;o tạo ph&aacute;t triển nguồn nh&acirc;n lực.</p>
<p>&nbsp; - Đ&agrave;o tạo, bồi dưỡng n&acirc;ng cao nghiệp vụ chuy&ecirc;n m&ocirc;n, nghề nghiệp trong c&aacute;c doanh nghiệp, tổ chức , c&aacute;c cơ sở gi&aacute;o dục &ndash; đ&agrave;o tạo.</p>
<p>&nbsp; - Li&ecirc;n kết đ&agrave;o tạo về c&aacute;c lĩnh vực: Gi&aacute;o dục, khoa học, c&ocirc;ng nghệ, du lịch, nghệ thuật, văn h&oacute;a &ndash; x&atilde; hội v&agrave; c&aacute;c lĩnh vực c&oacute; li&ecirc;n quan c&aacute;c cấp tr&igrave;nh độ từ sơ cấp đến đại học.</p>
<p>&nbsp; - Dịch vụ kiểm to&aacute;n, kế to&aacute;n ; tư vấn v&agrave; dịch vụ s&agrave;n giao dịch chứng kho&aacute;n.</p>
<p>&nbsp; - Tổ chức hội thảo khoa học, bi&ecirc;n soạn, bi&ecirc;n dịch in ấn, xuất bản c&aacute;c ấn phẩm trong c&aacute;c lĩnh vực n&ecirc;u tr&ecirc;n.</p>
<p>&nbsp; - Hợp t&aacute;c với c&aacute;c tổ chức, c&aacute; nh&acirc;n trong v&agrave; ngo&agrave;i nước trong c&aacute;c lĩnh vực nghi&ecirc;n cứu của Viện.</p>
<p><strong>II. Loại h&igrave;nh hoạt động Khoa học v&agrave; C&ocirc;ng nghệ:</strong></p>
<p>&nbsp; - Đơn vị sự nghiệp khoa học c&ocirc;ng nghệ hoạt động theo cơ chế tự chủ.</p>
</div>
</div>
</div>
</div>
</div>
</section>',
'created_at' => '2019-09-24 03:56:39',
'updated_at' => '2019-09-24 03:57:57',
'deleted_at' => NULL,
),
1 => 
array (
'id' => 2,
'page_id' => 33,
'title' => 'Chính sách bảo mật',
'content' => NULL,
'created_at' => '2019-09-24 03:58:30',
'updated_at' => '2019-09-24 03:58:30',
'deleted_at' => NULL,
),
2 => 
array (
'id' => 3,
'page_id' => 34,
'title' => 'Thoả thuận sử dụng',
'content' => NULL,
'created_at' => '2019-09-24 03:58:47',
'updated_at' => '2019-09-24 03:58:47',
'deleted_at' => NULL,
),
));
        
        
    }
}