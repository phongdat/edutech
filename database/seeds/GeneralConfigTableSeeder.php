<?php

use Illuminate\Database\Seeder;

class GeneralConfigTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('general_config')->delete();
        
        \DB::table('general_config')->insert(array (
            0 => 
            array (
                'id' => 6,
                'name' => 'logo',
                'value' => 'http://localhost:8000/files/2/logo.png',
                'type' => 'image',
                'created_at' => '2019-09-24 09:20:39',
                'updated_at' => '2019-09-24 09:20:39',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 7,
                'name' => 'so-dien-thoai',
                'value' => '+84-242023.5566',
                'type' => 'textbox',
                'created_at' => '2019-09-24 09:24:51',
                'updated_at' => '2019-09-24 09:24:51',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 8,
                'name' => 'icon-twitter',
                'value' => '<i class="fa fa-twitter"></i>',
                'type' => 'icon',
                'created_at' => '2019-09-24 09:26:34',
                'updated_at' => '2019-09-24 09:27:05',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 9,
                'name' => 'email',
                'value' => 'seafit.edu@gmail.com',
                'type' => 'textbox',
                'created_at' => '2019-09-24 09:26:58',
                'updated_at' => '2019-09-24 09:29:07',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 10,
                'name' => 'icon-google-plus',
                'value' => '<i class="fa fa-google-plus"></i>',
                'type' => 'icon',
                'created_at' => '2019-09-24 09:27:38',
                'updated_at' => '2019-09-24 09:27:38',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 11,
                'name' => 'icon-facebook',
                'value' => '<i class="fa fa-facebook-f"></i>',
                'type' => 'icon',
                'created_at' => '2019-09-24 09:28:00',
                'updated_at' => '2019-09-24 09:28:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 12,
                'name' => 'icon-instagram',
                'value' => '<i class="fa fa-instagram"></i>',
                'type' => 'icon',
                'created_at' => '2019-09-24 09:28:20',
                'updated_at' => '2019-09-24 09:28:20',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 13,
                'name' => 'fax',
                'value' => '+84-242023.5566',
                'type' => 'textbox',
                'created_at' => '2019-09-24 09:31:27',
                'updated_at' => '2019-09-24 09:31:27',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 14,
                'name' => 'icon-telephone',
                'value' => 'http://localhost:8000/files/2/telephone-icon.png',
                'type' => 'image',
                'created_at' => '2019-09-24 09:32:53',
                'updated_at' => '2019-09-24 09:32:53',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 15,
                'name' => 'letter-icon',
                'value' => 'http://localhost:8000/files/2/letter-icon.png',
                'type' => 'image',
                'created_at' => '2019-09-24 09:33:53',
                'updated_at' => '2019-09-24 09:33:53',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 16,
                'name' => 'notification',
                'value' => 'http://localhost:8000/files/2/notification.png',
                'type' => 'image',
                'created_at' => '2019-09-24 09:35:23',
                'updated_at' => '2019-09-24 09:35:23',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 17,
                'name' => 'vien-nghien-cuu-tai-chinh',
                'value' => 'VIỆN NGHIÊN CỨU TÀI CHÍNH,
<br> ĐẦU TƯ & HỢP TÁC, THƯƠNG 
<br> MẠI ĐÔNG NAM Á',
                'type' => 'textarea',
                'created_at' => '2019-09-24 09:36:39',
                'updated_at' => '2019-09-24 09:36:39',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 18,
                'name' => 'dia-chi',
                'value' => 'Địa chỉ: Khu đô thị Nam An Khánh, <br> Hoài Đức, TP Hà Nội',
                'type' => 'textarea',
                'created_at' => '2019-09-24 09:37:50',
                'updated_at' => '2019-09-24 09:37:50',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 19,
                'name' => 'vpgd',
                'value' => 'VPGD: Số 2, Đường Đức Diễn, P Phúc Diễn,<br>Q Bắc Từ Liêm, TP Hà Nội',
                'type' => 'textarea',
                'created_at' => '2019-09-24 09:38:23',
                'updated_at' => '2019-09-24 09:38:23',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 20,
                'name' => 'copyright',
                'value' => '© 2019 EduTech, All rights reserved.',
                'type' => 'textbox',
                'created_at' => '2019-09-24 09:39:35',
                'updated_at' => '2019-09-24 09:39:35',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}