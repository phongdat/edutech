<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddAttributesToNotificationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_details', function (Blueprint $table) {
            $table->boolean('status')->default(false);
            $table->bigInteger('sender_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_details', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('sender_id');
        });
    }
}
