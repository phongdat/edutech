<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessResourcesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('access_resources', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->bigInteger('page_id')->unsigned()->nullable();
			$table->bigInteger('control_id')->unsigned()->nullable();

			$table->string('name')->nullable();
			$table->string('description')->nullable();

			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('access_resources');
	}

}
