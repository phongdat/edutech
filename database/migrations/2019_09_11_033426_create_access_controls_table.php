<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessControlsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('access_controls', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->bigInteger('role_id')->unsigned()->nullable();
			$table->bigInteger('access_resource_id')->unsigned()->nullable();

			$table->boolean('status')->default(false);

			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('access_controls');
	}

}
