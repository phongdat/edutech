<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rating_lessons', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('lesson_id')->unsigned()->nullable();
            $table->bigInteger('member_id')->unsigned()->nullable();

            $table->integer('rating')->nullable();
            $table->text('content')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rating_lessons');
    }
}
