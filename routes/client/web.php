<?php
Route::group([
], function () {
    $c = 'Client\\';
    
    Route::get('/', $c . 'HomeController@index');

    //Level
    Route::get('/hoc-online/{parent}', $c . 'LevelController@index');
    
    // Independent page
    Route::get('/contact', $c . 'HomeController@contact');
    Route::get('/introduce', $c . 'HomeController@introduce');
    Route::get('/policy', $c . 'HomeController@policy');
    Route::get('/agreement', $c . 'HomeController@agreement');    
    Route::get('/nhan-su-va-to-chuc', $c . 'HomeController@personnel');
    Route::get('/huong-dan', $c . 'HomeController@instruction');
    Route::get('/bo-cong-thuong', $c . 'HomeController@ministry');

    // Account
    Route::get('/account/{id?}', $c . 'AccountController@index');    
    Route::get('/lich-su-giao-dich', $c . 'AccountController@historyTransaction');
    Route::get('/chinh-sua-ho-so', $c . 'AccountController@editProfile');
    Route::get('/doi-mat-khau', $c . 'AccountController@editPassword');
    Route::get('/danh-sach', $c . 'AccountController@list');
    Route::get('/danh-gia', $c . 'AccountController@evaluate');
    Route::get('/bai-giang/{lessonId?}', $c . 'AccountController@lesson');
    Route::post('/saveChangePassword', $c . 'AccountController@saveChangePassword');
    Route::post('/saveEditProfile', $c . 'AccountController@saveEditProfile');
    Route::post('/rating', $c . 'AccountController@rating');
    Route::post('/like-course', $c . 'AccountController@likeCourse');    
    // Card
    Route::get('/transfer', $c . 'CardController@transfer');

    // News
    Route::get('/tin-tuc', $c . 'NewsController@index');
    Route::get('/tin-tuc/{news}', $c . 'NewsController@detail');

    // Error
    Route::get('pageNotFound', ['as' => 'notfound', 'uses' => 'Error\ErrorController@clientPageNotFound']);

}); 
Route::group([
    'middleware' => 'doNotCacheResponse',
], function () {
    $c = 'Client\\';
    Route::get('/hoc-online/{parent}/{courseCategory}/{course?}', $c . 'LevelController@detail');

    // Login & Logout & Register
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::get('register', 'Auth\LoginController@showLoginForm')->name('register');
    Route::get('logout', 'Auth\LoginController@logout');

    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@create');

    Route::post('/submit-contact-form', $c . 'HomeController@submitContactForm');

    // Account
    Route::get('/account/{id?}', $c . 'AccountController@index');
    Route::get('/lich-su-giao-dich', $c . 'AccountController@historyTransaction');
    Route::get('/chinh-sua-ho-so', $c . 'AccountController@editProfile');
    Route::get('/doi-mat-khau', $c . 'AccountController@editPassword');
    Route::get('/danh-gia', $c . 'AccountController@evaluate');
    Route::get('/bai-giang/{lessonId?}', $c . 'AccountController@lesson');
    Route::post('/saveChangePassword', $c . 'AccountController@saveChangePassword');
    Route::post('/saveEditProfile', $c . 'AccountController@saveEditProfile');
    Route::get('/quen-mat-khau', $c . 'AccountController@forgotPassword');
    Route::post('/submit-forgot-password', $c . 'AccountController@saveForgotPassword');
    

    // Card    
    Route::post('/account/paid', $c . 'CardController@paid');
    Route::post('/saveTransfer', $c . 'CardController@saveTransfer');
    Route::post('/rechargeCard', $c . 'CardController@rechargeCard');

    // API
    Route::post('/api/create-user', $c . 'AccountController@registerUser');
    Route::post('/api/change-password', $c . 'AccountController@changePasswordApi');
});