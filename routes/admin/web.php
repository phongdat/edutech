<?php
Route::group([
    'middleware' => 'auth',
    'middleware' => 'doNotCacheResponse',
    'prefix' => 'admin'
], function () {
    $d = 'Admin\\';

    // Dashboard
    Route::get('/', $d . 'DashboardController@index');
    Route::get('/dashboard', $d . "DashboardController@index");

    // Roles
    Route::get('/roles', $d . 'RolesController@index');
    Route::get('/roles/new', $d . 'RolesController@new');
    Route::get('/roles/update/{roleId}', $d . 'RolesController@update');
    Route::post('/roles/save', $d . 'RolesController@save');

    // Users
    Route::get('/users', $d . 'UsersController@index');
    Route::get('/users/new', $d . 'UsersController@new');
    Route::get('/users/update/{userId}', $d . 'UsersController@update');
    Route::post('/users/save', $d . 'UsersController@save');
    Route::post('/users/search', $d . 'UsersController@search');

    // Pages
    Route::get('/functions', $d . 'PagesController@index');
    Route::get('/functions/new', $d . 'PagesController@new');
    Route::get('/functions/update/{pageId}', $d . 'PagesController@update');
    Route::get('/functions/delete/{pageId}', $d . 'PagesController@delete');
    Route::post('/functions/save', $d . 'PagesController@save');
    Route::post('/functions/search', $d . 'PagesController@search');

    // Lesson
    Route::get('/lessons', $d . 'LessonController@index');
    Route::get('/lessons/new', $d . 'LessonController@new');
    Route::get('/lessons/update/{lessonId}', $d. 'LessonController@update');
    Route::post('/lessons/save', $d . 'LessonController@save');
    Route::post('/lessons/search', $d. 'LessonController@search');
    Route::get('/lessons/search', $d. 'LessonController@searchPage');
    Route::post('/lessons/del', $d. 'LessonController@delete');
    Route::post('/lessons/filter', $d . 'LessonController@filter');
    Route::get('/lessons/filter', $d . 'LessonController@filterPage');

    Route::get('/approve', $d . 'LessonController@approveView');
    Route::post('/lessons/approved', $d . 'LessonController@approvedLesson');
    Route::post('/lessons/no-approved', $d . 'LessonController@noApprovedLesson');

    // Course Category
    Route::get('/category/course/new', $d . 'CategoryCourseController@new');
    Route::get('/category/course/update/{categoryId}', $d . 'CategoryCourseController@update');
    Route::get('/category/course/delete/{categoryId}', $d . 'CategoryCourseController@delete');
    Route::post('/category/course/save', $d . 'CategoryCourseController@save');
   
    //Object
    Route::get('/object', $d. 'ObjectController@object');
    Route::get('/object/{objectId}', $d. 'ObjectController@update');
    Route::get('/object/delete/{objectId}', $d. 'ObjectController@delete');
    Route::post('/object/save', $d. 'ObjectController@save');
    
    // Course
    Route::get('/courses', $d . 'CourseController@index');
    Route::get('/course/new', $d . 'CourseController@new');
    Route::get('/courses/update/{course_id}', $d . 'CourseController@update');
    Route::post('/course/save', $d . 'CourseController@save');

    Route::post('/course/search', $d . 'CourseController@search');
    Route::get('/course/search', $d . 'CourseController@searchPage');
    Route::post('/courses/delete', $d . 'CourseController@delete');
    Route::get('/course/export', $d . 'CourseController@export');

    Route::post('/course/filter', $d . 'CourseController@filter');
    Route::get('/course/filter', $d . 'CourseController@filterPage');

    Route::get('/course/import-view', $d . 'CourseController@importView');
    Route::post('/course/import', $d . 'CourseController@import');
    
    // Teacher
    Route::get('/teachers', $d . 'TeacherController@index');
    Route::get('/teachers/new', $d . 'TeacherController@new');
    Route::get('/teachers/update/{teacherId}', $d . 'TeacherController@update');
    Route::post('/teachers/save', $d . 'TeacherController@save');
    Route::post('/teacher/deactive', $d . 'TeacherController@deactive');
    Route::post('/teacher/delete', $d . 'TeacherController@delete');
    
    // Cards
    Route::get('/cards', $d . 'CardController@index');
    Route::get('/cards/new', $d . 'CardController@new');
    Route::post('/cards/save', $d . 'CardController@save');
    Route::post('/cards/search', $d . 'CardController@search');
    Route::post('/cards/import',$d . 'CardController@import');
    Route::get('/card/generate-group-series',$d . 'CardController@generateGroupSeries');

    // sales-area
    Route::get('/sales-area', $d . 'SaleAreaController@index');
    Route::get('/sales-area/new', $d . 'SaleAreaController@new');
    Route::get('/sales-area/update/{saleId}', $d . 'SaleAreaController@update');
    Route::get('/sales-area/transfer/{saleId}', $d . 'SaleAreaController@transfer');
    Route::post('/sales-area/save', $d . 'SaleAreaController@save');
    Route::post('/card/transfer/save', $d . 'SaleAreaController@transferSave');
    Route::post('/sales-area/paid', $d . 'SaleAreaController@paid');

    // Distributor
    Route::get('/distributor', $d . 'DistributorController@index');
    Route::get('/distributor/new', $d . 'DistributorController@new');
    Route::post('/distributor/save', $d . 'DistributorController@save');
    Route::post('/distributor/paid', $d . 'DistributorController@paid');
    Route::get('/distributor/transfer/{saleId}', $d . 'DistributorController@transfer');

    // Agency
    Route::get('/agency', $d . 'AgencyController@index');
    Route::get('/agency/new', $d . 'AgencyController@new');
    Route::post('/agency/save', $d . 'AgencyController@save');
    Route::post('/agency/paid', $d . 'AgencyController@paid');
    Route::get('/agency/transfer/{saleId}', $d . 'AgencyController@transfer');

    // Card Configure
    Route::get('/card-configure', $d . 'CardConfigureController@index');
    Route::get('/card-configure/update/{configId}', $d . 'CardConfigureController@update');
    Route::post('/card-config/save', $d . 'CardConfigureController@save');

    // Products 
    Route::get('/products', $d . 'ProductController@index');
    Route::get('/product/new', $d . 'ProductController@new');
    Route::get('/product/update/{prodId}', $d . 'ProductController@update');
    Route::post('/product/save', $d . 'ProductController@save');

    // Products spcecial
    Route::get('/products/special', $d . 'ProductSpecialController@index');


    //General page configuration
    Route::get('/introduction', $d . 'GeneralPageConfigController@index');
    Route::get('/policy', $d . 'GeneralPageConfigController@index');
    Route::get('/agreement', $d . 'GeneralPageConfigController@index');
    Route::get('/personnel', $d . 'GeneralPageConfigController@index');
    Route::get('/instruction', $d . 'GeneralPageConfigController@index');
    Route::get('/ministry', $d . 'GeneralPageConfigController@index');
    Route::post('/general-page-config/save', $d . 'GeneralPageConfigController@save');    

    // Web config
    Route::get('/webconfig', $d . 'GeneralWebConfigController@index');
    Route::get('/webconfig/new', $d . 'GeneralWebConfigController@new');
    Route::get('/webconfig/update/{id}', $d . 'GeneralWebConfigController@update');
    Route::post('/webconfig/save', $d . 'GeneralWebConfigController@save');
    Route::get('/webconfig/delete/{id}', $d . 'GeneralWebConfigController@delete');

    //Students
    Route::get('/students', $d . 'StudentController@index');
    Route::get('/students/new', $d . 'StudentController@new');
    Route::get('/students/update/{studentId}', $d . 'StudentController@update');
    Route::get('/students/listCard/{studentId}', $d . 'StudentController@listCard');
    Route::post('/students/save', $d . 'StudentController@save');
    Route::post('/students/deactive', $d . 'StudentController@deactive');
    // Route::post('/students/delete/{studentId}', $d . 'StudentController@delete');

    // News
    Route::get('/newses', $d . 'NewsController@index');
    Route::get('/newses/new', $d . 'NewsController@new');
    Route::get('/newses/update/{id}', $d . 'NewsController@update');
    Route::post('/newses/save', $d . 'NewsController@save');
    Route::get('/newses/delete/{id}', $d . 'NewsController@delete');

});

Route::any('tus/{any?}', function () {
    $response = app('tus-server')->serve();
    return $response->send();
})->where('any', '.*');