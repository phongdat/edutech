<?php

namespace App\Http\Services;
use App\Models\Course;
use App\Models\Media;
use App\Models\Member;
use App\Models\Lesson;
use App\Models\CourseCategory;
use Illuminate\Support\Facades\Log;

class CourseServices {

    public static function getAscCoursesBelongToCategoryId($id)
    {
        $courses = Course::where('course_category_id', $id)->orderBy('created_at', 'asc')->get();
        for ($i=0; $i < count($courses); $i++) {             
            if ($courses[$i]->banner_media_id) {
                $courses[$i]->banner = Media::getBanner($courses[$i]->banner_media_id);
            }
            if ($courses[$i]->video_media_id) {
                $courses[$i]->video =  Media::getVideo($courses[$i]->video_media_id);
            }
            if ($courses[$i]->user_id) {
                $teacher = Member::find($courses[$i]->user_id);
                $courses[$i]->teacher_name = $teacher->full_name;
                $courses[$i]->teacher_id = $teacher->id;
            }
            $courses[$i]->lessons =  LessonServices::getLessonsByCourseId($courses[$i]->id);               
        }
        return $courses;
    }

    public static function importCourse( $row )
    {
        $course_category_id = $row[1];
        $course_category = CourseCategory::find($course_category_id);
        if( !isset( $course_category ) )
        {
            Log::error('Not found course_categorty_id: ' . $course_category_id);
            return;
        }        

        $sample_course = Course::where('course_category_id', $course_category_id)->first();

        $course_nane = trim( $row[2] );

        $course = Course::where( [
            [ 'course_category_id', '=', $course_category_id ],
            [ 'title', 'like', '%'. $course_nane . '%' ],
            [ 'user_id', '=', $sample_course->user_id ]
        ] )->first();

        if( !isset( $course ) )
        {
            $course = new Course();
            $course->user_id = $sample_course->user_id;
            $course->course_category_id = $sample_course->course_category_id;
            $course->banner_media_id = $sample_course->banner_media_id;
            $course->title = $course_nane;
            $course->content = '<h3 class="sppb-addon-title" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; line-height: 23px; border: 0px; font-size: 18px; margin: 10px 0px 16px 5px; outline: 0px; padding: 0px; vertical-align: baseline; overflow-wrap: break-word; font-family: "Roboto Condensed", sans-serif; font-weight: normal; color: blue;">' . $course_nane . '</h3>';
            $course->save();

            Log::info('created course name: ' . $course_nane);
        }

        $lesson_nane = trim( $row[3] );

        $lesson = Lesson::where( [
            [ 'course_id', '=', $course->id ],
            [ 'title', 'like', '%'. $lesson_nane . '%' ],
            [ 'user_id', '=', $course->user_id ]
        ] )->first();

        if( !isset( $lesson ) )
        {
            $lesson = new Lesson();
            $lesson->user_id = $sample_course->user_id;
            $lesson->course_id = $course->id;
            $lesson->banner_media_id = $sample_course->banner_media_id;
            $lesson->tags = 'import';
            $lesson->title = $lesson_nane;
            $lesson->content = '<p><a style="box-sizing: border-box; border: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: 23px; font-family: "Open Sans", sans-serif; margin: 0px; outline: none medium; padding: 0px; vertical-align: baseline; text-decoration-line: none; color: #000000; transition: all 0.3s ease-out 0s;" href="https://edutech.org.vn/user/login">' . $lesson_nane . '</a></p>';
            $lesson->save(); 
            
            Log::info('created lesson name: ' . $lesson_nane);
        }
    }
}