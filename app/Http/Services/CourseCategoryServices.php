<?php

namespace App\Http\Services;
use App\Models\CourseCategory;
use App\Models\Course;
use App\Models\Rating;

class CourseCategoryServices {

    public static function getCourseData()
    {
        $categories = CourseCategory::getParents();  
        $result = array();
        for ($i=0; $i < count($categories); $i++) {             
            $result[$categories[$i]->name] = self::getAllCourseBelongToCategory($categories[$i]->id, $categories[$i]);
        }
        return $result; 
    }

    public static function getCourseBelongToCategory( $id, $parent, $member=null )
    {
        
        $categories = CourseCategory::getNumberOfNewestCoursesByCateId($id, 6);     
        $liked_courses = null;
        if( $member )
        {
            $liked_courses = Rating::where('member_id', $member->id)
                                    ->get();                                               
        }   
        for ($i=0; $i < count($categories); $i++) {  
            if($categories[$i]->parent_id)
                $categories[$i]->parentSlugify = str_slug($parent->name .' '. $parent->id);
                            
            if( $liked_courses && $liked_courses->contains( 'course_category_id', $categories[$i]->id ) )
            {
                $current_rating = $liked_courses->where('course_category_id', $categories[$i]->id)->first();
                if( $current_rating )                
                    $categories[$i]->rating = $current_rating->rating;
            }
            $categories[$i]->slugify = str_slug($categories[$i]->name .' '. $categories[$i]->id);  
            $categories[$i]->detail = CourseServices::getAscCoursesBelongToCategoryId($categories[$i]->id)[0];                 
        }
        return $categories;
    }

    public static function getCourseShowData( $member=null )
    {
        $categories = CourseCategory::getParents();  
        $result = array();
        for ($i=0; $i < count($categories); $i++) {             
            $result[$categories[$i]->name] = self::getCourseBelongToCategory($categories[$i]->id, $categories[$i], $member);
        }
        return $result; 
    }

    public static function getAllCourseBelongToCategory($id, $parent, $member=null)
    {
        $categories = CourseCategory::getCategoriesById($id);    
        $liked_courses = null;
        if( $member )
        {
            $liked_courses = Rating::where('member_id', $member->id)
                                    ->get();            
        }      
        for ($i=0; $i < count($categories); $i++) {  
            if($categories[$i]->parent_id)
                $categories[$i]->parentSlugify = str_slug($parent->name .' '. $parent->id);
            if( $liked_courses && $liked_courses->contains('course_category_id', $categories[$i]->id) )
            {
                $current_rating = $liked_courses->where('course_category_id', $categories[$i]->id)->first();
                if( $current_rating )  
                    $categories[$i]->rating = $current_rating->rating;
            }
            $categories[$i]->slugify = str_slug($categories[$i]->name .' '. $categories[$i]->id);  
            $categories[$i]->detail = CourseServices::getAscCoursesBelongToCategoryId($categories[$i]->id)[0];                 
        }
        return $categories;
    }

    public static function getCategoryParents()
    {
        $parents = CourseCategory::getParents();         
        for ($i=0; $i < count($parents); $i++) {             
            $parents[$i]->slugify = str_slug($parents[$i]->name .' '. $parents[$i]->id);                   
        }
        return $parents;
    }

    public static function getNumOfNewestData( $num, $member=null )
    {
        $result = array();        
        $categories = CourseCategory::getParents();  
        for ($i=0; $i < count($categories); $i++) { 
            $result[$categories[$i]->name] = CourseCategory::getNumberOfNewestCoursesByCateId($categories[$i]->id, $num);
        }  
        return $result;
    }

    public static function findById( $id, $member=null )
    {
        $currentCourseCategory = CourseCategory::find($id);          
           
        if($currentCourseCategory->parent_id)
            $currentCourseCategory->detail = CourseServices::getAscCoursesBelongToCategoryId($id)[0];                          
            
        $currentCourseCategory->slugify = str_slug($currentCourseCategory->name .' '. $currentCourseCategory->id);  
        if( $member )
        {
            $liked_course = Rating::where([ 
                                            [ 'member_id', '=', $member->id ],
                                            [ 'course_category_id', '=', $id ],
                                        ])
                                    ->first(); 
            if( $liked_course )                
                $currentCourseCategory->rating = $liked_course->rating;
        }      
        return $currentCourseCategory;
    }

    public static function getRandomNewestData( $num, $member=null )
    {
        $categories = CourseCategory::getNumberOfNewestCourses($num);
        $liked_courses = null;
        if( $member )
        {
            $liked_courses = Rating::where('member_id', $member->id)
                                    ->get();            
        }   
        for ($i=0; $i < count($categories); $i++) {  
            $parent = CourseCategory::find($categories[$i]->parent_id);
            if( $liked_courses && $liked_courses->contains('course_category_id', $categories[$i]->id) )
            {          
                $current_rating = $liked_courses->where('course_category_id', $categories[$i]->id)->first();
                if( $current_rating )        
                    $categories[$i]->rating = $current_rating->rating;
            }
            $categories[$i]->slugify = str_slug($categories[$i]->name .' '. $categories[$i]->id);
            $categories[$i]->parentSlugify = str_slug($parent->name .' '. $parent->id);
            $categories[$i]->detail = CourseServices::getAscCoursesBelongToCategoryId($categories[$i]->id)[0];                 
        }
        return $categories;
    }
}