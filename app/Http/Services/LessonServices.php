<?php

namespace App\Http\Services;
use App\Models\Lesson;
use App\Models\Media;
use App\Models\Member;
use App\Models\RatingLesson;

class LessonServices {

    public static function getLessonsByCourseId($id)
    {
        $lessons = Lesson::where([
                                    ['course_id', '=', $id],
                                    ['status', '=', \Config::get('constants.lesson_status.approved')],
                                ])
                            ->orWhere([
                                ['course_id', '=', $id],
                                ['status', '=', null],
                            ]) 
                            ->get();
        for ($i=0; $i < count($lessons); $i++) { 
            $lessons[$i]->tag_list = [];
            if ( $lessons[$i]->tags ) {
                $tags = explode( ",",$lessons[$i]->tags);
                $lessons[$i]->tag_list = $tags;
            }
            if ($lessons[$i]->banner_media_id) {
                $lessons[$i]->banner = Media::getBanner($lessons[$i]->banner_media_id);
            }
            if ($lessons[$i]->video_media_id) {
                $lessons[$i]->video = Media::getVideo($lessons[$i]->video_media_id);
            }
            if ($lessons[$i]->user_id) {
                $teacher = Member::find($lessons[$i]->user_id);
                $lessons[$i]->teacher_name = $teacher->full_name;
                $lessons[$i]->teacher_id = $teacher->id;
            }
            $lessons[$i]->slugify = str_slug($lessons[$i]->title .' '. $lessons[$i]->id);  
        }
        return $lessons;
    }

    public static function get_rating_each_lesson_by_member( $member  )
    {
        $lessons = Lesson::where( 'user_id', $member->id )->get();
        
        for ($i=0; $i < sizeof( $lessons ); $i++) {                         
            $lessons[$i]->rating = RatingLesson::calc_rating( $lessons[$i]->id );
        }
        return $lessons;
    }

    public static function get_rating_list_except_logger( $member )
    {
        $rating_lessons = RatingLesson::where([
                                    [ 'member_id', '=', $member->id ],
                                    [ 'rating', '=', null ]                  
                                ])
                            ->get();
        for ($i=0; $i < sizeof( $rating_lessons ); $i++) {             
            $rating_lessons[$i]->lesson = Lesson::find( $rating_lessons[$i]->lesson_id );;
        }
        return $rating_lessons;
    }
}