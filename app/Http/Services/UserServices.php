<?php

namespace App\Http\Services;
use App\Models\User;
use App\Models\Order;
use App\Models\Member;
use App\Models\ProductCourseAssignment;
use App\Models\ProductCardAssignment;
use App\Models\CourseCategory;
use App\Models\MemberCurrency;
use App\Models\Transaction;
use App\Models\Stock;
use App\Models\Phone;
use App\Models\Notification;
use DB;
use Illuminate\Support\Facades\Hash;

class UserServices {

    public static function registerUser($username, $password)
    {
        $user = new User;
        $checkUsernameExisted = User::where('username', $username)->first();        
        if($checkUsernameExisted){
            return null;
        }
        $user->username = $username;
        $user->password = $password;
        $user->save();

        $phone = new Phone();
        $phone->user_id = $user->id;
        $phone->national = 'Viet Nam';
        $phone->phone = $username;
        $phone->save();

        return $user;
    }

    public static function getUserByUsername($username)
    {
        $user = User::where('username', $username)->first();             
        return $user;
    }
    
    public static function checkCourseIsAvailable($course_category_id, $member)
    {
        //special case 
        if(self::checkVipAccount($member)){
            return true;
        }

        //normal case
        $ordersProductId = Order::select('product_id')->where('member_id', $member->id)->get();
        $curProdCourseAss = ProductCourseAssignment::select('product_id')
                                                        ->whereIn('product_id', $ordersProductId)
                                                        ->where('course_category_id', $course_category_id)
                                                        ->get();            
        
        if(sizeof($curProdCourseAss) != 0)
            return true;                                                                         
        
        return false;
    }

    public static function checkVipAccount($member)
    {
        //special case 
        $checkSpecialCase = Order::where([
                                        ['member_id','=', $member->id],
                                        ['product_id', '=', 1]
                                    ])->first();                                    
        
        if($checkSpecialCase)
            return true;
        
        $ProdAssCards = ProductCardAssignment::select('card_id')->get();
        $specialCards = Order::where("member_id", $member->id)->whereNotIn('card_id', $ProdAssCards)->get();
        
        if(sizeof($specialCards) > 0)
            return true;
        
        return false;
    }

    public static function getStudyingCourses($member)
    {      
        //mormal case                        
        $categories = ProductCourseAssignment::join('orders', 'product_course_assignments.product_id', '=', 'orders.product_id')
                                            ->select('orders.product_id', 'orders.card_id', 'product_course_assignments.course_category_id', 'orders.created_at')
                                            ->get();
                    
        for ($i=0; $i < sizeof($categories); $i++) { 
            $categories[$i]->course_data = CourseCategory::find($categories[$i]->course_category_id);                                
        }            
        return $categories;                                               
    }

    public static function getPoint($member)
    {
        return MemberCurrency::getPointData($member);
    }

    public static function changePassword($user, $password)
    {
        $user = User::find($user->id);
        $user->password = Hash::make($password);
        $user->save();
        return $user;
    }

    public static function editProfile($user, $fullname, $address)
    {
        $member = Member::where('user_id', $user->id)->first();
        $member->full_name = $fullname;
        $member->address = $address;
        $member->save();
        return $member;
    }

    public static function getDataMemberByMemberType( $member )
    {
        $data = array();        
        // Type = 1, Student
        if( $member->member_type_id == 1 ){
            $checkVip = UserServices::checkVipAccount( $member );
            $stydyingCourses = UserServices::getStudyingCourses( $member );  
            $points = UserServices::getPoint( $member );   
            $data = ['checkVip' => $checkVip
                    , 'stydyingCourses' => $stydyingCourses
                    , 'points' => $points];
        }
        else 
        {
            $cards = Stock::getCardsOnMember($member->id);                                
            $data = ['cards' => $cards];
        }    
        return $data;
    }

    public static function getDataTransactionsByMemberType($member)
    {
        $data = array();        
        // Type = 1, Student
        if( $member->member_type_id == 1 ){
            $data['transactions'] = Transaction::getTransactions($member->id);
        }
        else 
        {            
            $data['receiving_transactions'] = Order::select('orders.*', DB::raw('count(*) as number_of_cards'), DB::raw('DATE(created_at) as day'))
                                                    ->groupBy('day', 'member_id')  
                                                    ->where('member_id', $member->id)
                                                    ->get();
            
            for ($i=0; $i < sizeof($data['receiving_transactions']); $i++) {
                if(!$data['receiving_transactions'][$i]->sender_id){
                    $data['receiving_transactions'][$i]->sender = 'Admin';
                }else{
                    $sender = Member::find($data['receiving_transactions'][$i]->sender_id);
                    if( !$sender )
                    {
                        $data['receiving_transactions'][$i]->sender = 'Không tim thấy';                
                    }
                    else
                    {
                        $data['receiving_transactions'][$i]->sender = $sender->full_name;                
                    }
                    
                }                
            }

            $data['sending_transactions'] = Order::select('orders.*', DB::raw('count(*) as number_of_cards'), DB::raw('DATE(created_at) as day'))
                                                ->groupBy('day', 'member_id')  
                                                ->where('sender_id', $member->id)
                                                ->get();

            for ($i=0; $i < sizeof($data['sending_transactions']); $i++) { 
                $receiver = Member::find($data['sending_transactions'][$i]->member_id);
                if($receiver)
                {
                    $data['sending_transactions'][$i]->receiver = $receiver->full_name;                
                }
                else{
                    $data['sending_transactions'][$i]->receiver = 'Không tim thấy';                
                }
                
            }
        }    
        return $data;
    }


    public static function getUserByEmailOrTel( $input_value, $type, &$value )
    {
        if($type == 'phone')
        {
            $user = User::where( 'username', $input_value )->first();

            if( isset( $user ) )
            {
                $value = $user->username;
                return $user;
            }
        }
        else
        {
            $user = User::where( 'email', $input_value )->first();

            if( isset( $user ) )
            {
                $value = $user->email;
                return $user;
            }
        }
        return null;
    }
    
    public static function isDuplicatedPhone( $input_value )
    {
        // TODO: tạm thời bỏ qua bảng phone
        // $phones = Phone::where( 'phone', $input_value )->get();

        // if( sizeof($phones) > 1)
        //     return true;

        $users = User::where('username', $input_value)->get();

        if( sizeof($users) > 1)
            return true;

        return false;
    }

    // public static function generate_account_link( $input_value )
    // {
    //     $phones = Phone::where( 'phone', $input_value )->get();

    //     if( sizeof($phones) > 1)
    //         return true;

    //     $users = User::where('username', $input_value)->get();

    //     if( sizeof($users) > 1)
    //         return true;

    //     return false;
    // }

    public static function get_notification_by_user( $user )
    {        
        return Notification::get_notification_by_user_id( $user->id );
    }

    public static function get_notification_by_member( $member )
    {
        return Notification::get_notification_by_member_id( $member->id );
    }
}