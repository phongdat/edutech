<?php

namespace App\Http\Services;

use App\Models\GeneralConfig;

class GeneralConfigServices
{
    public static function loadConfigs()
    {
        $configs = GeneralConfig::getConfigs();
        $result = array();
        for ($i=0; $i < count($configs); $i++) { 
            $result[$configs[$i]->name] = $configs[$i]->value;
        }
        return $result;
    }
}