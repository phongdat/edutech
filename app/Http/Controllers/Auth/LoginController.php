<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Services\CourseCategoryServices;
use App\Http\Services\GeneralConfigServices;
use App\Models\Member;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [ 
            'username' => 'required', 
            'password' => 'required'
        ]);
        if ($validator->fails()) {     
            return redirect('login')->with('error', $validator->errors());
        }

        $username = $input['username'];
        $password = $input['password'];
        $isRemember = true;

        // if(!isset($input['is_remember'])){
        //     $isRemember = false;
        // }
        if ( Auth::attempt(['username' => $username, 'password' => $password], $isRemember) ) {
            $user = Auth::user();
            $request->session()->regenerate();
            $previous_session = $user->session_id;
            if ($previous_session) {
                if ( Session::getHandler()->destroy($previous_session) ) {
                    
                }
            }

            $user->session_id = Session::getId();
            $user->save();
                        
            if ( $user->role_id ) {
                return redirect('/admin/dashboard');                   
            }
            else {
                $studentType = Member::getStudentType();
                if(Member::checkIsChosenRole($user->id, $studentType)){                    
                    return redirect('/account');
                }                                        
                return redirect('/');
            }                 
        }

        return redirect('/login')->with('error', "Mật khẩu không đúng");
    }

    public function logout ()
    {
        if (Auth::check()) {
            Auth::logout();
        }

        return redirect('/')->with('signout_ohere', "Signout applycation");
    }

    public function showLoginForm()
    {
        $title = \Config::get('constants.title.login_page');
        $categories = CourseCategoryServices::getCategoryParents();  
        $courses = CourseCategoryServices::getCourseData();   
        $newest_data = CourseCategoryServices::getRandomNewestData(\Config::get('constants.data.default_number_of_courses'));  
        $configs = GeneralConfigServices::loadConfigs(); 
        $user = Auth::user();      
        if (Auth::check()) {
            return redirect('/')->with('success', 'Bạn đã login');
        }
        return view('client.dang-nhap.index')->with(compact('title', 'categories', 'courses', 'user', 'newest_data', 'configs'));
    }
}
