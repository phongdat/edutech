<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Services\UserServices;
use App\Http\Services\CourseCategoryServices;
use App\Http\Services\GeneralConfigServices;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            // 'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'username' => 'required',
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        try {
            $input = $request->all();
            $messages = [
                'name.required' => 'Xin hãy nhập họ và tên.',
                'username.required' => 'Xin hãy nhập số điện thoại.',
            ];
            $validator = Validator::make($input, [ 
                'name' => ['required', 'string', 'max:255'],            
                'username' => 'required',                
            ], $messages);
            
            if ($validator->fails()) {     
                return redirect('register')->with('error', $validator->errors());
            }

            $checkUser = UserServices::getUserByUsername($input['username']);            
            if(!isset($checkUser))
            {                                   
                $linkCreateUser = \Config::get('constants.data.link_create_user');
                return Redirect::to(\Config::get('constants.data.oherecore'). "/client/edutech/register?fullname=" . $input['name'] . "&username=" . $input['username'] . "&address=" . $input['address'] . "&link-create-user=" . $linkCreateUser);
            }
            
            return redirect('register')->with('error', 'Số điện thoại đã được sử dụng. Vui lòng nhập sô điện thoại khác.');

        } catch (\Exception $th) {
            return redirect('register')->with('error', 'Đã có lỗi xảy ra, đăng ký không thành công.');
        }        
    }
}
