<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\News;
use App\Models\Media;

class NewsController extends AdminBaseController
{
    public function index()
    {
        $newses = News::getLoadMoreNews();
        return view('admin.tin-tuc.index', compact('newses'));
    }

    public function new()
    {
    	return view('admin.tin-tuc.update');
    }

    public function update( $id )
    {
        $title = 'Chỉnh sửa tin tức';        
        $news = News::getNewsById($id);
        return view("admin.tin-tuc.update", compact('title', 'news'));
    }

    public function save( Request $request )
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng nhập tiêu đề');
        }

        $news = new News();
        if ($input["id"] != 0) {
            $news = News::find($input['id']);
        } 
        $news->title = $input['title'];
        $news->content = $input['content'];
        $news->summary = $input['summary'];
        $news->user_id = $this->user->id;
        
        if($request->input('tags'))
            $news->tags = implode(',', $request->input('tags'));
        $news->save();

        if ($input['filepath'] != "") {
            $media = Media::where("source", $input['filepath'])->first();
            if (!$media) {
                $media = new Media;
                $media->source = $input['filepath'];
                $media->type = AdminBaseController::$imageType;
                $media->user_id = $this->user->id;
                $media->save();
            }
            $news->banner_media_id = $media->id;
            $news->save();
        }

        if ($input["id"] != 0) {
            return redirect('/admin/newses/update/' . $input["id"])->with("success", 'Cập nhật tin tức thành công');
        }
        return redirect('/admin/newses')->with("success", 'Thêm mới tin tức thành công');        
    }

    public function delete($id)
    {
        $news = News::find($id);
        $news->delete();
        return redirect()->back()->with('success','Xóa thành công');
    }
}
