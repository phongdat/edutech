<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

use App\Models\Member;
use App\Models\Order;
use App\Models\Card;
use App\Models\Product;
use App\Models\Stock;
use Validator;

class SaleAreaController extends AdminBaseController
{
    public function index()
    {
        $members = Member::getLoadMoreSaleArea();
        Order::getTotalCardOnSaleAreas($members);
        $title = "Kinh doanh khu vực";
        $urlNew = 'admin/sales-area/new';
        $urlUpdate = '/admin/sales-area/update/';
        $urlTransfer = '/admin/sales-area/transfer/';
        return view("admin.salearea.index", compact('members', 'urlNew', 'urlUpdate', 'title', 'urlTransfer'));
    }

    public function new()
    {
        $urlSave = '/admin/sales-area/save';
        return view("admin.salearea.update", compact('urlSave'));
    }

    public function update($saleId)
    {
        $member = Member::getMember($saleId);
        $urlSave = '/admin/sales-area/save';
        return view("admin.salearea.update", compact('member', 'urlSave'));
    }

    public function transfer($saleId) 
    {
        $member = Member::getMember($saleId);
        $products = Product::getProductWithCard();        
        $urlBack = "admin/sales-area";
        return view("admin.salearea.transfer", compact('member', 'products', 'urlBack'));
    }

    public function save(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'username' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng nhập Họ tên và tên đăng nhập');
        }

        $member = new Member;
        if ($input['id'] != 0) {
            $member = Member::find($input['id']);
        } else {
            $validator = Validator::make($input, [
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->with('error', 'Vui lòng nhập mật khẩu');
            }
        }
        $member::saveUser($member, $input['username'], $input['password']);
        $member->full_name = $input['name'];
        $member->address = $input['address'];
        $member->member_type_id = Member::getSaleareaType();
        $member->save();

        if ($input['id'] != 0) {
            return redirect()->back()->with('success', 'Cập nhật Kinh doanh khu vực thành công');
        }
        return redirect('admin/sales-area')->with('success', 'Tạo mới Kinh doanh khu vực thành công');
    }

    public function transferSave(Request $req)
    {
        $input = $req->all();
        $cards = Card::take($input['quantity'])->get();
        for ($idxQuantity = 0; $idxQuantity < count($cards); $idxQuantity ++) {
            $order = new Order();
            $order->product_id = $input['product_id'];
            $order->member_id = $input['member_id'];
            $order->card_id = $cards[$idxQuantity]->id;
            $order->status = \Config::get('constants.order_status.transfer_unpaid');
            $order->save();

            //create stock for this tranferred member
            $stock = new Stock();
            $stock->product_id = $input['product_id'];
            $stock->member_id = $input['member_id'];
            $stock->card_id = $cards[$idxQuantity]->id;
            $stock->status = \Config::get('constants.order_status.transfer_unpaid');
            $stock->save();
        }
        return redirect($input['url_back'])->with('success', 'Chuyển thẻ thành công');
    }

    public function paid ( Request $req )
    {
        $input = $req->all();
        $orders = Order::where('member_id', $input['member_id'])->get();
        for ($idxOrder =0; $idxOrder  < count($orders); $idxOrder ++) {
            $orders[$idxOrder]->status = \Config::get('constants.order_status.transfer_paid');
            $orders[$idxOrder]->save();
        }

        //create stock for this tranferred member
        $stocks = Stock::where('member_id', $input['member_id'])->get();
        for ($idxStock = 0; $idxStock  < count($stocks); $idxStock ++) {
            $stocks[$idxStock]->status = \Config::get('constants.order_status.transfer_paid');
            $stocks[$idxStock]->save();
        }

        return redirect()->back()->with('success', 'Thanh toán thành công');
    }

}
