<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

use App\Models\Product;
use App\Models\CourseCategory;
use App\Models\Card;
use App\Models\CardConfigure;
use App\Models\ProductCardAssignment;
use App\Models\ProductCourseAssignment;
use Validator;

class ProductController extends AdminBaseController
{

    public function index() 
    {
        $products = Product::getLoadMoreProductNormal();
        return view("admin.product.index", compact('products'));
    }

    public function new()
    {
        $coures = CourseCategory::getCourseCategory();
        $cards = Card::getCardCanChoose();
        $config = CardConfigure::getLastConfig();
        return view("admin.product.update", compact('coures', 'cards', 'config'));
    }

    public function update( $prodId )
    {
        $product = Product::find( $prodId );
        $prodAssigns = ProductCardAssignment::getCardIdByProdId( $prodId );
        $coures = CourseCategory::getCourseSelectByProdId($prodId );
        $cards = Card::getCardCanChoose();
        $config = CardConfigure::getLastConfig();
        return view("admin.product.update", compact('product', 'coures', 'cards', 'prodAssigns', 'config'));
    }

    public function save(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng nhập tên sản phẩm');
        }

        if ($input['id'] != 0) {
            $prod = Product::find($input['id']);
            $prod->name = $input['name'];
            $prod->save();
            // ProductCourseAssignment::updateCateCourse($input['course_category_id'], $prod->id);

            if ($input['quantity'] != 0) {
                $cardIds = Card::createNewCards($input['quantity'], $prod->id);

                for ($idxProdAssign = 0; $idxProdAssign < count($cardIds); $idxProdAssign++) {
                    $prodAssign = new ProductCardAssignment;
                    $prodAssign->card_id = $cardIds[$idxProdAssign];
                    $prodAssign->product_id = $prod->id;
                    $prodAssign->save();
                }

            }

            return redirect()->back()->with('success', 'Cập nhật sản phẩm thành công');
        }

        $prod = new Product;
        $prod->name = $input['name'];
        $prod->save();
        $cardIds = Card::createNewCards($input['quantity'], $prod->id);

        for ($idxCourseCate = 0; $idxCourseCate  < count($input['course_category_id']); $idxCourseCate ++) {
            $prodCourseAssgn = new ProductCourseAssignment();
            $prodCourseAssgn->course_category_id = $input['course_category_id'][$idxCourseCate];
            $prodCourseAssgn->product_id = $prod->id;
            $prodCourseAssgn->save();
        }

        for ($idxProdAssign=0; $idxProdAssign < count($cardIds); $idxProdAssign++) {
            $prodAssign = new ProductCardAssignment;
            $prodAssign->card_id = $cardIds[$idxProdAssign];
            $prodAssign->product_id = $prod->id;
            $prodAssign->save();
        }

        return redirect('admin/products')->with('success', 'Tạo mới sản phẩm thành công');
    }
}
