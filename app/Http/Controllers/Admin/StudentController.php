<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Validator;

use App\Models\Member;
use App\Models\Order;
use App\Models\Card;


class StudentController extends AdminBaseController
{
    public function index()
    {
    	$students = Member::getLoadMoreStudent();			
	   	return view('admin.student.index', compact('students'));
    }

    public function new()
    {
    	return view('admin.student.update');
    }

    public function save( Request $request )
    {
    	$input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'username' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng nhập Họ tên và tên đăng nhập');
        }

        $member = new Member;
        if ($input['id'] != 0) {
            $member = Member::find($input['id']);
        } else {
            $validator = Validator::make($input, [
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->with('error', 'Vui lòng nhập mật khẩu');
            }
        }
        $member::saveUser($member, $input['username'], $input['password']);
        $member->full_name = $input['name'];
        $member->address = $input['address'];
        $member->member_type_id = Member::getStudentType();
        if ($input['parent'] != 0)
            $member->parent_id = $input['parent'];
        $member->save();

        if ($input['id'] != 0) {
            return redirect('admin/students')->with('success', 'Cập nhật User thành công');
        }
        return redirect('admin/students')->with('success', 'Tạo mới User thành công');
    }

    public function update($studentId)
    {
    	$member = Member::getMember($studentId);
    	return view('admin/student/update', compact('member'));
    }

    public function deactive(Request $request)
    {
    	$member = Member::find($request->id);
        $member->status = $member->status ? false : true;
        $member->save();
        return redirect()->back()->with('success', 'Cập nhật trạng thái Giáo viên thành công');
    }

    public function listCard($studentId)
    {
        $member = Member::find($studentId);
        $orders = Order::with('card')->where('member_id',$studentId)->get();        
    	return view('admin.student.listcard',compact('orders','member'));
    }

    public function delete($studentId)
    {
        // $member = Member::find($studentId);
        // $member->delete();
        // return redirect()->back()->with('success','Xóa thành công');
    }
}
