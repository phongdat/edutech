<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

use Validator;

use App\Models\Page;
use App\Models\GeneralPageConfig;

class GeneralPageConfigController extends AdminBaseController
{
    public function index()
    {
        $currentUrl = Route::getFacadeRoot()->current()->uri();
        $curPage = Page::getPageByPath($currentUrl);        
        $curPageConfig = GeneralPageConfig::getCurrentPage($curPage);
        return view("admin.general-page-config.index",  compact('curPageConfig', 'curPage'));
    }
    
    public function save(Request $req)
    {
        $input = $req->all();

        $validator = Validator::make($input, [
            'title' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng nhập tiêu đề');
        }
        $curPage = Page::find($input['page-id']);        

        $pageConfig = new GeneralPageConfig();
        if ($input['id'] != 0) {
            $pageConfig = GeneralPageConfig::find($input['id']);
        }
        $pageConfig->title = $input['title'];
        $pageConfig->content = $input['content'];        
        $pageConfig->page_id = $input['page-id'];
        $pageConfig->save();

        if ($input['id'] != 0) {
            return redirect()->back()->with('success', 'Cập nhật cấu hình trang chung thành công');
        }
        return redirect()->back()->with('success', 'Tạo mới cấu hình trang chung thành công');
    }
}
