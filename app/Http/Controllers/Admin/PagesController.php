<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

use Validator;

use App\Models\Page;
use App\Models\Role;
use App\Models\AccessControl;
use App\Models\AccessResource;
use App\Models\User;

class PagesController extends AdminBaseController
{
    public function index()
    {
        $pages = Page::get();
        return view("admin.pages.index", compact('pages'));
    }

    public function new()
    {
        $roles = Role::getWhereNotRoot();
        $controls = $this->getControls();
        $parents = Page::getParents();
        return view("admin.pages.update", compact('roles', 'controls', 'parents'));
    }

    public function update($pageId)
    {
        $page = Page::find($pageId);
        $roles = Role::getWhereNotRoot();
        $controls = $this->getControls();
        $accessResources = AccessResource::getByPageId($pageId);
        $accessControls = AccessControl::getACByARList($accessResources);
        $parents = Page::getParents();
        return view("admin.pages.update", compact('page', 'controls', 'roles', 'accessResources', 'accessControls', 'parents'));
    }

    public function save(Request $req)
    {
        $input = $req->all();

        $validator = Validator::make($input, [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng nhập tên chức năng');
        }

        $page = new Page;
        if ($input['id'] != 0) {
            $page = Page::find($input['id']);
        }
        $page->name = $input['name'];
        $page->slugify = $input['slugify'];
        $page->description = $input['description'];
        $page->icon =  $input['icons'];
        if ( $input['parent'] != 0) {
            $page->parent_id = $input['parent'];
        }
        $page->save();

        if ($input['id'] != 0 ) {
            $arList = AccessResource::getByPageId($page->id);
            for ($idxControl = 0; $idxControl < count($arList); $idxControl++) {
                $arList[$idxControl]->description = $arList[$idxControl]->name . " " . $page->name;
                $arList[$idxControl]->save();
            }
            
            for ($idxRoles = 0; $idxRoles < count($input['role_id']); $idxRoles++) {
                for ($idxAr = 0; $idxAr < count($arList); $idxAr++) {
                    $accessControl = AccessControl::where([
                        ['role_id', $input['role_id'][$idxRoles]],
                        ['access_resource_id', $arList[$idxAr]->id],
                    ])->first();
                    if ( $accessControl ) {
                        $accessControl->status = $req->has($input['role_id'][$idxRoles] . '-' . $arList[$idxAr]->control_id) ? true : false;
                        $accessControl->save();
                    } else {
                        $accessControl = new AccessControl;
                        $accessControl->role_id = $input['role_id'][$idxRoles];
                        $accessControl->access_resource_id = $arList[$idxAr]->id;
                        $accessControl->status = $req->has($input['role_id'][$idxRoles] . '-' . $arList[$idxAr]->control_id) ? true : false;
                        $accessControl->save();
                    }
                    
                }
            }
        } else {
            $controls = $this->getControls();

            for ($idxControl = 0; $idxControl < count($controls); $idxControl++) {
                $ar = new AccessResource;
                $ar->page_id = $page->id;
                $ar->control_id = $controls[$idxControl]->id;
                $ar->name = $controls[$idxControl]->name;
                $ar->description = $controls[$idxControl]->name . " " . $page->name;
                $ar->save();
            }
            $arList = AccessResource::getByPageId($page->id);
            for ($idxAr = 0; $idxAr < count($arList); $idxAr++) {
                $accessControl = new AccessControl;
                $accessControl->role_id = 1;
                $accessControl->access_resource_id = $arList[$idxAr]->id;
                $accessControl->status = true;
                $accessControl->save();
            }
            for ($idxRoles = 0; $idxRoles < count($input['role_id']); $idxRoles++) {
                for ($idxAr = 0; $idxAr < count($arList); $idxAr++) {
                    $accessControl = new AccessControl;
                    $accessControl->role_id = $input['role_id'][$idxRoles];
                    $accessControl->access_resource_id = $arList[$idxAr]->id;
                    $accessControl->status = $req->has($input['role_id'][$idxRoles] . '-' . $arList[$idxAr]->control_id) ? true : false;
                    $accessControl->save();
                }
            }
        }
        

        if ($input['id'] != 0) {
            return redirect()->back()->with('success', 'Cập nhật Chức năng thành công');
        }
        return redirect('admin/functions')->with('success', 'Tạo mới Chức năng thành công');
    }

    public function search(Request $req)
    {
        $users = User::where('username', $req->username)->get();
        return view("admin.pages.index", compact('users'));
    }

    public function delete($pageId)
    {
        $page = Page::find($pageId);
        $page->delete();
        return redirect('admin/functions')->with('success', 'Xoá functions thành công');
    }

    private function getControls () 
    {
        $controls = [];
        
        array_push($controls, $this->createControl(1, "View"));
        array_push($controls, $this->createControl(2, "Edit"));
        array_push($controls, $this->createControl(3, "Add"));
        
        return $controls;
    }

    private function createControl ( $id = 1, $name = "" )
    {
        $control = new \stdClass();
        $control->id = $id;
        $control->name = $name;
        return $control;
    }

    
}
