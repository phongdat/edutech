<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

use App\Models\CourseCategory;
use Validator;

class ObjectController extends AdminBaseController
{
    public function object()
    {
        $objects = CourseCategory::where('parent_id', null)->get();
        return view('admin.categorycourse.object', compact('objects'));
    }

    public function save(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng nhập tên nhóm');
        }

        if ($input['object_id'] != 0) {
            $courseCate = CourseCategory::find($input['object_id']);
            $courseCate->name = $input['name'];
            $courseCate->description = $input['description'];
            $courseCate->save();
            return redirect(url('admin/object'))->with('success', 'Cập nhật đối tượng thành công');
        }
        $courseCate = new CourseCategory;
        $courseCate->name = $input['name'];
        $courseCate->description = $input['description'];
        $courseCate->save();

        return redirect()->back()->with('success', 'Tạo mới đối tượng thành công');
    }

    public function update($objectId)
    {
        $objects = CourseCategory::where('parent_id', null)->get();
        $object = CourseCategory::find($objectId);
        return view("admin.categorycourse.object", compact('objects','object'));
    }

    public function delete($objectId)
    {
        $object = CourseCategory::find($objectId)->delete();
        return redirect()->back()->with('success','Xóa đối tượng thành công');
    }
}
