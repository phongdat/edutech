<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Maatwebsite\Excel\Facades\Excel;
use App\Excels\ExcelImport;

use App\Models\Card;
use App\Models\CardImport;
use App\Helper\HpRandom;
use Validator;
use File;
use Artisan;

class CardController extends AdminBaseController
{
    public function index()
    {
        $cards = Card::orderBy('created_at', 'desc')->paginate(50);
        return view("admin.card.index", compact('cards'));
    }

    public function new()
    {
        return view("admin.card.new");
    }

    public function search ( Request $request )
    {
        $cards = Card::getBySerial( $request->serial );
        return view("admin.card.index", compact('cards'));
    }

    public function save(Request $request)
    {
        $cardCount = Card::count();
        if ( $cardCount == 0 )
            $cardCount = 1;

        $quantity = $request->quantity;
        $fstSerial = $request->str_serial;
        $cards = [];

        for ($idxCard = 0; $idxCard < $quantity; $idxCard++) {
            Card::create([
                'number' => HpRandom::randCardNum(AdminBaseController::$cardNumber ),
                'series' => HpRandom::randCardSerial( $fstSerial, $cardCount, AdminBaseController::$serialNumber),
                'type' => 'auto',
            ]);
            $cardCount++;
        }
        return redirect("/admin/cards")->with("success", "Tạo thẻ thành công");

    }

    public function import(Request $req)
    {
        $input = $req->all();

        $import = new CardImport;
        $import->user_id = $this->user->id;
        $import->source = AdminBaseController::$urlVideo . parse_url($input['card_import'])['path'];
        $import->type = $input['card_import_extension'];
        $import->save();
        
        define('ARTISAN_BINARY', base_path() . '/artisan');
        call_in_background('import:excel');
        return redirect()->back()->with('success', 'Đã xử lý Import vui lòng không import nữa trong 30 phút tiếp theo');
    }

}
