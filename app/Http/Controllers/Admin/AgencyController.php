<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

use App\Models\Member;
use App\Models\Order;
use App\Models\Card;
use App\Models\Product;
use Validator;

class AgencyController extends AdminBaseController
{
    public function index()
    {
        $members = Member::getLoadMoreAgency();
        Order::getTotalCardOnSaleAreas($members);
        $title = "Đại lý";
        $urlNew = 'admin/agency/new';
        $urlUpdate = '/admin/agency/update/';
        $urlTransfer = '/admin/agency/transfer/';
        return view("admin.salearea.index", compact('members', 'urlNew', 'urlUpdate', 'title', 'urlTransfer'));
    }

    public function new()
    {
        $urlSave = '/admin/agency/save';
        return view("admin.salearea.update", compact('urlSave'));
    }

    public function update($saleId)
    {
        $member = Member::getMember($saleId);
        $urlSave = '/admin/agency/save';
        return view("admin.salearea.update", compact('member', 'urlSave'));
    }

    public function transfer($saleId)
    {
        $member = Member::getMember($saleId);
        $products = Product::getProductWithCard();
        $urlBack = "admin/agency";
        return view("admin.salearea.transfer", compact('member', 'products', 'urlBack'));
    }

    public function save(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'username' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng nhập Họ tên và tên đăng nhập');
        }

        $member = new Member;
        if ($input['id'] != 0) {
            $member = Member::find($input['id']);
        } else {
            $validator = Validator::make($input, [
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->with('error', 'Vui lòng nhập mật khẩu');
            }
        }
        $member::saveUser($member, $input['username'], $input['password']);
        $member->full_name = $input['name'];
        $member->address = $input['address'];
        $member->member_type_id = Member::getAgencyType();
        $member->save();

        if ($input['id'] != 0) {
            return redirect()->back()->with('success', 'Cập nhật Nhà phân phối thành công');
        }
        return redirect('admin/agency')->with('success', 'Tạo mới Nhà phân phối thành công');
    }
}
