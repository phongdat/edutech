<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

use Validator;

use App\Models\BaseModel;
use App\Models\GeneralConfig;

class GeneralWebConfigController extends AdminBaseController
{
    public function index()
    {
        $data =  GeneralConfig::get();        
        return view("admin.web-config.index", compact('data'));
    }

    public function new()
    {
        $title = 'Thêm cấu hình';
        $asset = BaseModel::$types;
        $data = null;
        return view("admin.web-config.update", compact('title', 'asset', 'data'));
    }

    public function update( $id )
    {
        $title = 'Chỉnh sửa cấu hình';
        $asset = BaseModel::$types;
        $data = GeneralConfig::find($id);
        return view("admin.web-config.update", compact('title', 'asset', 'data'));
    }

    public function save( Request $request )
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name_config' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng nhập tên cấu hình');
        }

        $config = new GeneralConfig();
        if ($input['id'] != 0) {
            $config = GeneralConfig::find($input['id']);
        }

        $config->name = str_slug($input['name_config']);        
        $config->value = $input[$input['type']];
        if($input['type'] == 'image'){
            $config->value = str_replace(url('/'), '', $input[$input['type']]);
        }
        $config->type = $input['type'];                
        $config->save();

        if ($input["id"] != 0) {
            return redirect('/admin/webconfig/update/' . $input["id"])->with("success", 'Cập nhật cấu hình thành công');
        }
        return redirect('/admin/webconfig')->with("success", 'Thêm mới cấu hình thành công');        
    }

    public function delete( Request $request )
    {
        $config = GeneralConfig::find($request->id);
        $config->delete();
        return redirect('/admin/webconfig')->with("success", 'Xoá thành công cấu hình');
    }
}