<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

use App\Models\Product;
use App\Models\Course;
use App\Models\Card;
use App\Models\CardConfigure;
use App\Models\ProductCardAssignment;
use Validator;

class ProductSpecialController extends AdminBaseController
{

    public function index()
    {
        $products = Product::getProductSpecial();
        return view("admin.product-special.index", compact('products'));
    }

    public function new()
    {
        $coures = Course::get();
        $cards = Card::getCardCanChoose();
        $config = CardConfigure::getLastConfig();
        return view("admin.product.update", compact('coures', 'cards', 'config'));
    }

    public function update($prodId)
    {
        $product = Product::find($prodId);
        $prodAssigns = ProductCardAssignment::getCardIdByProdId($prodId);
        $coures = Course::get();
        $cards = Card::getCardCanChoose();
        $config = CardConfigure::getLastConfig();
        return view("admin.product.update", compact('product', 'coures', 'cards', 'prodAssigns', 'config'));
    }

    public function save(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng nhập tên sản phẩm');
        }

        if ($input['id'] != 0) {
            $prod = Product::find($input['id']);
            $prod->name = $input['name'];
            $prod->course_id = $input['course_id'];
            $prod->save();

            if ($input['quantity'] != 0) {
                $cardIds = Card::createNewCards($input['quantity'], $prod->id);

                for ($idxProdAssign = 0; $idxProdAssign < count($cardIds); $idxProdAssign++) {
                    $prodAssign = new ProductCardAssignment;
                    $prodAssign->card_id = $cardIds[$idxProdAssign];
                    $prodAssign->product_id = $prod->id;
                    $prodAssign->save();
                }
            }

            return redirect()->back()->with('success', 'Cập nhật sản phẩm thành công');
        }

        $prod = new Product;
        $prod->name = $input['name'];
        $prod->course_id = $input['course_id'];
        $prod->save();
        $cardIds = Card::createNewCards($input['quantity'], $prod->id);

        for ($idxProdAssign = 0; $idxProdAssign < count($cardIds); $idxProdAssign++) {
            $prodAssign = new ProductCardAssignment;
            $prodAssign->card_id = $cardIds[$idxProdAssign];
            $prodAssign->product_id = $prod->id;
            $prodAssign->save();
        }

        return redirect('admin/products')->with('success', 'Tạo mới sản phẩm thành công');
    }
}
