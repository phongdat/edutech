<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

use Validator;

use App\Models\Role;

class RolesController extends AdminBaseController
{
    public function index()
    {
        $roles = Role::get();
        return view("admin.roles.index", compact('roles'));
    }

    public function new()
    {
        return view("admin.roles.update");
    }

    public function update ( $roleId ) 
    {
        $role = Role::find($roleId);
        return view("admin.roles.update", compact('role'));
    }

    public function save(Request $req)
    {
        $input = $req->all();
        $validator = Validator::make($input, [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng nhập tên nhóm');
        }

        if ( $input['id'] != 0 ) {
            $role = Role::find($input['id']);
            $role->name = $input['name'];
            $role->save();
            return redirect()->back()->with('success', 'Cập nhật Groups thành công');
        }
        $role = new Role;
        $role->name = $input['name'];
        $role->save();

        return redirect('admin/roles')->with('success', 'Tạo mới Groups thành công');
    }
}
