<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Controllers\Client\BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\Media;
use App\Models\Member;
use App\Excels\CourseExport;
use App\Excels\CourseImport;
use App\Excels\CourseExportWithView;
use Validator;
use File;
use Storage;
use Youtube;

class CourseController extends AdminBaseController
{
    public function index()
    {
        $courses = Course::getLoadMoreCourse();   
        $categories = Course::getAllCourseWithCate();
        $objects = CourseCategory::getParents();
        $classes = CourseCategory::getCourseCategory();
        $index = $courses->currentPage() * $courses->perPage() - $courses->perPage();
        return view("admin.course.index", compact('courses', 'index', 'categories', 'objects', 'classes'));
    }

    public function new()
    {
        $categories = CourseCategory::getCourseCategory();
        $teachers = Member::getTeachers();
        return view("admin.course.update", compact('categories', 'teachers'));
    }

    public function update( $courseId )
    {
        $course = Course::getCourseById( $courseId );
        $categories = CourseCategory::getCourseCategory();
        $teachers = Member::getTeachers();
        return view("admin.course.update", compact('categories', 'course', 'teachers'));
    }

    function search(Request $request)
    {
        $courses = Course::where('title', 'like', '%' . $request->name . '%')->latest()->paginate(5);    
        for ($idxCourse = 0; $idxCourse  < count($courses); $idxCourse ++) { 
            if ( $courses[$idxCourse]->banner_media_id ) {
                $courses[$idxCourse]->banner = Media::getBanner($courses[$idxCourse]->banner_media_id);
            }
            if ($courses[$idxCourse]->video_media_id) {
                $courses[$idxCourse]->video = Media::getVideo($courses[$idxCourse]->video_media_id);
            }
            if ( $courses[$idxCourse]->user_id ) {
                $courses[$idxCourse]->teacher_name = Member::find($courses[$idxCourse]->user_id)->full_name;
            }
        }
        $categories = Course::getAllCourseWithCate();
        $objects = CourseCategory::getParents();
        $classes = CourseCategory::getCourseCategory();
        
        $index = $courses->currentPage() * $courses->perPage() - $courses->perPage();                
        $name = $request->name;
        return view('admin.course.index', compact('courses', 'name', 'index', 'categories', 'objects', 'classes'));
    }

    public function searchPage( Request $request )
    {        
        $categories = Course::getAllCourseWithCate();
        $objects = CourseCategory::getParents();
        $classes = CourseCategory::getCourseCategory();

        if(request()->has('value')){
            $name = request()->value;
            $courses = Course::where('title', 'like', '%' . $name . '%')->latest()->paginate(5); 
            for ($idxCourse = 0; $idxCourse  < count($courses); $idxCourse ++) { 
                if ( $courses[$idxCourse]->banner_media_id ) {
                    $courses[$idxCourse]->banner = Media::getBanner($courses[$idxCourse]->banner_media_id);
                }
                if ($courses[$idxCourse]->video_media_id) {
                    $courses[$idxCourse]->video = Media::getVideo($courses[$idxCourse]->video_media_id);
                }
                if ( $courses[$idxCourse]->user_id ) {
                    $courses[$idxCourse]->teacher_name = Member::find($courses[$idxCourse]->user_id)->full_name;
                }
            }
            $index = $courses->currentPage() * $courses->perPage() - $courses->perPage();               
            return view('admin.course.index', compact('courses', 'name', 'index', 'categories', 'objects', 'classes'));
        }        
        $courses = Course::getLoadMoreCourse();           
        $index = $courses->currentPage() * $courses->perPage() - $courses->perPage();
        return view("admin.course.index", compact('courses', 'index', 'categories', 'objects', 'classes'));
    }

    public function save( Request $request )
    {
        //
        $input = $request->all();

        $course = new Course;
        if ($input["id"] != 0) {
            $course = Course::find($input['id']);
        } 
        $course->title = $input['title'];
        $course->content = $input['content'];
        $course->user_id = $input['teacher'];
        $course->course_category_id = $input['category'];
        if ( $input['filepath'] != "" ) {
            $media = Media::where("source", $input['filepath'])->first();
            if ( !$media ) {
                $media = new Media;
                $media->source = $input['filepath'];
                $media->type = AdminBaseController::$imageType;
                $media->user_id = $this->user->id;
                $media->save();
            }
            $course->banner_media_id = $media->id;
        }
        $course->save();
        if ($input['video'] != "") {
            $video = parse_url($input['video']);
            $path = AdminBaseController::$urlVideo . $video["path"];
            $media = Media::where("source", $path)->first();
            if (!$media) {
                $media = new Media;
                $media->source = $path;
                $media->type = AdminBaseController::$videoType;
                $media->user_id = $this->user->id;
                $media->save();
            }
            $course->video_media_id = $media->id;
        }

        $course->save();

        if ($input["id"] != 0) {
            return redirect('/admin/courses/update/' . $input["id"])->with("success", 'Cập nhật khoá học thành công');
        }
        return redirect('/admin/courses')->with("success", 'Thêm mới khoá học thành công');
    }

    function delete(Request $request)
    {
        $course = Course::find($request->id);
        $course->delete();
        return redirect('/admin/courses')->with("success", 'Xoá thành công khoá học');
    }

    function export ()
    {
        return Excel::download(new CourseExportWithView, 'course.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    public function filter( Request $request )
    {
        $input = $request->all();
        $filter = true;

        $categories = Course::getAllCourseWithCate();
        $objects = CourseCategory::getParents();
        $classes = CourseCategory::getCourseCategory();

        $object = $input['object'] != 0 ? CourseCategory::find($input['object']) : null;
        $category = $input['category'] != 0 ? Course::find($input['category']) : null;
        $class = $input['classes'] != 0 ? CourseCategory::find($input['classes']) : null;

        $sl_category = $input['category'];
        $sl_object = $input['object'];
        $sl_class = $input['classes'];

        $courses = array();

        if($category && $object && $class)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();

            $courses = Course::where('id', $input['category'])
                                    ->orWhere('course_category_id', $sl_class)
                                    ->orWhereIn('course_category_id', $cateBelongObjs)
                                    ->latest()->paginate(5);              
        }
        else if($category && $object)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();

            $courses = Course::where('id', $input['category'])                                    
                                    ->orWhereIn('course_category_id', $cateBelongObjs)
                                    ->latest()->paginate(5);  
        }
        else if($class && $object)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();

            $courses = Course::where('course_category_id', $sl_class)
                                    ->orWhereIn('course_category_id', $cateBelongObjs)
                                    ->latest()->paginate(5);   
        }
        else if($category && $class)
        {
            $courses = Course::where('id', $input['category'])
                                    ->orWhere('course_category_id', $sl_class)
                                    ->latest()->paginate(5);   
        }
        else if($category)
        {
            $courses = Course::where('id', $input['category'])
                                ->latest()->paginate(5);  
        }
        else if($object)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();

            $courses = Course::whereIn('course_category_id', $cateBelongObjs)
                            ->latest()->paginate(5);  
        }
        else
        {
            $courses = Course::where('course_category_id', $sl_class)            
                            ->latest()->paginate(5);  
        }

        for ($idxCourse = 0; $idxCourse  < count($courses); $idxCourse ++) { 
            if ( $courses[$idxCourse]->banner_media_id ) {
                $courses[$idxCourse]->banner = Media::getBanner($courses[$idxCourse]->banner_media_id);
            }
            if ($courses[$idxCourse]->video_media_id) {
                $courses[$idxCourse]->video = Media::getVideo($courses[$idxCourse]->video_media_id);
            }
            if ( $courses[$idxCourse]->user_id ) {
                $courses[$idxCourse]->teacher_name = Member::find($courses[$idxCourse]->user_id)->full_name;
            }
        }
        
        $index = $courses->currentPage() * $courses->perPage() - $courses->perPage();        
            
        return view('admin.course.index', compact('courses'                                                
                                                , 'index'
                                                , 'categories'
                                                , 'objects'
                                                , 'classes'
                                                , 'filter'
                                                , 'sl_category'
                                                , 'sl_object'
                                                , 'sl_class'
                                            ));
    }

    public function filterPage( Request $request )
    {
        $sl_category = request()->cate;
        $sl_object = request()->obj;
        $sl_class = request()->cla;

        $filter = true;

        $categories = Course::getAllCourseWithCate();
        $objects = CourseCategory::getParents();
        $classes = CourseCategory::getCourseCategory();

        $object = $sl_object != 0 ? CourseCategory::find($sl_object) : null;
        $category = $sl_category != 0 ? Course::find($sl_category) : null;
        $class = $sl_class != 0 ? CourseCategory::find($sl_class) : null;

        $courses = array();

        if($category && $object && $class)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();

            $courses = Course::where('id', $sl_category)
                                    ->orWhere('course_category_id', $sl_class)
                                    ->orWhereIn('course_category_id', $cateBelongObjs)
                                    ->latest()->paginate(5);              
        }
        else if($category && $object)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();

            $courses = Course::where('id', $sl_category)                                    
                                    ->orWhereIn('course_category_id', $cateBelongObjs)
                                    ->latest()->paginate(5);  
        }
        else if($class && $object)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();

            $courses = Course::where('course_category_id', $sl_class)
                                    ->orWhereIn('course_category_id', $cateBelongObjs)
                                    ->latest()->paginate(5);   
        }
        else if($category && $class)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();

            $courses = Course::where('id', $sl_category)
                                    ->orWhere('course_category_id', $sl_class)
                                    ->latest()->paginate(5);   
        }
        else if($category)
        {
            $courses = Course::where('id', $sl_category)
                                ->latest()->paginate(5);  
        }
        else if($object)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();

            $courses = Course::whereIn('course_category_id', $cateBelongObjs)
                            ->latest()->paginate(5);  
        }
        else
        {
            $courses = Course::where('course_category_id', $sl_class)            
                            ->latest()->paginate(5);  
        }

        for ($idxCourse = 0; $idxCourse  < count($courses); $idxCourse ++) { 
            if ( $courses[$idxCourse]->banner_media_id ) {
                $courses[$idxCourse]->banner = Media::getBanner($courses[$idxCourse]->banner_media_id);
            }
            if ($courses[$idxCourse]->video_media_id) {
                $courses[$idxCourse]->video = Media::getVideo($courses[$idxCourse]->video_media_id);
            }
            if ( $courses[$idxCourse]->user_id ) {
                $courses[$idxCourse]->teacher_name = Member::find($courses[$idxCourse]->user_id)->full_name;
            }
        }
        
        $index = $courses->currentPage() * $courses->perPage() - $courses->perPage();  

        return view('admin.course.index', compact('courses'                                                
                                                , 'index'
                                                , 'categories'
                                                , 'objects'
                                                , 'classes'
                                                , 'filter'
                                                , 'sl_category'
                                                , 'sl_object'
                                                , 'sl_class'
                                            ));
    }

    public function importView()
    {
        return view('admin.course.import');
    }

    public function import( Request $request )
    {
        Excel::import(new CourseImport, request()->file('file'));
        return back()->with("success", 'Import thành công.');;
    }
}
