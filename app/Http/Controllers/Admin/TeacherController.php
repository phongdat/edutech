<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

use App\Models\Member;

use Validator;

class TeacherController extends AdminBaseController
{

    public function index() 
    {
        $teachers = Member::getLoadMoreTeacher();
        return view("admin.teacher.index", compact('teachers'));
    }

    public function new()
    {
        return view("admin.teacher.update");
    }

    public function update ($teacherId)
    {
        $member = Member::getMember( $teacherId);
        return view("admin.teacher.update", compact('member'));
    }

    function deactive ( Request $request ) 
    {
        $member = Member::find($request->id);
        $member->status = $member->status ? false : true;
        $member->save();
        $teachers = Member::getTeachers();
        return view("admin.teacher.index", compact('teachers'))->with('success', 'Cập nhật trạng thái Giáo viên thành công');
    }

    public function save( Request $request )
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'username' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng nhập Họ tên và tên đăng nhập');
        }

        $member = new Member;
        if ($input['id'] != 0) {
            $member = Member::find($input['id']);
        } else {
            $validator = Validator::make($input, [
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->with('error', 'Vui lòng nhập mật khẩu');
            }
        }
        $member::saveUser($member, $input['username'], $input['password']);
        $member->full_name = $input['name'];
        $member->address = $input['address'];
        $member->member_type_id = Member::getTeacherType();
        if ($input['parent'] != 0)
            $member->parent_id = $input['parent'];
        $member->save();

        if ($input['id'] != 0) {
            return redirect()->back()->with('success', 'Cập nhật User thành công');
        }
        return redirect('admin/teachers')->with('success', 'Tạo mới User thành công');
    }
}
