<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

use App\Models\CourseCategory;
use Validator;

class CategoryCourseController extends AdminBaseController
{
    public function new()
    {
        $parents = CourseCategory::getParents();
        $categories = CourseCategory::where('parent_id', '!=' ,0 )->get();
        return view("admin.categorycourse.update", compact('parents','categories'));
    }

    public function save(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng nhập tên nhóm');
        }

        if ($input['category_id'] != 0) {
            $courseCate = CourseCategory::find($input['category_id']);
            $courseCate->name = $input['name'];
            $courseCate->description = $input['description'];
            $courseCate->save();
            return redirect()->back()->with('success', 'Cập nhật danh mục khoá học thành công');
        }
        $courseCate = new CourseCategory;
        $courseCate->name = $input['name'];
        $courseCate->description = $input['description'];
        $courseCate->parent_id = $input['parent'];
        $courseCate->save();

        return redirect()->back()->with('success', 'Tạo mới danh mục khoá học thành công');
    }

    public function update($categoryId){
        $category = CourseCategory::find($categoryId);
        $parents = CourseCategory::getParents();
        $categories = CourseCategory::where('parent_id', ! null)->get();
        return view("admin.categorycourse.update", compact('parents','categories','category'));
    }

    public function delete($categoryId){
        $category = CourseCategory::find($categoryId)->delete();
        return redirect()->back()->with('success','Xóa đối tượng thành công');
    }   
}
