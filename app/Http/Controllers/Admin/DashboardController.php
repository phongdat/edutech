<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Carbon\Carbon;
use App\Models\Member;
use App\Models\Order;
use App\Models\Card;
use DB;

class DashboardController extends AdminBaseController
{
    public function index () 
    {
    	$month 		= Carbon::now()->month;
    	$members 	= Member::getStudents();
    	$saleareas 	= Member::getSaleAreas();
        Order::getTotalCardOnSaleAreas($saleareas);

    	foreach ($members as $member) {
            if ( $member->created_at )
    		{
                $newstudent = $member->created_at->month;
                $online = $member->updated_at->month;
                $members->count = 0;
                if ($month == $newstudent) {
                    $members->count = count($members);
                }
            }
    	}

    	$active = Order::whereMonth('created_at', $month)->where('status', "active")->get()->count();

    	$cards =  Card::get()->count();
    	$transaction = Order::with('card','member')->where('status', \Config::get('constants.order_status.transfer_unpaid'))->get();
    	$countstrans = count($transaction);
    	$stock = $cards-$countstrans;
    	$paid = Order::where('status', \Config::get('constants.order_status.transfer_paid'))->count();
    	$unpaid = Order::where('status', \Config::get('constants.order_status.transfer_unpaid'))->count();

        
        $month = 1;
        $studNew = [];
        $studOnline = [];
        $studActive = [];
        for($i = $month; $i < 13; $i++) {
        //count new students
        $newStudents = Member::where('member_type_id',1)->whereMonth('created_at', $i)->get();
        $studNew[$i] = $newStudents->count(); 

        //students online
        $onlStudents = Member::where('member_type_id',1)->whereMonth('updated_at', $i)->get();
        $studOnline[$i] = $onlStudents->count(); 

        // count card active
        $act = Order::where('status', "active")->whereMonth('updated_at', $i)->get();
        $studActive[$i] = $act->count(); 
        }

        $monthNewStudent = json_encode($studNew);
        $monthOnline = json_encode($studOnline);
        $monthActive = json_encode($studActive);
        return view('admin.dashboard.index', compact('members',
        											'active',
        											'saleareas',
        											'transaction',
        											'stock',
        											'countstrans', 
        											'paid', 
        											'unpaid',
                                                    'monthNewStudent',
                                                    'monthOnline',
                                                    'monthActive'));
    }
}
