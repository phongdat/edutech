<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Hash;

use Validator;

use App\Models\User;
use App\Models\Role;

class UsersController extends AdminBaseController
{
    public function index()
    {
        $users = User::getUsersWithRole();
        Role::addRoleNameToUsers($users);
        return view("admin.users.index", compact('users'));
    }

    public function new()
    {
        $roles = Role::getWhereNotRoot();
        return view("admin.users.update", compact('roles'));
    }

    public function update($userId)
    {
        $user = User::find($userId);
        $roles = Role::get();
        return view("admin.users.update", compact('user', 'roles'));
    }

    public function save(Request $req)
    {
        $input = $req->all();
        $validator = Validator::make($input, [
            'username' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng nhập username và password');
        }

        $role = new User;
        if ($input['id'] != 0) {
            $role = User::find($input['id']);
            if ($input['password'] != "")
                $role->password = Hash::make($input['password']);
        } else {
            $role->password = Hash::make($input['password']);
        }
        $role->username = $input['username'];
        $role->email = $input['email'];
        $role->role_id = $input['role'];
        $role->save();

        if ($input['id'] != 0) {
            return redirect()->back()->with('success', 'Cập nhật User thành công');
        }
        return redirect('admin/users')->with('success', 'Tạo mới User thành công');
    }

    public function search( Request $req )
    {
        $users = User::where('username', $req->username)->get();
        return view("admin.users.index", compact('users'));
    }
}
