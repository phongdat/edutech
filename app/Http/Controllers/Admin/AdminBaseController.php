<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Redirector;

use View;
use Redirect;

use App\Models\Page;

class AdminBaseController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected static $serialNumber = 11;
    protected static $cardNumber = 13;
    protected static $imageType = 1;
    protected static $videoType = 2;
    protected static $urlVideo = 'tus-node-server';

    public $user;

    public function __construct(Request $request, Redirector $redirect)
    {
        $this->middleware(function ($request, $next) {
            $this->init();
            return $next($request);
        });
    }

    /* PUBLIC FUNCTION */
    public function init()
    {
        $this->user = Auth::user();
        if ($this->user) {
            if ( !$this->user->role_id ) {
                return Redirect::to("/")->send();
            }
            $pages = Page::getWithPAC();
            View::share('_pages', $pages);
        } else {
            return Redirect::to("/")->send();
        }
    }
}
