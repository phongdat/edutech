<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Controllers\Client\BaseController;
use App\Models\Lesson;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\RatingLesson;
use App\Models\Media;
use App\Models\Member;
use App\Models\Notification;
use App\Models\NotificationDetail;
use Validator;
use File;
use Storage;
use Youtube;

class LessonController extends AdminBaseController
{

    function index()
    {
        $lessons = Lesson::getLoadMoreLesson();        
        $categories = Course::getAllCourseWithCate();
        $objects = CourseCategory::getParents();
        $classes = CourseCategory::getCourseCategory();
        // $courseBelongCates = Course::select('id')->where('course_category_id', 20)->get();

        // $lessons = Lesson::whereIn('course_id', $courseBelongCates)                                    
        //                         ->latest()->paginate(5);
        //                         dd($lessons);
        return view('admin.lessons.index', compact('lessons', 'categories', 'objects', 'classes'));
    }

    function new()
    {
        $courses = Course::get();
        $teachers = Member::getTeachers();
    	return view('admin.lessons.update', compact('courses', 'teachers'));
    }

    function update ( $lessonId )
    {
        $lesson = Lesson::getById($lessonId);
        $courses = Course::get();
        $teachers = Member::getTeachers();
        $rating_members = RatingLesson::where( 'lesson_id', $lessonId )->get();
        return view('admin.lessons.update', compact( 'lesson'
                                                    , 'courses'
                                                    , 'teachers'
                                                    , 'rating_members' ));
    }
    
    function search (Request $request)
    {
        $lessons = Lesson::where('title', 'like' , '%' . $request->name . '%')->latest()->paginate(5);        
        $categories = Course::getAllCourseWithCate();
        $objects = CourseCategory::getParents();
        $classes = CourseCategory::getCourseCategory();
        $name = $request->name;
        return view('admin.lessons.index', compact('lessons', 'name', 'categories', 'objects', 'classes'));
    }

    public function searchPage( Request $request )
    {        
        $categories = Course::getAllCourseWithCate();
        $objects = CourseCategory::getParents();
        $classes = CourseCategory::getCourseCategory();

        if(request()->has('value')){
            $name = request()->value;
            $lessons = Lesson::where('title', 'like' , '%' . $request->name . '%')->latest()->paginate(5);                    
            return view('admin.lessons.index', compact('lessons', 'name', 'categories', 'objects', 'classes'));
        }        
        $lessons = Lesson::getLoadMoreLesson();
        return view('admin.lessons.index', compact('lessons', 'categories', 'objects', 'classes'));
    }

    function save(Request $request)
    {
        $input = $request->all();

        $lesson = new Lesson;
        if ($input["id"] != 0) {
            $lesson = Lesson::find($input['id']);
        } 
        $lesson->title = $input['title'];
        $lesson->content = $input['content'];
        $lesson->course_id = $input['course_id'];
        $lesson->user_id = $input['teacher'];
        $lesson->tags = implode(',', $request->input('tags'));
        $lesson->status = \Config::get('constants.lesson_status.pending');  
        $lesson->save();

        if ($input['filepath'] != "") {
            $media = Media::where("source", $input['filepath'])->first();
            if (!$media) {
                $media = new Media;
                $media->source = $input['filepath'];
                $media->type = AdminBaseController::$imageType;
                $media->user_id = $this->user->id;
                $media->save();
            }
            $lesson->banner_media_id = $media->id;
        }
        $lesson->save();
        if ($input['video'] != "") {
            $video = parse_url($input['video']);
            $path = AdminBaseController::$urlVideo . $video["path"];
            $media = Media::where("source", $path)->first();
            if (!$media) {
                $media = new Media;
                $media->source = $path;
                $media->type = AdminBaseController::$videoType;
                $media->user_id = $this->user->id;
                $media->save();
            }
            $lesson->video_media_id = $media->id;
        }
        $lesson->save();

        $approver_ids = $request->input( 'approvers' );
        if( $approver_ids )
        {
            $cur_approver_ids = RatingLesson::select( 'member_id' )
                                            ->where( 'lesson_id', '=', $lesson->id )
                                            ->forceDelete();

            for ( $i=0; $i < sizeof( $approver_ids ); $i++ ) {             
                $rating_data = new RatingLesson();
                $rating_data->lesson_id = $lesson->id;
                $rating_data->member_id = $approver_ids[$i];                        
                $rating_data->save();
            }
        }        

        if ($input["id"] != 0) {
            return redirect('/admin/lessons/update/' . $input["id"])->with("success", 'Cập nhật bài giảng thành công');
        }
        return redirect('/admin/lessons')->with("success", 'Thêm mới bài giảng thành công');
    }

    function delete ( Request $request )
    {
        $lesson = Lesson::getById($request->id);
        $rating_lesson = RatingLesson::where('lesson_id', $request->id)->delete();
        $lesson->delete();
        return redirect('/admin/lessons')->with("success", 'Xoá thành công bài giảng');
    }

    public function filter( Request $request )
    {
        $input = $request->all();
        $filter = true;

        $categories = Course::getAllCourseWithCate();
        $objects = CourseCategory::getParents();
        $classes = CourseCategory::getCourseCategory();

        $object = $input['object'] != 0 ? CourseCategory::find($input['object']) : null;
        $category = $input['category'] != 0 ? Course::find($input['category']) : null;
        $class = $input['classes'] != 0 ? CourseCategory::find($input['classes']) : null;

        $sl_category = $input['category'];
        $sl_object = $input['object'];
        $sl_class = $input['classes'];

        $lessons = array();

        if($category && $object && $class)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();
            $courseBelongCates = Course::select('id')->where('course_category_id', $sl_class)->get();
            $courseBelongObjs = Course::select('id')->whereIn('course_category_id', $cateBelongObjs)->get();

            $lessons = Lesson::where('course_id', $input['category'])                                    
                                    ->orWhereIn('course_id', $courseBelongCates)
                                    ->orWhereIn('course_id', $courseBelongObjs)
                                    ->latest()->paginate(5);              
        }
        else if($category && $object)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();
            $courseBelongObjs = Course::select('id')->whereIn('course_category_id', $cateBelongObjs)->get();

            $lessons = Lesson::where('course_id', $input['category'])                                                                        
                                    ->orWhereIn('course_id', $courseBelongObjs)
                                    ->latest()->paginate(5);  
        }
        else if($class && $object)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();
            $courseBelongCates = Course::select('id')->where('course_category_id', $sl_class)->get();
            $courseBelongObjs = Course::select('id')->whereIn('course_category_id', $cateBelongObjs)->get();

            $lessons = Lesson::whereIn('course_id', $courseBelongCates)
                                    ->orWhereIn('course_id', $courseBelongObjs)
                                    ->latest()->paginate(5);
        }
        else if($category && $class)
        {            
            $courseBelongCates = Course::select('id')->where('course_category_id', $sl_class)->get();
            
            $lessons = Lesson::where('course_id', $input['category'])                                    
                                    ->orWhereIn('course_id', $courseBelongCates)                                    
                                    ->latest()->paginate(5);
        }
        else if($category)
        {
            $lessons = Lesson::where('course_id', $input['category'])                                                                       
                                    ->latest()->paginate(5);
        }
        else if($object)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();            
            
            $courseBelongObjs = Course::select('id')->whereIn('course_category_id', $cateBelongObjs)->get();

            $lessons = Lesson::whereIn('course_id', $courseBelongObjs)
                                    ->latest()->paginate(5); 
        }
        else
        {            
            $courseBelongCates = Course::select('id')->where('course_category_id', $sl_class)->get();

            $lessons = Lesson::whereIn('course_id', $courseBelongCates)                                    
                                    ->latest()->paginate(5);
        }

        for ($idxLesson = 0; $idxLesson  < count($lessons); $idxLesson ++) { 
            if ( $lessons[$idxLesson]->banner_media_id ) {
                $lessons[$idxLesson]->banner = Media::getBanner($lessons[$idxLesson]->banner_media_id);
            }
            if ($lessons[$idxLesson]->video_media_id) {
                $lessons[$idxLesson]->video = Media::getVideo($lessons[$idxLesson]->video_media_id);
            }
            if ( $lessons[$idxLesson]->user_id ) {
                $lessons[$idxLesson]->teacher_name = Member::find($lessons[$idxLesson]->user_id)->full_name;
            }
        }
        
        $index = $lessons->currentPage() * $lessons->perPage() - $lessons->perPage();        
            
        return view('admin.lessons.index', compact('lessons'                                                
                                                , 'categories'
                                                , 'objects'
                                                , 'classes'
                                                , 'filter'
                                                , 'sl_category'
                                                , 'sl_object'
                                                , 'sl_class'
                                            ));
    }

    public function filterPage( Request $request )
    {
        $sl_category = request()->cate;
        $sl_object = request()->obj;
        $sl_class = request()->cla;

        $filter = true;

        $categories = Course::getAllCourseWithCate();
        $objects = CourseCategory::getParents();
        $classes = CourseCategory::getCourseCategory();

        $object = $sl_object != 0 ? CourseCategory::find($sl_object) : null;
        $category = $sl_category != 0 ? Course::find($sl_category) : null;
        $class = $sl_class != 0 ? CourseCategory::find($sl_class) : null;

        $lessons = array();

        if($category && $object && $class)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();
            $courseBelongCates = Course::select('id')->where('course_category_id', $sl_class)->get();
            $courseBelongObjs = Course::select('id')->whereIn('course_category_id', $cateBelongObjs)->get();

            $lessons = Lesson::where('course_id', $sl_category)                                    
                                    ->orWhereIn('course_id', $courseBelongCates)
                                    ->orWhereIn('course_id', $courseBelongObjs)
                                    ->latest()->paginate(5);              
        }
        else if($category && $object)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();
            $courseBelongObjs = Course::select('id')->whereIn('course_category_id', $cateBelongObjs)->get();

            $lessons = Lesson::where('course_id', $sl_category)                                                                        
                                    ->orWhereIn('course_id', $courseBelongObjs)
                                    ->latest()->paginate(5);  
        }
        else if($class && $object)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();
            $courseBelongCates = Course::select('id')->where('course_category_id', $sl_class)->get();
            $courseBelongObjs = Course::select('id')->whereIn('course_category_id', $cateBelongObjs)->get();

            $lessons = Lesson::whereIn('course_id', $courseBelongCates)
                                    ->orWhereIn('course_id', $courseBelongObjs)
                                    ->latest()->paginate(5);
        }
        else if($category && $class)
        {            
            $courseBelongCates = Course::select('id')->where('course_category_id', $sl_class)->get();
            
            $lessons = Lesson::where('course_id', $sl_category)                                    
                                    ->orWhereIn('course_id', $courseBelongCates)                                    
                                    ->latest()->paginate(5);
        }
        else if($category)
        {
            $lessons = Lesson::where('course_id', $sl_category)                                                                       
                                    ->latest()->paginate(5);
        }
        else if($object)
        {
            $cateBelongObjs = CourseCategory::select('id')->where('parent_id', $sl_object)->get();            
            $courseBelongObjs = Course::select('id')->whereIn('course_category_id', $cateBelongObjs)->get();

            $lessons = Lesson::whereIn('course_id', $courseBelongObjs)
                                    ->latest()->paginate(5); 
        }
        else
        {            
            $courseBelongCates = Course::select('id')->where('course_category_id', $sl_class)->get();

            $lessons = Lesson::whereIn('course_id', $courseBelongCates)                                    
                                    ->latest()->paginate(5);
        }

        for ($idxLesson = 0; $idxLesson  < count($lessons); $idxLesson ++) { 
            if ( $lessons[$idxLesson]->banner_media_id ) {
                $lessons[$idxLesson]->banner = Media::getBanner($lessons[$idxLesson]->banner_media_id);
            }
            if ($lessons[$idxLesson]->video_media_id) {
                $lessons[$idxLesson]->video = Media::getVideo($lessons[$idxLesson]->video_media_id);
            }
            if ( $lessons[$idxLesson]->user_id ) {
                $lessons[$idxLesson]->teacher_name = Member::find($lessons[$idxLesson]->user_id)->full_name;
            }
        }
        
        $index = $lessons->currentPage() * $lessons->perPage() - $lessons->perPage();    

        return view('admin.lessons.index', compact('lessons'
                                                , 'categories'
                                                , 'objects'
                                                , 'classes'
                                                , 'filter'
                                                , 'sl_category'
                                                , 'sl_object'
                                                , 'sl_class'
                                            ));
    }

    public function approveView()
    {
        $lessons = Lesson::where('status', \Config::get('constants.lesson_status.pending'))
                        ->latest()
                        ->paginate(5);

        for ($i=0; $i < sizeof( $lessons ); $i++) { 
            if ( $lessons[$i]->banner_media_id ) {
                $lessons[$i]->banner = Media::getBanner($lessons[$i]->banner_media_id);
            }          
            if ($lessons[$i]->video_media_id) {
                $lessons[$i]->video = Media::getVideo($lessons[$i]->video_media_id);
            }  
            if ( $lessons[$i]->user_id ) {
                $lessons[$i]->teacher_name = Member::find($lessons[$i]->user_id)->full_name;
            }
            $rating_lessons = RatingLesson::where('lesson_id', $lessons[$i]->id)
                                        ->get();

            for ($j=0; $j < sizeof( $rating_lessons ); $j++) { 
                if ( $rating_lessons[$j]->member_id ) {
                    $rating_lessons[$j]->teacher_name = Member::find($rating_lessons[$j]->member_id)->full_name;
                }
            }
            $lessons[$i]->rating_lessons = $rating_lessons;
            $lessons[$i]->rating = RatingLesson::calc_rating( $lessons[$i]->id );
        }
        
        return view('admin.lessons.approve', compact( 'lessons' ));
    }

    public function approvedLesson( Request $request )
    {
        $lesson = Lesson::find($request->id);
        $lesson->status = \Config::get('constants.lesson_status.approved');         
        $lesson->save();
        
        $notification = new Notification();        
        $notification->title = "Duyệt bài";        
        $notification->content = "Duyệt bài [" . $lesson->title . "]";
        $notification->save();

        $notificationDetail = new NotificationDetail();
        $notificationDetail->member_id = $lesson->user_id;
        $notificationDetail->notification_id = $notification->id;
        $notificationDetail->sender_id = $this->user->idate;
        $notificationDetail->save();

        return redirect('/admin/approve')->with("success", 'Phê duyệt bài giảng thành công');
    }

    public function noApprovedLesson( Request $request )
    {
        $lesson = Lesson::find($request->id);
        $lesson->status = \Config::get('constants.lesson_status.reject'); 
        $lesson->save();

        $notification = new Notification();
        $notification->title = "Không duyệt bài";
        $notification->content = "Không duyệt bài [" . $lesson->title . "]";
        $notification->save();

        $notificationDetail = new NotificationDetail();
        $notificationDetail->member_id = $lesson->user_id;
        $notificationDetail->notification_id = $notification->id;
        $notificationDetail->sender_id = $this->user->idate;
        $notificationDetail->save();

        return redirect('/admin/approve')->with("success", 'Không duyệt bài giảng thành công');
    }
}
