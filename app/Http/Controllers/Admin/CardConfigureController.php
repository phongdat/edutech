<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

use App\Models\CardConfigure;
use App\Helper\HpRandom;
use Validator;

class CardConfigureController extends AdminBaseController
{
    public function index()
    {
        $configs = CardConfigure::paginate(50);
        return view("admin.cardconfigure.index", compact('configs'));
    }

    public function update($configId)
    {
        $configs = CardConfigure::paginate(50);
        $config = CardConfigure::find($configId);
        return view("admin.cardconfigure.index", compact('configs','config'));
    }

    public function delete($configId)
    {
        $config = CardConfigure::find($configId)->delete();
        return redirect()->back()->with('success','Xóa cấu hình thành công');
    }

    public function save(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'value' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng điền đầy đủ thông tin');
        }

        if ($input['config_id'] != 0) {
            $courseCate = CardConfigure::find($input['config_id']);
            $courseCate->name = $input['name'];
            $courseCate->value = $input['value'];
            $courseCate->save();
            return redirect()->back()->with('success', 'Cập nhật đối tượng thành công');
        }
        $courseCate = new CardConfigure;
        $courseCate->name = $input['name'];
        $courseCate->value = $input['value'];
        $courseCate->save();

        return redirect()->back()->with('success', 'Tạo mới đối tượng thành công');
    }

}
