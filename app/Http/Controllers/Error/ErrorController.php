<?php

namespace App\Http\Controllers\Error;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ErrorController extends Controller
{
    public function clientPageNotFound()
    {       
        $title = 'EDUTECH - Không tìm thấy trang';
        return view('client.errors.404')->with(compact('title'));
    }
}