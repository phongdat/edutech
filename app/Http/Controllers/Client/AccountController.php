<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Client\BaseController;

use App\Models\GeneralPageConfig;
use App\Models\Member;
use App\Models\User;
use App\Models\Transaction;
use App\Models\Lesson;
use App\Models\Course;
use App\Models\RatingLesson;
use App\Models\Rating;
use Illuminate\Support\Facades\Auth;

use App\Http\Services\CourseCategoryServices;
use App\Http\Services\UserServices;
use App\Http\Services\LessonServices;

use App\Helper\EmailHelper;


class AccountController extends BaseController
{

    ////////////////////////////////////////////////////////////////////////////////
    // GET METHOD

    public function index( $id = null )
    {            
        $title = \Config::get('constants.title.account_page');  

        if( $id ){
            $user = User::find($id);
                  
            if( $user ){                               
                Auth::login($user);
                $this->user = Auth::user(); 
            }
        } else {
            $user = $this->user;
        }
        
        $this->checkLogin();        
        $member = $this->checkOrGetMember();        
        $data = UserServices::getDataMemberByMemberType( $member );
        $notifications = UserServices::get_notification_by_member( $member );

        return view('client.account.index')->with(compact('title'
                                                        , 'user'
                                                        , 'data'
                                                        , 'member' 
                                                        , 'notifications')); 
    }

    public function historyTransaction()
    {
        $this->checkLogin();
        $member = $this->checkOrGetMember();
        $title = \Config::get('constants.title.account_page');          
        $data = UserServices::getDataTransactionsByMemberType($member);        
        return view('client.history-transaction.index')->with(compact('title', 'data', 'member')); 
    }

    public function editProfile()
    {
        $this->checkLogin();
        $member = $this->checkOrGetMember();
        $title = \Config::get('constants.title.account_page');  
        
        return view('client.edit-profile.index')->with(compact('title', 'member')); 
    }

    public function editPassword()
    {
        $this->checkLogin();
        $member = $this->checkOrGetMember();

        $title = \Config::get('constants.title.account_page');  
        
        return view('client.change-password.index')->with(compact('title', 'member')); 
    }

    public function forgotPassword()
    {
        $title = 'Quên mật khẩu';
        return view('client.forgot-password.index', compact('title')); 
    }

    public function evaluate()
    {
        $title = 'Đánh giá';
        $this->checkLogin();
        $member = $this->checkOrGetMember();
        $lessons = LessonServices::get_rating_each_lesson_by_member( $member );
        $rates = LessonServices::get_rating_list_except_logger( $member );

        return view( 'client.evaluate.index', compact( 'lessons','rates','member', 'title' ));
    }

    public function lesson($lessonId)
    {
        $lesson = Lesson::getById($lessonId);
        $title = $lesson->title;
        return view('client.detail-lesson.index', compact('lesson', 'title'));
    }

    // END GET MOTHOD
    ////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////
    // POST METHOD

    public function saveChangePassword( Request $request )
    {
        $this->checkLogin();
        $member = $this->checkOrGetMember();

        $title = \Config::get('constants.title.account_page');  
        $input = $request->all();
        $messages = [
            'password.required' => 'Xin hãy nhập mật khẩu.',
            'password.string' => 'Mật khẩu không hợp lệ. Xin hãy nhập lại.',
            'password.min' => 'Mật khẩu không hợp lệ. Xin hãy nhập lại.',
            'password.confirmed' => 'Mật khẩu xác nhận không đúng.',
        ];
        $validator = Validator::make($input, [             
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ], $messages);
        
        if ($validator->fails()) {     
            return redirect()->back()->with('error', $validator->errors());            
        }

        $user = UserServices::changePassword($this->user, $input['password']);

        return redirect()->back()->with('success', 'Cập nhật mật khẩu thành công'); 
    }

    public function saveEditProfile( Request $request )
    {
        $this->checkLogin();
        $member = $this->checkOrGetMember();

        $title = \Config::get('constants.title.account_page');  
        $input = $request->all();
        $messages = [
            'name.required' => 'Xin hãy nhập họ và tên.',            
        ];
        $validator = Validator::make($input, [ 
            'name' => ['required', 'string', 'max:255']                
        ], $messages);
        
        if ($validator->fails()) {     
            return redirect()->back()->with('error', $validator->errors());            
        }
        $member = UserServices::editProfile($this->user, $input['name'], $input['address']);

        return redirect()->back()->with('success', 'Cập nhật hồ sơ thành công'); 
    }

    public function registerUser( Request $request )
    {
        $input = $request->all();
                
        $username = str_replace('+84', '0', $input['username']);
        $fullname = $input['fullname'];
        $address = $input['address'];
        $password = $input['password'];

        $checkUser = UserServices::getUserByUsername($username); 
        if(!isset($checkUser)){   
            $user = UserServices::registerUser($username, $password);
            $member = new Member;
            $member->user_id = $user->id;
            $member->full_name = $input['fullname'];
            $member->address = $input['address'];                      
            $member->member_type_id = Member::getStudentType();  
            $member->save();        
            
            $linkCallback = \Config::get('constants.data.link_call_back') . '/' . $user->id;
            return response()->json( [ 'msg' => 'success', 'linkCallback' => $linkCallback] );
        }
        return response()->json([ 'msg' => 'failed', 'username' => $username ]);
    }

    public function saveForgotPassword( Request $request )
    {
        $input = $request->all();        
        $input_value = $input['username'];
        $type = '';
        $value = '';

        $check_email = EmailHelper::validEmail( $input_value );
        if( $check_email )
        {
            $type = 'email';
        }
        else
        {
            $type = 'phone';
            $isDuplicatedTel = UserServices::isDuplicatedPhone( $input_value );
            if( $isDuplicatedTel )
                return redirect()->back()->with( 'error', 'Tài khoản này chỉ được hỗ trợ đổi mật khẩu bằng email.' );            
        }

        $checkUser = UserServices::getUserByEmailOrTel( $input_value, $type, $value );            
        
        if( isset ( $checkUser ) )
        {                                   
            $linkChangePassword = \Config::get('constants.data.link_change_password');
            return Redirect::to(\Config::get('constants.data.oherecore'). "/client/edutech/change-password?username=" . $checkUser->username . "&value=" . $value . "&type=" . $type . "&link-change-password=" . $linkChangePassword);
        }

        return redirect()->back()->with( 'error', 'Tài khoản không tồn tại' );            
    }

    public function changePasswordApi( Request $request )
    {
        $input = $request->all();
              
        $type = $input['type'];
        $username = $input['username'];
        $password = $input['password'];
        $value = $input['value'];

        // [Trường hợp số điện thoại bị trùng]
        if( $type == 'email' )
        {
            $isDuplicatedTel = UserServices::isDuplicatedPhone( $username );
            
            if( $isDuplicatedTel )
            {
                $user = User::where( 'email', $value )->first();                                
                if( isset( $user ) ){ 
                    $user->username = $value;                                     
                    $user->password = $password;
                    $user->save();
        
                    $linkCallback = \Config::get('constants.data.link_call_back') . '/' . $user->id;
                    return response()->json( [ 'msg' => 'success', 'linkCallback' => $linkCallback] );
                }
                return response()->json([ 'msg' => 'failed', 'username' => $username ]);                
            }                                    
        }
        // End [Trường hợp số điện thoại bị trùng]

        $checkUser = UserServices::getUserByUsername( $username ); 
        if( isset( $checkUser ) ){                 
            $user = User::find($checkUser->id);
            $user->password = $password;
            $user->save();

            $linkCallback = \Config::get('constants.data.link_call_back') . '/' . $user->id;
            return response()->json( [ 'msg' => 'success', 'linkCallback' => $linkCallback] );
        }
        return response()->json([ 'msg' => 'failed', 'username' => $username ]);
    }

    public function rating(Request $req)
    {   
        $rating_lesson = RatingLesson::find($req->rateId);
        $rating_lesson->rating = $req->rating;
        $rating_lesson->content = $req->feedback_content;
        $rating_lesson->save();
        return redirect()->back()->with('success', 'Đánh giá thành công');
    }

    public function likeCourse( Request $request )
    {
        $input = $request->all();        

        if( !$this->user )
            return redirect()->back()->with('error', 'Xin hãy đăng nhập để sử dụng chức năng này');

        $member = Member::where('user_id', $this->user->id)->first();
        if( !$member )
            return redirect()->back()->with('error', 'Xin hãy đăng nhập để tài khoản thành viên để sử dụng chức năng này');

        $courseCategoryId =  $input['courseId'];
        $ratingValue = $input['rating'];
        
        $rating = Rating::where([
                                    [ 'member_id', '=', $member->id ],
                                    [ 'course_category_id', '=', $courseCategoryId ],
                                ])
                        ->first();                

        if( isset( $rating ) )
        {
            $rating->rating = $ratingValue;
            $rating->save();
        }
        else
        {
            $rating = new Rating();
            $rating->member_id = $member->id;
            $rating->course_category_id = $courseCategoryId;
            $rating->rating = $ratingValue;
            $rating->save();
        }        
        return redirect()->back()->with('success', 'Cảm ơn bạn đã đánh giá.');

    }
    // END POST METHOD
    ////////////////////////////////////////////////////////////////////////////////  
}
