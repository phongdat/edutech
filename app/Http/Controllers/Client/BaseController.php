<?php

namespace App\Http\Controllers\Client;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

use App\Http\Services\CourseCategoryServices;
use App\Http\Services\GeneralConfigServices;
use App\Models\Member;
use App\Models\News;
use View;
use Redirect;

class BaseController extends Controller
{
    public $user;

    public $member;

    protected $request;    

    public function __construct(Request $request, Redirector $redirect)
    {
        $this->middleware(function ($request, $next) {
            $this->init();
            return $next($request);
        });
    }

    public function init()
    {
        $this->user = Auth::user();  
        $this->member = null;
        if( $this->user )
        {
            $this->member = Member::where('user_id', $this->user->id)->first();
        }
        // $member_link = '';
        // if( $this->user )
        // {
        //     $member = Member::where('user_id', $this->user->id)->first();
        
        //     if( $member )
        //     {
        //         if( $member->full_name )
        //         {
        //             $member_link = str_slug($member->full_name .' '. $member->id);
        //         }
        //         else
        //         {
        //             $member_link = str_slug($this->user->username .' '. $member->id); 
        //         }                
        //     }            
        // }
        
        $categories = CourseCategoryServices::getCategoryParents();
        $courses = CourseCategoryServices::getCourseData();  
        
        $courseShow = CourseCategoryServices::getCourseShowData( $this->member );

        $newestCourses = CourseCategoryServices::getRandomNewestData(\Config::get('constants.data.default_number_of_courses'), $this->member);  
        $configs = GeneralConfigServices::loadConfigs();  
          
        View::share('categories', $categories);
        View::share('courses', $courses);
        View::share('courseShow', $courseShow);
        View::share('user', $this->user);
        View::share('newest_data', $newestCourses);
        View::share('configs', $configs);
        // View::share('member_link', $member_link);
    }

    public function checkLogin ()
    {
        if ( !$this->user ) {
            return Redirect::to("/login")->send();
        }
    }

    public function checkOrGetMember()
    {          
        $member = Member::where('user_id', $this->user->id)->first();        
        if( !$member )
        {
            $this->member = $member;
            return Redirect::to("/")->send();     
        }      
            
        return $member;                       
    }
}