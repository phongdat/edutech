<?php

namespace App\Http\Controllers\Client;

use App;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Client\BaseController;
use App\Helper\UrlHelper;
use App\Http\Services\CourseCategoryServices;
use App\Http\Services\CourseServices;
use App\Http\Services\UserServices;
use App\Models\Lesson;
use App\Models\Member;
use App\Models\MemberCoursesWatched;
use App\Models\Course;
use Illuminate\Support\Facades\Auth;

class LevelController extends BaseController
{

    public function index($parent) 
    {              
        $id = UrlHelper::getIdFromUrl($parent);  

        $curCategory = CourseCategoryServices::findById($id);
        $title = \Config::get('constants.title.list_level_page').$curCategory->name;
        $allCourses = CourseCategoryServices::getAllCourseBelongToCategory($curCategory->id
                                                                        , $curCategory
                                                                        , $this->member);         
        return view('client.level.list')->with(compact('title', 'curCategory', 'allCourses')); 
    }

    public function detail($parent, $courseCategory, $lesson = null)
    {
        $parentId = UrlHelper::getIdFromUrl($parent);        
        $parentReqCourseCategory = CourseCategoryServices::findById($parentId);
        $courseCategoryId = UrlHelper::getIdFromUrl($courseCategory);    
        
        $reqCourseCategory = CourseCategoryServices::findById($courseCategoryId, $this->member);        
        $title = $reqCourseCategory->name; 
        $reqCourses = CourseServices::getAscCoursesBelongToCategoryId($courseCategoryId);                                
        $pieces = array_chunk($reqCourses->toArray(), ceil(count($reqCourses) / 2));
        $memCWatcheds = null;
        
        if(is_null($lesson)){
            $data = $reqCourses[0];
            if ( Auth::check() ) {
                $member = $this->checkOrGetMember();
                $memCWatched = MemberCoursesWatched::where([
                    ['member_id', $member->id],
                    ['courses_id', $courseCategoryId]
                ])->first();

                if ( !$memCWatched ) {
                    $memCWatched = new MemberCoursesWatched;
                    $memCWatched->member_id = $member->id;
                    $memCWatched->courses_id = $courseCategoryId;
                    $memCWatched->save();
                }

                $memCWatcheds = MemberCoursesWatched::where([
                    ['member_id', $member->id],
                    ['courses_id' , '!=', $courseCategoryId]
                ])->orderBy('created_at', 'desc')->take(3)->get();

                for ($idxC=0; $idxC < count($memCWatcheds); $idxC++) {
                    $course = CourseServices::getAscCoursesBelongToCategoryId($memCWatcheds[$idxC]->courses_id)[0];
                    $memCWatcheds[$idxC]->course = $course;     
                    $memCWatcheds[$idxC]->course_category = CourseCategoryServices::findById($course->course_category_id, $this->member);                    
                }                
            }
        }
        else
        {                        
            $lessonId = UrlHelper::getIdFromUrl($lesson);        
            $introCourse = $reqCourses[0]->lessons[0];
            if (!Auth::check() && $lessonId != $introCourse->id) {
                return redirect('/login');
            }       
            if($lessonId != $introCourse->id){
                $member = $this->checkOrGetMember();
                $checkAvail = UserServices::checkCourseIsAvailable($courseCategoryId, $member);
                if(!$checkAvail){
                    return redirect('/account')->with('error', "Bạn chưa kích hoạt khoá học");
                }
            }
            $data = Lesson::getById($lessonId);            
        }

        $breadcrumbs = array($parentReqCourseCategory->name, $title);
        return view('client.level.detail')->with(compact('title'
                                                        , 'reqCourseCategory'
                                                        , 'pieces'
                                                        , 'data'
                                                        , 'parent'
                                                        , 'courseCategory'
                                                        , 'breadcrumbs'
                                                        , 'memCWatcheds'
                                                    )); 
    }
}