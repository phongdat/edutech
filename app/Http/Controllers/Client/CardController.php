<?php
namespace App\Http\Controllers\Client;

use App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Client\BaseController;
use Illuminate\Support\Facades\Auth;

use App\Models\Card;
use App\Models\Member;
use App\Models\MemberType;
use App\Models\Stock;
use App\Models\Order;
use Validator;

class CardController extends BaseController
{
    public function rechargeCard( Request $request )
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'serial_number' => 'required',
            'code' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Vui lòng nhập đầy đủ thông tin.');
        }
        $member = Member::getMemberByUserId($input['user_id']);
        if($member){
            $result = Card::rechargeCard($input['serial_number'], $input['code'], $member->id);

            if($result){
                return redirect()->back()->with('success', 'Kích hoạt thẻ thành công.');
            }
            return redirect()->back()->with('error', 'Kích hoạt thẻ không thành công, vui lòng kiểm tra lại thông tin.');
        }
        return redirect()->back()->with('error', 'Thông tin tài khoản thành viên của bạn không tìm thấy.');
    }

    public function transfer()
    {
        $title = \Config::get('constants.title.transfer_page');           
        $member = $this->checkOrGetMember();
        $member_types = MemberType::get();
        $member_id = $member->id;
        $groupMembers = Member::groupByMembers();
        $cards = Stock::getCardsOnMember($member_id);
        $max = sizeof($cards);
        return view('client.account.transfer')->with(compact('title'
                                                            , 'member_id'
                                                            , 'member_types'
                                                            , 'groupMembers'
                                                            , 'max')); 
    }

    public function paid( Request $request )
    {
        $input = $request->all();

        $orders = Order::where('member_id', $input['member_id'])->get();
        for ($idxOrder =0; $idxOrder  < count($orders); $idxOrder ++) {
            $orders[$idxOrder]->status = \Config::get('constants.order_status.transfer_paid');
            $orders[$idxOrder]->save();
        }

        //create stock for this tranferred member
        $stocks = Stock::where('member_id', $input['member_id'])->get();
        for ($idxStock = 0; $idxStock  < count($stocks); $idxStock ++) {
            $stocks[$idxStock]->status = \Config::get('constants.order_status.transfer_paid');
            $stocks[$idxStock]->save();
        }        

        return redirect()->back()->with('success', 'Thanh toán thành công');
    }

    public function saveTransfer( Request $request )
    {
        try {

            $input = $request->all();        
            $availableStocks = Stock::getNumberOfAvailableStock($input['member_id'], $input['quantity']);   

            for ( $idxQuantity = 0; $idxQuantity < count($availableStocks); $idxQuantity ++ ) 
            {
                $cur = $availableStocks[$idxQuantity];
    
                $curStock = Stock::find($cur->id);
                $curStock->member_id = $input['sl_member']; 
                $curStock->save();                
                    
                $order = new Order();
                $order->product_id = $cur->product_id;
                $order->member_id = $input['sl_member'];
                $order->card_id = $cur->card_id;
                $order->sender_id = $input['member_id'];
                $order->status = isset($input['chk-paid']) == true ? \Config::get('constants.order_status.transfer_paid')  : \Config::get('constants.order_status.transfer_unpaid');
                $order->save();                                            
            }
            return redirect('/account')->with('success', 'Chuyển thẻ thành công');
        } catch (\Exception $ex) {
            dd($ex); exit();
            return redirect()->back()->with('error', 'Có lỗi xảy ra.');       
        }       
    }
}