<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Helper\UrlHelper;
use App\Models\News;

class NewsController extends BaseController
{
    public function index()
    {
        $title = \Config::get('constants.title.news_page');
        $newses = News::getDescNewsByDate();        
        return view('client.tin-tuc.index', compact('title', 'newses'));
    }

    public function detail( $news )
    {
        $news_id = UrlHelper::getIdFromUrl($news);        
        $cur_news = News::getNewsById($news_id);
        $title = $cur_news->title; 
        $newest_news = News::getNumbOfDescNewsByDate(1);
        $breadcrumbs = array('Tin tức', $title);
        if(!$cur_news)
            return redirect('/tin-tuc')->with('error', "Không tìm thấy bài viết");

        return view('client.tin-tuc.detail', compact('title', 'cur_news', 'newest_news', 'breadcrumbs'));
    }
}
