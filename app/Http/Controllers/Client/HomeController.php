<?php

namespace App\Http\Controllers\Client;

use App;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Client\BaseController;

use App\Models\GeneralPageConfig;
use App\Models\News;
use App\Helper\EmailHelper;
use App\Http\Services\CourseCategoryServices;

class HomeController extends BaseController
{

    public function index() 
    {        
 
        $title = \Config::get('constants.title.home_page');
        $num = \Config::get('constants.data.default_number_of_courses');
        $newest_news = News::getNumbOfDescNewsByDate(3);
        $newestCourses = CourseCategoryServices::getNumOfNewestData($num);       
        return view('client.trang-chu.index')->with(compact('title', 'newestCourses', 'newest_news')); 
    }

    public function introduce()
    {        
        $page = GeneralPageConfig::getIntroductionPage();
        return view('client.gioi-thieu.index')->with(compact('page')); 
    }

    public function policy()
    {
        $page = GeneralPageConfig::getPolicyPage();
        return view('client.chinh-sach-bao-mat.index')->with(compact('page')); 
    }

    public function agreement()
    {
        $page = GeneralPageConfig::getAgreementPage();
        return view('client.thoa-thuan-su-dung.index')->with(compact('page')); 
    }

    public function contact()
    {
        $title = \Config::get('constants.title.contact_page');
        return view('client.lien-he.index')->with(compact('title')); 
    }

    public function getLibrary()
    {
        return view('client.trang-chu.index'); 
    }

    public function examPractice()
    {
        return view('client.trang-chu.index'); 
    }

    public function register()
    {
        $title = \Config::get('constants.title.register_page');
        return view('client.dang-ky.index')->with(compact('title')); 
    }    

    public function personnel()
    {
        $page = GeneralPageConfig::getPersonnelPage();
        return view('client.nhan-su-va-to-chuc.index')->with(compact('page'));         
    }

    public function instruction()
    {
        $page = GeneralPageConfig::getInstructionPage();
        return view('client.huong-dan.index')->with(compact('page'));         
    }

    public function ministry()
    {
        $page = GeneralPageConfig::getMinistryPage();
        return view('client.bo-cong-thuong.index')->with(compact('page'));   
    }

    public function submitContactForm( Request $request )
    {
        $input = $request->all();
        $data = array(
            'name' => $input["name"],
            'email' => $input["email"], 
            'msg' => $input['messages'],
            'phone' => $input['phone']
        );

        $subject = 'EduTech Web Admin Confirmation';            
        EmailHelper::sendEmail($data, $subject);

        return back()->with('success','Gửi mail thành công.');
    }
}