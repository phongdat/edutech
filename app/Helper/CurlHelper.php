<?php

namespace App\Http\Helper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CurlHttp extends Controller
{

    public static function getFBAccessToken()
    {
        $curl = curl_init();

        $facebookAccessTokenUrl = "https://graph.facebook.com/oauth/access_token?client_id=2416749948546307&client_secret=94eeceee7a9ab6de45efba3ad15d3796&grant_type=client_credentials";

        curl_setopt_array($curl, array(
            CURLOPT_URL => $facebookAccessTokenUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        return $response;
    }

    public static function getFromLink($addr)
    {
        $fbToken = json_decode(CurlHttp::getFBAccessToken(), true);
        $facebookApi = "https://graph.facebook.com/v3.3/?access_token=" . $fbToken['access_token'];
        $data = array(
            'id' => $addr,
            'scrape' => 'true',
            'suppress_http_code' => '1',
            'format' => 'json'
        );
        $message = http_build_query($data);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $facebookApi,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $message,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        return $response;
    }
}
