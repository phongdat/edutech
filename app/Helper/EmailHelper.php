<?php

namespace App\Helper;
use Mail;
use \Swift_Mailer;
use \Swift_SmtpTransport;

class EmailHelper
{
    public static function validEmail($value)
    {      
        $email_regex = "/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/";
        if (!is_string($value) && !is_numeric($value)) {
            return false;
        }
    
        return preg_match($email_regex, $value);
    }

    public static function sendEmail($data, $subj)
    {
        // Backup your default mailer
        $backup = Mail::getSwiftMailer();

        // Setup your gmail mailer
        $transport = new Swift_SmtpTransport('smtp.sendgrid.net', 587, 'tls');
        $transport->setUsername('huynhhq96');
        $transport->setPassword('uS!3D_pAwR$K9QK');
        // Any other mailer configuration stuff needed...

        $gmail = new Swift_Mailer($transport);

        // Set the mailer as gmail
        Mail::setSwiftMailer($gmail);

        // $emailTo = $email;

        Mail::send(['html' => 'client.mail.admin'], $data, function ($message) use ($subj) {
            $message->to('info@edutech.org.vn');            
            $message->bcc('tainc@ftcjsc.com', 'EduTech Web Admin');      
            $message->bcc('yncdbnap@gmail.com', 'EduTech Web Admin');            
            $message->subject($subj);
            $message->from('huynhhq96@gmail.com', 'EduTech Web Admin');            
        });

        // Restore your original mailer
        Mail::setSwiftMailer($backup);
    }
}