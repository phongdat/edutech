<?php

namespace App\Helper;

class UrlHelper
{
    public static function getIdFromUrl($url)
    {      
        $parts = explode('-', $url);        
        return end($parts);
    }
}