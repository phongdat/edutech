<?php

namespace App\Helper;

class CookieHelper
{
    public static function pushCurrentCourseToRecentlyViewed($viewedCourse)
    {
        try {
            $num_to_store = 2;
            $minutes_to_store = 1440;
            $recent = \Cookie::get('recently_viewed_content');
            $recent = json_decode($recent, TRUE);
            
            if ($recent) {
                foreach ($recent as $key=>$val) {
                    $cur = (array)json_decode($val);                    
                        if ($cur["id"] == $viewedCourse->id)
                            unset($recent[$key]);
                }
            }
            $recent[time()] = $viewedCourse->toJson();
            
            if (sizeof($recent) > $num_to_store) {            
                $recent = array_slice($recent, sizeof($recent) - 2, sizeof($recent), true);
            }
            \Cookie::queue('recently_viewed_content', json_encode($recent), $minutes_to_store);
        } catch (\Exception $ex) {
            dd($ex);exit();
        }
    }
}