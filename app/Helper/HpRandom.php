<?php

namespace App\Helper;

class HpRandom
{
    public static function randCardSerial($fstSerial ,$lstCrdId, $length = 11) {
        $str = "";
        $characters = array_merge(range('0','9'), range('9','0'));
        $max = count($characters) - 1;
        $length = $length - (strlen((string)$fstSerial) + strlen((string)$lstCrdId));
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $fstSerial . $str . $lstCrdId;
    }

    public static function randCardNum($length = 13) {
        $str = "";
        $characters = array_merge(range('0','9'), range('9','0'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }
}
