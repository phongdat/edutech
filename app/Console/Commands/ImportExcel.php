<?php

namespace App\Console\Commands;

use App\Excels\ExcelImport;
use Illuminate\Console\Command;
use App\Models\CardImport;

class ImportExcel extends Command
{
    protected $signature = 'import:excel';

    protected $description = 'Laravel Excel importer';

    public function handle()
    {
        $this->output->title('Starting import');
        (new ExcelImport)->withOutput($this->output)->import(
            public_path(CardImport::orderBy('created_at', 'desc')->first()->source),
            null,
            \Maatwebsite\Excel\Excel::XLSX
        );
        $this->output->success('Import successful');
    }
}
