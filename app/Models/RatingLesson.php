<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class RatingLesson extends Model
{
    public static function calc_rating( $lesson_id )
    {
        $result = RatingLesson::select( 'lesson_id', 'member_id'
                                        , DB::raw('sum(rating) as number_of_rating')
                                        , DB::raw('count(*) as number_of_rater'))
                                ->where( 'lesson_id', $lesson_id )->first();
        if( !isset( $result->lesson_id ) )
            return 0;

        return $result->number_of_rating / $result->number_of_rater;
    }
}
