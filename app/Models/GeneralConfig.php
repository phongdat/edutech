<?php

namespace App\Models;

use App\Models\BaseModel;

class GeneralConfig extends BaseModel
{    
    public static function getConfigs()
    {
        return GeneralConfig::get();
    }

    public static function getOrCreateActivePoint()
    {
        $activePoint = GeneralConfig::where('name', BaseModel::$active_point_name)->first();
        if(!$activePoint){
            $activePoint = new GeneralConfig();
            $activePoint->name = BaseModel::$active_point_name;
            $activePoint->value = 1;
            $activePoint->type = 'textbox';
            $activePoint->save();
        }
        return (int)$activePoint->value;
    }
}