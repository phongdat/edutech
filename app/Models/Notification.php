<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\NotificationDetail;

class Notification extends BaseModel
{
    public static function get_notification_by_member_id( $member_id )
    {
        $notifications = NotificationDetail::where([
                                                        [ 'member_id', '=', $member_id ],
                                                        [ 'is_read', '=', false ]
                                                    ])->get();        
        for ($i=0; $i < sizeof( $notifications ); $i++) { 
            $current_notify_detail = NotificationDetail::find($notifications[$i]->id);
            $current_notify_detail->is_read = true;
            $current_notify_detail->save();

            $notify_data = Notification::find( $notifications[$i]->notification_id );
            $notifications[$i]->title = isset( $notify_data ) == true ? $notify_data->title : '';
            $notifications[$i]->content = isset( $notify_data ) == true ? $notify_data->content : '';
        }    
        return $notifications;
    }

    public static function get_notification_by_user_id( $user_id )
    {        
        $notifications = NotificationDetail::where([
                                                    [ 'user_id', '=', $user_id ],
                                                    [ 'is_read', '=', false ]
                                                ])->get();   
        for ($i=0; $i < sizeof( $notifications ); $i++) { 
            $current_notify_detail = NotificationDetail::find($notifications[$i]->id);
            $current_notify_detail->is_read = true;
            $current_notify_detail->save();

            $notify_data = Notification::find( $notifications[$i]->notification_id );
            $notifications[$i]->title = isset( $notify_data ) == true ? $notify_data->title : '';
            $notifications[$i]->content = isset( $notify_data ) == true ? $notify_data->content : '';
        } 
        return $notifications;
    }
}
