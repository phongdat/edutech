<?php

namespace App\Models;

use App\Models\BaseModel;

class CardConfigure extends BaseModel
{
    public static function getLastConfig ()
    {
        return CardConfigure::latest("created_at")->first();
    }
}
