<?php

namespace App\Models;

use App\Models\BaseModel;

class ProductCourseAssignment extends BaseModel
{
    protected $table = 'product_course_assignments';

    public static function updateCateCourse( $courseCategoryIds, $prodId )
    {
        $notExists = ProductCourseAssignment::select('course_category_id')->whereNotIn('course_category_id', $courseCategoryIds)->get();
        for ($idxCourse=0; $idxCourse < count($courseCategoryIds); $idxCourse++) { 
            for ($idxInsert=0; $idxInsert < count($notExists); $idxInsert++) { 
                if ($courseCategoryIds[$idxCourse] == $notExists[$idxInsert]->course_category_id) {
                    $prodCourseAssgn = new ProductCourseAssignment();
                    $prodCourseAssgn->course_category_id = $courseCategoryIds[$idxCourse];
                    $prodCourseAssgn->product_id = $prodId;
                    $prodCourseAssgn->save();
                }
            }
        }

        $exist = ProductCourseAssignment::select("course_category_id")->where('product_id', $prodId)->get();
        for ($idxDelete = 0; $idxDelete < count($exist); $idxDelete++) {
            if( !in_array($exist[$idxDelete]->course_category_id, $courseCategoryIds)) {
                $exist[$idxDelete]->delete();
            }
        }

    }
}
