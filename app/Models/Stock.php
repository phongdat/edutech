<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\Card;

class Stock extends BaseModel
{
    public static function getCardsOnMember($member_id)
    {
        $cards = Stock::where('member_id', $member_id)->get();
        for ($i=0; $i < sizeof($cards); $i++) { 
            $cards[$i]->card_data = Card::find($cards[$i]->card_id);
            $cards[$i]->idPaid = false;
            if($cards[$i]->status == \Config::get('constants.order_status.transfer_paid'))
                $cards[$i]->idPaid = true;
        }
        return $cards;
    }

    public static function getNumberOfAvailableStock($member_id, $num)
    {
        $cards = Stock::where('member_id', $member_id)->take($num)->get();
        for ($i=0; $i < sizeof($cards); $i++) { 
            $cards[$i]->card_data = Card::find($cards[$i]->card_id);
        }
        return $cards;
    }
}
