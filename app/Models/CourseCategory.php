<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\Course;
use App\Models\ProductCourseAssignment;

class CourseCategory extends BaseModel
{ 
	public static function getParents()
    {
        return CourseCategory::where('parent_id', null)->get();
    }

    public static function getNumberOfNewestCoursesByCateId($id, $num)
    {
        return CourseCategory::where('parent_id', $id)
                            ->orderBy('created_at', 'desc')
                            ->take($num)
                            ->get();
    }

    public static function getNumberOfNewestCourses($num)
    {
        return CourseCategory::where('parent_id', '!=', null)
                            ->orderBy('created_at', 'desc')
                            ->take($num)
                            ->get();
    }

    public static function getCategoriesById($id)
    {
        return CourseCategory::where('parent_id', $id)->orderBy('created_at', 'desc')->get();
    }

    public static function getCourseCategory()
    {
        return CourseCategory::where('parent_id', '!=', null)->get();
    }

    public static function getCourseSelectByProdId ( $prodId )
    {
        $courses = CourseCategory::where('parent_id', '!=', null)->get();
        $prodCourseAssg = ProductCourseAssignment::where("product_id", $prodId)->get();
        for ($idxCos =0; $idxCos  < count($courses); $idxCos ++) { 
            $courses[$idxCos]->selected = false;
            for ($idxProAssg =0; $idxProAssg  < count($prodCourseAssg); $idxProAssg ++) { 
                if ($courses[$idxCos]->id == $prodCourseAssg[$idxProAssg]->course_category_id) {
                    $courses[$idxCos]->selected = true;
                }
            }
        }
        return $courses;
    }
}
