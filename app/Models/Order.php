<?php

namespace App\Models;

use App\Models\BaseModel;

class Order extends BaseModel
{
    public function card()
    {
        return $this->belongsTo(Card::class);
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }
    
    public static function getTotalCardOnSaleAreas($members)
    {
        for ($idxMem=0; $idxMem < count($members); $idxMem++) {
            $members[$idxMem]->total_sending_order = Order::where([
                ['member_id', $members[$idxMem]->id],
                ['status', \Config::get('constants.order_status.transfer_unpaid')],
            ])->count();
            
            $members[$idxMem]->total_sended_order = Order::where([
                ['member_id', $members[$idxMem]->id],
                ['status', \Config::get('constants.order_status.transfer_paid')],
            ])->count();
            
        }
    }
}
