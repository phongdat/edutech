<?php

namespace App\Models;

use App\Models\BaseModel;

class Role extends BaseModel
{
    public static function getWhereNotRoot ()
    {
        return Role::where('id', '!=', 1)->get();
    }

    public static function addRoleNameToUsers ( $users )
    {
        for ($idxUsr = 0; $idxUsr  < count($users); $idxUsr ++) { 
            $users[$idxUsr]->role_name = $users[$idxUsr]->role_id ? Role::find($users[$idxUsr]->role_id)->name : "";
        }
    }
}
