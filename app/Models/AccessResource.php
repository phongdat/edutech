<?php

namespace App\Models;

use App\Models\BaseModel;

class AccessResource extends BaseModel
{
    public static function getByPageId($pageId)
    {
        return AccessResource::where('page_id', $pageId)->get();
    }
}
