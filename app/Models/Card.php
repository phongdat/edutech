<?php

namespace App\Models;

use App\Models\BaseModelWithoutCache;
use DB;
use App\Helper\HpRandom;
use App\Models\CardConfigure;
use App\Models\Order;
use App\Models\MemberCurrency;

class Card extends BaseModelWithoutCache
{
    /**
     * @var array
     */
    protected $table = 'cards';
    protected $fillable = ['id','number','series', 'group_series', 'type'];

    public static function getBySerial ( $serial )
    {
        return Card::where('series', 'like', '%' . $serial . '%')->paginate(50);
    }

    public static function getCardCanChoose ()
    {
        return Card::whereNotExists(function($query)
                {
                    $query->select(DB::raw(1))
                          ->from('orders')
                          ->whereRaw('orders.card_id = cards.id');
                })->get();
    }

    public static function createNewCards ($quantity, $prodId)
    {
        $cardIds = [];
        $card = Card::latest("created_at")->first();
        $lastId = 1;
        $grp_series = 1;
        $config_value = CardConfigure::getLastConfig()->value;
        if ($card)
            $lastId = $card->id + 1;
        if ($card)
            $grp_series = $card->group_series + 1;

        $idxGroup = 1;
        for ($idxCard = 0; $idxCard < $quantity; $idxCard++) {

            Card::create([
                'number' => HpRandom::randCardNum(BaseModel::$cardNumber),
                'series' => HpRandom::randCardSerial($prodId, $lastId, BaseModel::$serialNumber),
                'group_series' => $grp_series,
                'type' => 'auto',
            ]);
            array_push($cardIds, $lastId);

            if ($idxGroup < $config_value) {
                $idxGroup++;
            } else {
                $idxGroup = 1;
                $grp_series++;
            }

            $lastId++;
        }

        return $cardIds;
    }

    public static function getLastGroupSiries()
    {
        $card = Card::select('group_series')->latest("created_at")->first();
        if ( !$card ) {
            return 1;
        }
        $groupSeriesCount = Card::where('group_series', $card->group_series)->count();
        $config_value = CardConfigure::getLastConfig()->value;
        if ( $groupSeriesCount < (int)$config_value ) {
            return $card->group_series;
        } else {
            return $card->group_series + 1;
        }
        
    }

    public static function rechargeCard($serialNumber, $code, $memberId)
    {
        $card = Card::where([
                                ['series', '=', $serialNumber],
                                ['number', '=', $code]
                    ])->first();
        
        if($card){
            $order = new Order();
            $order->member_id = $memberId;
            $order->card_id = $card->id;
            $order->product_id = 1;
            $order->status = \Config::get('constants.order_status.active_status');
            $order->save();
            MemberCurrency::createOrUpdatePoints($memberId);
            return true;
        }
        return false;
    }
}
