<?php

namespace App\Models;

use Illuminate\Support\Facades\Hash;

use App\Models\BaseModel;
use App\Models\User;
use App\Models\Currency;
use App\Models\GeneralConfig;
use App\Models\Transaction;

class MemberCurrency extends BaseModel
{    
    public static function getPointData($member)
    {
        $data = MemberCurrency::where('member_id', $member->id)->first();
        if($data){
            $usedPoint = Transaction::getUsedPoint($member->id);
            return $data->remaining_point - $usedPoint;
        }         
        return 0;
    }

    public static function createOrUpdatePoints($memberId)
    {
        $activePoint = GeneralConfig::getOrCreateActivePoint();
        $memberCurrency = MemberCurrency::where('member_id', $memberId)->first();
        if($memberCurrency){
            $memberCurrency->total_point += $activePoint;
            $memberCurrency->remaining_point += $activePoint;
            $memberCurrency->save();
        }else{
            $currency = Currency::createCurrency();
            $memberCurrency = new MemberCurrency();
            $memberCurrency->member_id = $memberId;    
            $memberCurrency->currency_id = $currency->id;    
            $memberCurrency->total_point = $activePoint;
            $memberCurrency->remaining_point = $activePoint;        
            $memberCurrency->save();
        }        
    }
}