<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\Cacheable;
use App\Models\BaseModelWithoutCache;
use Spatie\ResponseCache\Facades\ResponseCache;

class BaseModel extends BaseModelWithoutCache
{
    use Cacheable;
    protected $cacheTime = 120;

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            // ... code here
        });

        self::created(function ($model) {
            exec("redis-cli FLUSHALL");
            ResponseCache::clear();
        });

        self::updating(function ($model) {
            // ... code here
        });

        self::updated(function ($model) {
            exec("redis-cli FLUSHALL");
            ResponseCache::clear();
        });

        self::deleting(function ($model) {
            // ... code here
        });

        self::deleted(function ($model) {
            exec("redis-cli FLUSHALL");
            ResponseCache::clear();
        });
    }
    
    // Type
    public static $types = [
        'textbox'  =>  'Textbox',
        'textarea'    => 'Textarea',
        'image'    => 'Image',
        'icon'  =>  'icon'
    ];

    // Active point
    protected static $active_point_name = "active-point";

    /**
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];
}
