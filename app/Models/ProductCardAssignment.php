<?php

namespace App\Models;

use App\Models\BaseModel;

class ProductCardAssignment extends BaseModel
{
    protected $table = 'product_card_assignments';

    public static function getCardIdByProdId ( $prodId )
    {
        return ProductCardAssignment::select('card_id')->where("product_id", $prodId)->get();
    }
}
