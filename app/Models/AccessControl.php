<?php

namespace App\Models;

use App\Models\BaseModel;

class AccessControl extends BaseModel
{
    public static function getACByARList ( $arList )
    {
        $arIds = [];
        for ($idxAR =0; $idxAR  < count($arList); $idxAR ++) { 
            array_push($arIds, $arList[$idxAR]->id);
        }
        $acs = AccessControl::whereIn('access_resource_id', $arIds)
        ->where('role_id', '!=', 1)
        ->get();
        
        for ($idxAR =0; $idxAR  < count($arList); $idxAR++) {
            for ($idxAcs = 0; $idxAcs  < count($acs); $idxAcs++) {
                if ( $acs[$idxAcs]->access_resource_id == $arList[$idxAR]->id )
                    $acs[$idxAcs]->control_id = $arList[$idxAR]->control_id;
            }
        }
        return $acs;
    }
}
