<?php

namespace App\Models;

use Illuminate\Support\Facades\Hash;
use DB;
use App\Models\BaseModel;
use App\Models\User;
use App\Models\MemberType;

class Member extends BaseModel
{
    public static function getTeacherType()
    {
        return BaseModel::$teacher;
    }

    public static function getSaleareaType()
    {
        return BaseModel::$salearea;
    }

    public static function getDistributorType()
    {
        return BaseModel::$distributor;
    }

    public static function getAgencyType()
    {
        return BaseModel::$agency;
    }

    public static function getStudentType()
    {
        return BaseModel::$student;
    }
    
    public static function getTeachers()
    {
        $members = Member::where('member_type_id', BaseModel::$teacher)->get();
        for ($idxMember = 0; $idxMember < count($members); $idxMember++) { 
            if ( $members[$idxMember]->user_id ) {
                $members[$idxMember]->username = User::find($members[$idxMember]->user_id)->username;
            }
        }
        return $members;
    }

    public static function getLoadMoreTeacher()
    {
        $members = Member::where('member_type_id', BaseModel::$teacher)
                        ->latest()
                        ->paginate(5);
        for ($idxMember = 0; $idxMember < count($members); $idxMember++) { 
            if ( $members[$idxMember]->user_id ) {
                $members[$idxMember]->username = User::find($members[$idxMember]->user_id)->username;
            }
        }
        return $members;
    }

    public static function getMember( $id )
    {
        $member = Member::find($id);
        if ($member->user_id) {
            $member->username = User::find($member->user_id)->username;
        }
        return $member;
    }

    public static function getSaleAreas ()
    {
        $members = Member::where('member_type_id', BaseModel::$salearea)->get();
        for ($idxMember = 0; $idxMember < count($members); $idxMember++) {
            if ($members[$idxMember]->user_id) {
                $members[$idxMember]->username = User::find($members[$idxMember]->user_id)->username;
            }
        }
        return $members;
    }

    public static function getDistributor()
    {
        $members = Member::where('member_type_id', BaseModel::$distributor)->get();
        for ($idxMember = 0; $idxMember < count($members); $idxMember++) {
            if ($members[$idxMember]->user_id) {
                $members[$idxMember]->username = User::find($members[$idxMember]->user_id)->username;
            }
        }
        return $members;
    }

    public static function getAgency()
    {
        $members = Member::where('member_type_id', BaseModel::$agency)->get();
        for ($idxMember = 0; $idxMember < count($members); $idxMember++) {
            if ($members[$idxMember]->user_id) {
                $members[$idxMember]->username = User::find($members[$idxMember]->user_id)->username;
            }
        }
        return $members;
    }

    public static function saveUser( $member, $username, $password )
    {
        $user = new User;
        if ( $member->user_id ) {
            $user = User::find($member->user_id);
        }
        $user->username = $username;
        $user->password = Hash::make($password);
        $user->save();
        $member->user_id = $user->id;
    }

    public static function getFullname( $id )
    {
        $member = Member::where('user_id', $id)->first();
        if( $member ){
            
            return $member->full_name;
        }
        return '';
    }

    public static function getMemberByUserId($id)
    {
        return Member::where('user_id', $id)->first();
    }

    public static function checkIsChosenRole($userId, $memberType)
    {
        $member = Member::where('user_id', $userId)->first();
        if($member && $member->member_type_id == $memberType)        
            return true;  

        return false;
    }

    public static function getStudents()
    {
        $members = Member::where('member_type_id', BaseModel::$student)->get();
        for ($idxMember = 0; $idxMember < count($members); $idxMember++) { 
            if ( $members[$idxMember]->user_id ) {
                $user = User::find($members[$idxMember]->user_id);
                if( $user ){
                    $members[$idxMember]->username = $user->username;
                }                
            }
        }
        return $members;
    }

    public static function groupByMembers()
    {
        $result = array();
        $result[BaseModel::$salearea] = self::getSaleAreas();
        $result[BaseModel::$distributor] = self::getDistributor();
        $result[BaseModel::$agency] = self::getAgency();
        $result[BaseModel::$student] = self::getStudents();
        
        return $result;
    }

    public static function getLoadMoreStudent()
    {
        $students = Member::leftJoin('orders', function($join){
                                $join->on( 'orders.member_id', '=', 'members.id');
                                $join->whereNull('orders.deleted_at');
                            })
                            ->where('members.member_type_id', BaseModel::$student)                                                          
                            ->select(
                                'members.id AS id',                                
                                'members.full_name AS full_name',                                
                                'members.updated_at AS updated_at',                                
                                'members.status AS status',                                
                                DB::raw("count(orders.member_id) AS number_order")
                            )
                            ->groupBy('members.id')                                                     
                            ->orderBy('number_order', 'DESC')
                            ->paginate(5);        
        
        for ($idxMember = 0; $idxMember < count($students); $idxMember++) { 
            if ( $students[$idxMember]->user_id ) {
                $students[$idxMember]->username = User::find($students[$idxMember]->user_id)->username;
            }
        }
	    foreach($students as $student){
	      $student->order = Order::where('member_id',$student->id)->count();
        }

        return $students;
    }

    public static function getLoadMoreAgency()
    {
        $members = Member::where('member_type_id', BaseModel::$agency)
                        ->latest()
                        ->paginate(5);
        for ($idxMember = 0; $idxMember < count($members); $idxMember++) {
            if ($members[$idxMember]->user_id) {
                $members[$idxMember]->username = User::find($members[$idxMember]->user_id)->username;
            }
        }
        return $members;
    }

    public static function getLoadMoreSaleArea()
    {
        $members = Member::where('member_type_id', BaseModel::$salearea)
                        ->latest()
                        ->paginate(5);
        for ($idxMember = 0; $idxMember < count($members); $idxMember++) {
            if ($members[$idxMember]->user_id) {
                $members[$idxMember]->username = User::find($members[$idxMember]->user_id)->username;
            }
        }
        return $members;
    }

    public static function getLoadMoreDistributor()
    {
        $members = Member::where('member_type_id', BaseModel::$distributor)
                        ->latest()
                        ->paginate(5);
        for ($idxMember = 0; $idxMember < count($members); $idxMember++) {
            if ($members[$idxMember]->user_id) {
                $members[$idxMember]->username = User::find($members[$idxMember]->user_id)->username;
            }
        }
        return $members;
    }
}
