<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Media;

class MemberCoursesWatched extends BaseModelWithoutCache
{
    protected $table = 'member_courses_watched';

}
