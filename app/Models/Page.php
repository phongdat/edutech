<?php

namespace App\Models;

use App\Models\BaseModel;

class Page extends BaseModel
{
    public static function getParents()
    {
        return Page::where('parent_id', null)->get();
    }

    public static function getChilds()
    {
        return Page::where('parent_id', '!=', null)->get();
    }

    public static function getWithPAC()
    {
        $parents = Page::getParents();
        $childs = Page::getChilds();
        for ($idxParents = 0; $idxParents  < count($parents); $idxParents ++) {
            $parents[$idxParents]->has_child = false;
            $childsArr = [];
            for ($idxChilds = 0; $idxChilds  < count($childs); $idxChilds ++) {
                if ($parents[$idxParents]->id == $childs[$idxChilds]->parent_id) {
                    $parents[$idxParents]->has_child = true;
                    array_push($childsArr, $childs[$idxChilds]);
                }
            }
            $parents[$idxParents]->childs = $childsArr;
        }
        return $parents;
    }

    public static function getPageByPath($path)
    {
        return Page::where('slugify', '/'.$path)->first();
    }
}
