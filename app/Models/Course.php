<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\Media;
use App\Models\CourseCategory;
use App\Models\Member;

class Course extends BaseModel
{
    public static function getAllCourse()
    {
        $courses = Course::get();
        for ($idxCourse = 0; $idxCourse  < count($courses); $idxCourse ++) { 
            if ( $courses[$idxCourse]->banner_media_id ) {
                $courses[$idxCourse]->banner = Media::getBanner($courses[$idxCourse]->banner_media_id);
            }
            if ($courses[$idxCourse]->video_media_id) {
                $courses[$idxCourse]->video = Media::getVideo($courses[$idxCourse]->video_media_id);
            }
            if ( $courses[$idxCourse]->user_id ) {
                $courses[$idxCourse]->teacher_name = Member::find($courses[$idxCourse]->user_id)->full_name;
            }
        }
        return $courses;
    }

    public static function getAllCourseWithCate()
    {
        $courses = Course::get();
        for ($idxCourse = 0; $idxCourse  < count($courses); $idxCourse ++) { 
            if ( $courses[$idxCourse]->course_category_id ) {
                $courses[$idxCourse]->category = CourseCategory::find($courses[$idxCourse]->course_category_id);
            }
        }
        return $courses;
    }

    public static function getLoadMoreCourse()
    {
        $courses = Course::latest()->paginate(5);
        for ($idxCourse = 0; $idxCourse  < count($courses); $idxCourse ++) { 
            if ( $courses[$idxCourse]->banner_media_id ) {
                $courses[$idxCourse]->banner = Media::getBanner($courses[$idxCourse]->banner_media_id);
            }
            if ($courses[$idxCourse]->video_media_id) {
                $courses[$idxCourse]->video = Media::getVideo($courses[$idxCourse]->video_media_id);
            }
            if ( $courses[$idxCourse]->user_id ) {
                $courses[$idxCourse]->teacher_name = Member::find($courses[$idxCourse]->user_id)->full_name;
            }
        }
        return $courses;
    }

    public static function getDataExportCourse()
    {
        // $courseRs = [];
        $courses = Course::select('banner_media_id', 'title', 'user_id')->get();
        for ($idxCourse = 0; $idxCourse  < count($courses); $idxCourse++) {
            $course = new \stdclass();
            
            if ($courses[$idxCourse]->banner_media_id) {
                $courses[$idxCourse]->banner_media_id = Media::getBanner($courses[$idxCourse]->banner_media_id);
            }
            $courses[$idxCourse]->title =  $courses[$idxCourse]->title;

            if ($courses[$idxCourse]->user_id) {
                $courses[$idxCourse]->user_id = Member::find($courses[$idxCourse]->user_id)->full_name;
            }
            
            // array_push($courseRs, $course);
        }        
        return $courses;
    }

    public static function getCourseById( $id )
    {
        $course = Course::find( $id );
        if ($course->banner_media_id) {
            $course->banner = Media::getBanner($course->banner_media_id);
        }
        if ($course->video_media_id) {
            $course->video = Media::getVideo($course->video_media_id);
        }
        if ($course->user_id) {
            $teacher = Member::find($course->user_id);
            $course->teacher_name = $teacher->full_name;
            $course->teacher_id = $teacher->id;
        }
        return $course;
    }

}
