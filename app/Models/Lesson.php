<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\Media;
use App\Models\Member;

class Lesson extends BaseModel
{
    public static function getById ( $id )
    {
        $lesson = Lesson::find($id);
        $lesson->tag_list = [];
        if ( $lesson->tags ) {
            $tags = explode( ",",$lesson->tags);
            $lesson->tag_list = $tags;
        }
        if ($lesson->banner_media_id) {
            $lesson->banner = Media::getBanner($lesson->banner_media_id);
        }
        if ($lesson->video_media_id) {
            $lesson->video = Media::getVideo($lesson->video_media_id);
        }
        if ($lesson->user_id) {
            $teacher = Member::find($lesson->user_id);
            $lesson->teacher_name = $teacher->full_name;
            $lesson->teacher_id = $teacher->id;
        }
        return $lesson;
    }

    public static function getLoadMoreLesson()
    {
        $lessons = Lesson::latest()->paginate(5);
        for ($idxLesson = 0; $idxLesson  < count($lessons); $idxLesson ++) { 
            if ( $lessons[$idxLesson]->banner_media_id ) {
                $lessons[$idxLesson]->banner = Media::getBanner($lessons[$idxLesson]->banner_media_id);
            }
            if ($lessons[$idxLesson]->video_media_id) {
                $lessons[$idxLesson]->video = Media::getVideo($lessons[$idxLesson]->video_media_id);
            }
            if ( $lessons[$idxLesson]->user_id ) {
                $lessons[$idxLesson]->teacher_name = Member::find($lessons[$idxLesson]->user_id)->full_name;
            }
        }
        return $lessons;
    }

    public static function getAllLesson()
    {
        $lessons = Lesson::get();    
        for ($idxLesson = 0; $idxLesson  < count($lessons); $idxLesson ++) { 
            if ( $lessons[$idxLesson]->banner_media_id ) {
                $lessons[$idxLesson]->banner = Media::getBanner($lessons[$idxLesson]->banner_media_id);
            }
            if ($lessons[$idxLesson]->video_media_id) {
                $lessons[$idxLesson]->video = Media::getVideo($lessons[$idxLesson]->video_media_id);
            }
            if ( $lessons[$idxLesson]->user_id ) {
                $lessons[$idxLesson]->teacher_name = Member::find($lessons[$idxLesson]->user_id)->full_name;
            }
        }
        return $lessons;
    }
}
