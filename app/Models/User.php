<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\ResponseCache\Facades\ResponseCache;

class User extends Authenticatable
{
    use Notifiable;

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            // ... code here
        });

        self::created(function ($model) {
            exec("redis-cli FLUSHALL");
            ResponseCache::clear();
        });

        self::updating(function ($model) {
            // ... code here
        });

        self::updated(function ($model) {
            exec("redis-cli FLUSHALL");
            ResponseCache::clear();
        });

        self::deleting(function ($model) {
            // ... code here
        });

        self::deleted(function ($model) {
            exec("redis-cli FLUSHALL");
            ResponseCache::clear();
        });
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getUsersWithRole()
    {
        return User::where('role_id', '!=', null)->get();
    }
}
