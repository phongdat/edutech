<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\ProductCardAssignment;
use App\Models\Order;

class Product extends BaseModel
{
    public static function getProductSpecial()
    {
        return Product::where('id', 1)->first();
    }

    public static function getProductNormal()
    {
        return Product::where('id', '!=', 1)->get();
    }

    public static function getLoadMoreProductNormal()
    {
        return Product::where('id', '!=', 1)->latest()->paginate(5);
    }

    public static function getProductWithCard()
    {
        $prodResult = [];
        $orders = Order::select('card_id')->get();
        $orderCardIds = [];
        for ($idxOrder=0; $idxOrder < count($orders); $idxOrder++) { 
            array_push($orderCardIds, $orders[$idxOrder]->card_id);
        }
        $prod = Product::get();
        for ($idxProd = 0; $idxProd  < count($prod); $idxProd ++) {
            
            $prod[$idxProd]->card_count = ProductCardAssignment::where("product_id", $prod[$idxProd]->id)->count();
            if ( $prod[$idxProd]->card_count != 0 ) {
                array_push($prodResult, $prod[$idxProd] );
            } else if ($prod[$idxProd]->id == 1) {
                $prod[$idxProd]->card_count = Card::where('type', 'import')->whereNotIn('id', $orderCardIds)->count();
                array_push($prodResult, $prod[$idxProd]);
            }
        }
        return $prodResult;
    }
}
