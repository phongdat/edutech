<?php

namespace App\Models;

use App\Models\BaseModel;

class Media extends BaseModel
{
    protected $table = 'medias';


    public static function getAllImages()
    {
        return Media::where('media_type_id', 1)->get();
    }

    public static function getAllVideos()
    {
        return Media::where('media_type_id', 2)->get();
    }

    public static function getVideo( $videoMediaId )
    {
        return BaseModel::$apppUrl . Media::find($videoMediaId)->source;
    }

    public static function getBanner( $bannerMediaId ) 
    {
        return Media::find($bannerMediaId)->source;
    }
}
