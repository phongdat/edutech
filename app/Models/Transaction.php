<?php

namespace App\Models;

use App\Models\BaseModel;

class Transaction extends BaseModel
{
    public static function createTransaction()
    {
        
    }

    public static function getTransactions($member_id)
    {
        return Transaction::where('member_id', $member_id)->get();             
    }

    public static function getUsedPoint($memberId)
    {
        return Transaction::where('member_id', $memberId)->sum('used_point');             
    }
}