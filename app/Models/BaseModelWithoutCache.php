<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class BaseModelWithoutCache extends Model
{
    use SoftDeletes;
    protected static $apppUrl = 'http://edu.quanly.io/';

    // Members Type
    protected static $student = 1;
    protected static $teacher = 2;
    protected static $salearea = 3;
    protected static $distributor = 4;
    protected static $agency = 5;

    // Cards 
    protected static $cardNumber = 13;
    protected static $serialNumber = 11;

    // General page config
    protected static $introductionPage = '/admin/introduction';
    protected static $policyPage = '/admin/policy';
    protected static $agreementPage = '/admin/agreement';
    protected static $personnelPage = '/admin/personnel';
    protected static $instructionPage = '/admin/instruction';
    protected static $ministryPage = '/admin/ministry';

    /**
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];
}
