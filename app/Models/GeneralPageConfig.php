<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\Page;

class GeneralPageConfig extends BaseModel
{    
    public static function getCurrentPage($page)
    {        
        return GeneralPageConfig::where('page_id', $page->id)->first();
    }

    public static function getIntroductionPage()
    {
        $curPage = Page::where('slugify', BaseModel::$introductionPage)->first();
        return GeneralPageConfig::where('page_id', $curPage->id)->first();
    }

    public static function getPolicyPage()
    {
        $curPage = Page::where('slugify', BaseModel::$policyPage)->first();
        return GeneralPageConfig::where('page_id', $curPage->id)->first();
    }

    public static function getAgreementPage()
    {
        $curPage = Page::where('slugify', BaseModel::$agreementPage)->first();
        return GeneralPageConfig::where('page_id', $curPage->id)->first();
    }

    public static function getPersonnelPage()
    {
        $curPage = Page::where('slugify', BaseModel::$personnelPage)->first();
        return GeneralPageConfig::where('page_id', $curPage->id)->first();
    }

    public static function getInstructionPage()
    {
        $curPage = Page::where('slugify', BaseModel::$instructionPage)->first();
        return GeneralPageConfig::where('page_id', $curPage->id)->first();
    }

    public static function getMinistryPage()
    {
        $curPage = Page::where('slugify', BaseModel::$ministryPage)->first();
        return GeneralPageConfig::where('page_id', $curPage->id)->first();
    }
}