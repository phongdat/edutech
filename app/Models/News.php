<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Media;

class News extends BaseModel
{    
    protected $table = 'newses';

    public static function getDescNewsByDate()
    {
        $newses = News::orderBy('created_at', 'desc')->get();
        
        for ($i=0; $i < sizeof($newses); $i++) { 
            if ( $newses[$i]->banner_media_id ) {
                $newses[$i]->banner = Media::getBanner($newses[$i]->banner_media_id);
            }
            $newses[$i]->slugify_url = str_slug($newses[$i]->title .' '. $newses[$i]->id);
        }

        return  $newses;
    }

    public static function getLoadMoreNews()
    {
        $newses = News::latest()->paginate(5);
        
        for ($i=0; $i < sizeof($newses); $i++) { 
            if ( $newses[$i]->banner_media_id ) {
                $newses[$i]->banner = Media::getBanner($newses[$i]->banner_media_id);
            }
            $newses[$i]->slugify_url = str_slug($newses[$i]->title .' '. $newses[$i]->id);
        }

        return  $newses;
    }

    public static function getNumbOfDescNewsByDate( $num )
    {
        $newses = News::orderBy('created_at', 'desc')
                        ->take($num)
                        ->get();
        
        for ($i=0; $i < sizeof($newses); $i++) { 
            if ( $newses[$i]->banner_media_id ) {
                $newses[$i]->banner = Media::getBanner($newses[$i]->banner_media_id);
            }
            $newses[$i]->slugify_url = str_slug($newses[$i]->title .' '. $newses[$i]->id);
        }
        
        return $newses;
    }

    public static function getNewsById( $id )
    {
        $news = News::find($id);

        $news->tag_list = [];
        if ( $news->tags ) {
            $tags = explode( ",",$news->tags);
            $news->tag_list = $tags;
        }

        if ($news->banner_media_id) {
            $news->banner = Media::getBanner($news->banner_media_id);
        }
        $news->published_date = date_format($news->created_at,"d/m/Y H:i");
        $news->publisher = User::find($news->user_id)->username;
        $news->slugify_url = str_slug($news->title .' '. $news->id);

        return $news;
    }
}
