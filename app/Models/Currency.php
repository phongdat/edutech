<?php

namespace App\Models;

use Illuminate\Support\Facades\Hash;

use App\Models\BaseModel;
use App\Models\User;

class Currency extends BaseModel
{

    public static function createCurrency()
    {
        $currency = new Currency();
        $currency->name = 'Điểm';
        $currency->description = 'Đơn vị tính điểm tích luỹ mỗi lần nạp card.';
        $currency->save();
        return $currency;
    }
}