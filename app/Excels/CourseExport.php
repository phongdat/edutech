<?php

namespace App\Excels;

use App\Models\Course;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CourseExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Course::getDataExportCourse();
    }

    public function headings(): array
    {
        return [
            'Hình đại diện',
            'Tên khoá học',
            'Giảng viên',
        ];
    }
}
