<?php

namespace App\Excels;

use App\Models\Course;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class CourseExportWithView implements FromView, WithDrawings, WithEvents
{
    public function registerEvents(): array
    {
        return [
            // Using a class with an __invoke method.
            AfterSheet::class => function(AfterSheet $event){
                $columns = ['A', 'B', 'C'];
                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(19);                
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(141);                
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(16);                

            }
        ];
    }
    
    public function drawings()
    {
        $data = Course::getDataExportCourse();
        $thumbnail_arr = array();
        for ($i=0; $i < sizeof($data); $i++) { 
            $index = "A" . ($i + 2);
            $drawing = new Drawing();
            $drawing->setPath(public_path($data[$i]->banner_media_id));
            $drawing->setHeight(120);
            $drawing->setWidth(150);
            $drawing->setCoordinates($index);
            array_push($thumbnail_arr, $drawing);
        }        
        return $thumbnail_arr;
    }

    public function view(): View
    {
        $header = [
                    'Hình đại diện',
                    'Tên khoá học',
                    'Giảng viên',
        ];
        $data = Course::getDataExportCourse();
        return view('exports.course-export', compact('header', 'data'));
    }
}
