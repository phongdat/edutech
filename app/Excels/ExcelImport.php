<?php

namespace App\Excels;

use App\Models\Card;
use App\Models\ProductCardAssignment;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithProgressBar;

class ExcelImport implements ToModel, WithHeadingRow, WithChunkReading, WithBatchInserts, WithProgressBar
{
    use Importable;

    public function model(array $rows)
    {
        
        $cards = [];
        for ($idx = 0; $idx < count($rows); $idx++) {
            return new Card([
                'number'            => $rows['keycode'],
                'series'            => strval($rows['seri']),
                'type'              => "import",
                'group_series'      => Null
            ]);
        }
        return $cards;
    }
    
    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
