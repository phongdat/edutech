<html lang="{{ app()->getLocale() }}">

  @include('inc.head')
  <!-- toast CSS -->
    <link href="{{ url('plugins/css/jquery.toast.css') }}" id="theme" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
  @yield('header_styles')
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu">

      @include('inc.brand')
  <!-- navbar-fixed-top-->


  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark">

    <div class="main-menu-content menu-accordion">
      <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
        <li>
            <a href="{{url('admin/dashboard')}}"><i class="ft-bar-chart-2"> </i><span data-i18n="" class="menu-title">Thống kê</span></a>
        </li>
        <li class="navigation-header">
          <span>Chức năng</span>
          <i class=" ft-minus" ></i>
        </li>

        @if (isset($_pages))
          @foreach ($_pages as $page)
            @if ( $page->has_child )
              <li>
                <a href="{{url($page->slugify)}}"><i class="{{$page->icon}}"> </i><span data-i18n="" class="menu-title">{{ $page->name }}</span></a>
                <ul>
                  @foreach ($page->childs as $child)
                    <li>
                      <a href="{{url( $child->slugify )}}"><span data-i18n="" class="menu-title">{{ $child->name ? $child->name : "" }}</span></a>
                    </li>
                  @endforeach
                </ul>
              </li>
            @else
              <li>
                <a href="{{url($page->slugify)}}"><i class="ft-briefcase"> </i><span data-i18n="" class="menu-title">{{ $page->name }}</span></a>
              </li>
            @endif
          @endforeach
        @endif

        <li class="navigation-header">
          <span>Truy cập ADMIN</span>
          <i class=" ft-minus" ></i>
        </li>
        
        <li>
          <a href="{{url('admin/roles')}}"><i class="fas fa-users"> </i><span data-i18n="" class="menu-title">Groups</span></a>
        </li>

        <li>
            <a href="{{url('admin/users')}}"><i class="far fa-address-book"> </i><span data-i18n="" class="menu-title">Tài khoản</span></a>
        </li>
        
        <li>
            <a href="{{url('admin/functions')}}"><i class="fas fa-wrench"> </i><span data-i18n="" class="menu-title">Chức năng</span></a>
        </li>
      </ul>
    </div>

  </div>

  <div class="app-content content container-fluid" style="margin-bottom: 5em;">
    @yield('content')
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  @include('inc.footer')
  <script src="{{ mix('js/app.js') }}"></script>
  <script src="{{url('plugins/js/jquery.toast.js') }}"></script>
  <script src="//unpkg.com/jscroll/dist/jquery.jscroll.min.js"></script>
  @yield('footer_scripts')
  @if (session('error'))
        <script>
            $(document).ready(function() {
                $.toast({
                    heading: 'Oops...',
                    text: '{{ session("error") }}',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                    
                });
            });
        </script>
    @endif
    @if (session('success'))
        <script>
            $(document).ready(function() {
                $.toast({
                    heading: 'Congratulations...',
                    text: '{{ session("success") }}',
                    position: 'top-right',
                    loaderBg:'#17a2b8',
                    icon: 'success',
                    hideAfter: 3500
                    
                });
            });
        </script>
    @endif
  </body>
</html>
