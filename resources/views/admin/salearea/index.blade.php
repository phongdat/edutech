@extends('admin.root')
@section('content')
<div class="content-wrapper">
	<div class="content-header row">
		<div class="content-header-left col-md-6 col-xs-12 mb-1">
			<h4 class="content-header-title">
				<span> {{ $title }} </span>
				<a class="btn btn-success" href="{{url($urlNew)}}">Tạo mới</a>
			</h4>
		</div>
	</div>
	<div class="content-body">
		<div class="card">

			<!-- Issuers -->
			<div class="card-block">
				<div class="row">
					<div class="col-md-2 ">
						<div class="card-title">
							<strong>Họ và Tên</strong>
						</div>
					</div>

					<div class="col-md-2">
						<div class="card-title">
							Tài khoản
						</div>
					</div>

					<div class="col-md-2">
						<div class="card-title">
							Đã chuyển
						</div>
					</div>

					<div class="col-md-2">
						<div class="card-title">
							Đã nhận
						</div>
					</div>
				</div>
				@if($members && sizeof($members) > 0)
					<div class="infinite-scroll">
						@foreach( $members as $member)

							<div class="row bottom">

								<div class="col-md-2">
									<div class="form-group">
										<div class="tag tag-primary">

											<strong> {{ $member->full_name }}</strong>
										</div>

										<div class="tag tag-warning">

											<strong> {{$member->status ? "Active" : "DeActive"}}</strong>
										</div>

									</div>
									<div class="form-group">

									</div>

								</div>

								<div class="col-md-2">
									<div class="form-group">
										{{$member->username}}
									</div>
								</div>

								<div class="col-md-2">
									<div class="form-group">
										{{ $member->total_sending_order }}
									</div>
								</div>

								<div class="col-md-2">
									<div class="form-group">
										{{ $member->total_sended_order }}
									</div>
								</div>

								<div class="col-md-4">
									<a href="{{ url($urlUpdate . $member->id) }}" class="btn btn-success mr-1 float-xs-left">
										Update
									</a>

									<form class="form d-inline-block mr-1 float-xs-left" action="sales-area/paid" method="post">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<input type="hidden" name="member_id" value="{{ $member->id }}">
										<div class="input-group" style="display: inline-flex;">
											<span class="input-group-btn">
												<button type="submit" class="btn">
													Thanh toán
												</button>
											</span>
										</div>
									</form>

									<a class="btn btn-warning" href="{{url($urlTransfer . $member->id)}}">Chuyển thẻ</a>
								</div>
							</div>
						@endforeach
						{{ $members->links() }}        
					</div>
				@endif
			</div>
			<!--End Domains -->

		</div>
	</div>
</div>

@endsection

@section('footer_scripts')

<script type="text/javascript">
    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,            
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });
</script>

@stop