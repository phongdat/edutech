@extends('admin.root')
@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">
      <span> Danh sách thẻ </span> 
      </h4>
      <span> Học viên: {{$member->full_name}}</span>
    </div>
  </div>
  <div class="content-body">
    <div class="card">
      <!-- Issuers -->
      <div class="card-block" >
        <div class="row">
          <div class="col-md-3 ">
            <div class="card-title">
              <strong>Số thẻ</strong>
            </div>
          </div>
          <div class="col-md-2">
            <div class="card-title">
              <strong>Số seri</strong>
            </div>
          </div>
        </div>
        @foreach($orders as $order)
        <div class="row bottom">
          <div class="col-md-3">
            <div class="form-group">
              {{ isset($order->card) ? $order->card->number : "Dữ liệu database cũ" }}
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              {{ isset($order->card) ? $order->card->series : "Dữ liệu database cũ"}}
            </div> 
          </div>
        </div>
        @endforeach
      </div>
      <!--End Domains -->
    </div>
    
  </div>
  <div class="offset-md-11">
    <a href="{{url('admin/students')}}" class="btn btn-success">Trở về</a>
  </div>
</div>
@endsection