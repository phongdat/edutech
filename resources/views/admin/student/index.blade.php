@extends('admin.root')
@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">
      <span> Học viên </span>
      <a class="btn btn-success" href="{{url('admin/students/new')}}" >New</a>
      </h4>
    </div>
  </div>
  <div class="content-body">
    <div class="card">
      <!-- Issuers -->
      <div class="card-block" >
        <div class="row">
          <div class="col-md-5">
            <div class="col-md-6 pl-0">
              <div class="card-title">
                <strong>Họ và Tên</strong>
              </div>
            </div>
            <div class="col-md-6 p-0">
              <div class="card-title">
                <strong> Tên đăng nhập </strong>
              </div>
            </div>
          </div>
          <div class="col-md-7 p-0">
            <div class="col-md-2 p-0">
              <div class="card-title text-xs-center">
                <strong> Số thẻ</strong>
              </div>
            </div>
            <div class="col-md-4 p-0">
              <div class="card-title">
                <strong> Lần đăng nhập cuối </strong>
              </div>
            </div>
          </div>
        </div>
        @if($students && sizeof($students) > 0)
          <div class="infinite-scroll">
            @foreach($students as $student)
            <hr>
            <div class="row bottom">
              <div class="col-md-5">
                <div class="col-md-6 pl-0">
                  <div class="form-group">
                    <div class="tag tag-primary">
                      <strong> {{ $student->full_name }}</strong>
                    </div>
                    <div class="tag tag-warning">
                      <strong> {{$student->status ? "Active" : "DeActive"}}</strong>
                    </div>
                  </div>
                  <div class="form-group">
                  </div>
                </div>
                <div class="col-md-6 p-0">
                  <div class="form-group">
                    {{$student->username}}
                  </div>
                </div>
              </div>

              <div class="col-md-7 p-0">
                <div class="col-md-2 p-0">
                  <div class="form-group text-xs-center">
                    <strong>{{$student->number_order}}</strong>
                  </div> 
                </div>

                <div class="col-md-4 p-0">
                  <div class="form-group">
                    {{$student->updated_at}}
                  </div> 
                </div>

                <div class="col-md-6 p-0">
                  <a href="{{ url('/admin/students/update/' . $student->id) }}" class="btn-student-list btn btn-success">
                    Update
                  </a>

                  <a href="{{ url('/admin/students/delete/' . $student->id) }}" class="btn-student-list btn btn-danger">
                    Delete
                  </a>

                  <form class="form float-xs-left" action="{{ url('admin/students/deactive') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token()}}" >
                    <input type="hidden" name="id" value="{{ $student->id }}" >
                    <button type="submit" class="btn-student-list btn">
                    {{ $student->status ? 'DeActive' : 'Active' }}
                    </button>
                  </form>

                  @if($student->order>0)
                    <a href="{{url('/admin/students/listCard/'. $student->id)}}" class="btn-student-list btn btn-info">
                      List Cards
                    </a>
                  @endif
              </div>
              </div>

              
            </div>
            @endforeach
            {{ $students->links() }}
          </div>
        @endif
      </div>
      <!--End Domains -->
    </div>
  </div>
</div>
@endsection

@section('footer_scripts')

<!-- <script type="text/javascript">
    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,            
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });
</script> -->

@stop