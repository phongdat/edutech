@extends('admin.root')
@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">
        <span> Groups </span>
      <a class="btn btn-success" href="{{url('admin/roles/new')}}" >New</a>
    </h4>
    </div>

  </div>
  <div class="content-body">
<div class="card">

  <!-- Issuers -->
    <div class="card-block" >
        <div class="row">
          <div class="col-md-4 ">
              <div class="card-title">
                <strong>Role</strong>
              </div>
          </div>

          <div class="col-md-2">
              <div class="card-title">
                Created
              </div>
          </div>
          
        </div>
    @foreach( $roles as $role)

        <div class="row bottom">

          <div class="col-md-4">
              <div class="form-group">
              <div class="tag tag-primary">

                     <strong> {{ $role->name }}</strong>
              </div>

              <div class="tag tag-warning">

                    <strong> {{$role->status ? "Active" : "DeActive"}}</strong>
              </div>

              </div>
              <div class="form-group">

              </div>

          </div>

          <div class="col-md-2">
              <div class="form-group">
                {{$role->created_at}}
              </div>
          </div>
          
          <div class="col-md-1 pr-0">

                  <a href="{{ url('/admin/roles/update/' . $role->id) }}" class="btn btn-success">
                    Update
                  </a>
          </div>
          <div class="col-md-1 pr-0">

              <form class="form" action="issuer/del" method="post">
                  <input type="hidden" name="_token" value="{{csrf_token()}}" >
                  <button type="submit" class="btn">
                    {{ $role->status ? 'DeActive' : 'Active' }}
                  </button>
              </form>
          </div>
          <div class="col-md-1">
            <form class="form" action="issuer/del" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}" >
                <button type="submit" class="btn btn-danger mr-1">Delete</button>
            </form>
          </div>
        </div>
    @endforeach

  </div>
  <!--End Domains -->

</div>
  </div>
</div>

@endsection
