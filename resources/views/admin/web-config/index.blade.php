@extends('admin.root')
@section('header_styles')

@stop
@section('content')


@if(session('message'))
<div class="alert alert-success">
	{{session('message')}}
</div>
@endif

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h4 class="content-header-title">
                <span> Danh sách các cấu hình </span>
                <a class="btn btn-success" href="{{url('admin/webconfig/new')}}" >Tạo mới</a>
            </h4>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-block" >
                <div class="large-table-fake-top-scroll-container-3">
                    <div>&nbsp;</div>
                </div>
                <div class="large-table-container-3">
                    <table class="table">
                        <thead>
                            <tr>                       
                                <th>Tên cấu hình</th>   
                                <th>Giá trị</th>                                            
                                <th>Loại</th>
                                <th colspan="2">Thao tác</th>                        
                            </tr>
                        </thead>
                        @if(isset($data) )
                            <tbody>
                                @foreach($data as $dt)
                                    <tr>
                                        <td> {{ $dt->name }} </td>
                                        <td>
                                            @php ( $cur = trim($dt->value) )                                                                              
                                            @if($dt->type == 'icon')
                                                {!! $cur !!}
                                            @elseif($dt->type == 'image')
                                                <img class="img_review" style="height: 150px; width: 150px; object-fit: contain" src="{!! $cur !!}" alt="">
                                            @else                                                   
                                                {{ mb_strlen($cur) > 100 ? mb_substr($cur,0,100)."..." : $cur }}
                                            @endif
                                        </td>
                                        <td> {{ $dt->type }} </td>
                                        <td><a href="{{url('admin/webconfig/update/' . $dt->id)}}">
                                                <button type="button" 
                                                        class="btn btn-info">
                                                            Cập nhật
                                                </button>
                                            </a>
                                        </td>
                                        <td class="pl-0"><a href="{{url('admin/webconfig/delete/' . $dt->id)}}" onclick="return confirmAction()">
                                                <button type="button" 
                                                        class="btn btn-danger">
                                                            Xoá
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        @endif
                    </table>
                </div>            
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer_scripts')
<script>

</script>
@stop