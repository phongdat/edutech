@extends('admin.root')
@section('header_styles')

@stop
@section('content')


@if(session('message'))
<div class="alert alert-success">
	{{session('message')}}
</div>
@endif

<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="card">
            <div class="col-md-8 offset-md-2 col-xs-8 offset-xs-1 box-shadow-1 p-0">
                <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                    <div class="card-body collapse in">
                        <div class="card-header">
                            <h4 class="card-title"> {{ $title }} </h4>
                        </div>
                        <div class="card-block">
                            <form class="form-horizontal form-simple" method="post" action="{{url('admin/webconfig/save')}}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">                                                                
                                <input type="hidden" name="id" value="{{ isset($data) ? $data->id : 0 }}">                    
                                    <div class="form-group">
                                        <label> Tên cấu hình </label>
                                        <input type="text" 
                                                class="form-control " 
                                                name="name_config" 
                                                placeholder="" 
                                                value=" {{ isset($data) ? $data->name : "" }}">
                                        <br>
                                        <label> Giá trị </label>                                                
                                        @if(isset($data))

                                            @if ($data->type == "textbox")
                                                <div id="textbox">
                                                    <input type="text" 
                                                            class="form-control"
                                                            id="textbox"  
                                                            name="textbox" 
                                                            placeholder="" 
                                                            value=" {{ isset($data) ? $data->value : "" }}">
                                                </div>

                                            @elseif ($data->type == "textarea")

                                                <div id="textbox">
                                                    <textarea class="form-control"
                                                                id="textarea"  
                                                                name="textarea" 
                                                                placeholder="">{{ isset($data) ? $data->value : "" }}</textarea>
                                                </div>

                                            @elseif ($data->type == "image")   
                                                <div id="image">
                                                    <br>

                                                    <img id="holder" 
                                                        onchange="changeImage()"
                                                        style="height: 150px; width: 150px; object-fit: contain" hidden/>

                                                    <img id="preview-image" 
                                                            src="{{ isset($data) ? $data->value : "" }}" 
                                                            style="height: 150px; width: 150px; object-fit: contain" 
                                                            class="" />       

                                                    <button type="button" 
                                                            class="btn btn-default add_file_btn" 
                                                            id="lfm" data-input="thumbnail" 
                                                            data-preview="holder">
                                                                <i class="fa fa-plus fa-3x"></i>
                                                    </button>

                                                    <input type="hidden" id="image" name="image" value="{{ isset($data) ? $data->value : "" }}">  
                                                </div>

                                            @elseif ($data->type == "icon")
                                                <div id="image">
                                                    <textarea class="form-control"
                                                                id="icon"  
                                                                name="icon" 
                                                                placeholder="">{{ isset($data) ? $data->value : "" }}
                                                    </textarea>        
                                                </div>
                                                                        
                                            @endif

                                        @else
                                            
                                            <div id="textbox">
                                                <input type="text" 
                                                            class="form-control"
                                                            id="textbox" 
                                                            name="textbox" 
                                                            placeholder="" 
                                                            value="">
                                            </div>

                                            <div id="textarea">
                                                <textarea class="form-control"
                                                            id="textarea"  
                                                            name="textarea" 
                                                            placeholder=""></textarea>
                                            </div>

                                            <div id="image">
                                                <br>
                                                <img id="holder" 
                                                    onchange="changeImage()"
                                                    style="height: 150px; width: 150px; object-fit: contain" hidden/>

                                                <img id="preview-image"                                                         
                                                        src="" 
                                                        style="height: 150px; width: 150px; object-fit: contain"
                                                        class="" />       

                                                <button type="button" 
                                                        class="btn btn-default add_file_btn" 
                                                        id="lfm" data-input="thumbnail" 
                                                        data-preview="holder">
                                                            <i class="fa fa-plus fa-3x"></i>
                                                </button>

                                                <input type="hidden" id="image" name="image" value="">  

                                            </div>

                                            <div id="icon">
                                                <textarea class="form-control"
                                                            id="icon"  
                                                            name="icon" 
                                                            placeholder=""></textarea>
                                            </div>

                                        @endif
                                    </div>
                                <div class="form-group">
                                    <label> Loại thuộc tính </label>
                                    <select class="form-control" id="slType" name="type" value="{{ isset($data) ? $data->type : '' }}">
                                        @foreach($asset as $key => $value)
                                        <option value="{{ $key }}" <?php if (isset($data) && $data->type == $key)
                                                                        echo 'selected';
                                                                    ?>>
                                            {{$value}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-actions right">
                                    <button class="btn btn-primary" type="submit" name="save" value="save">Cập nhật</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('footer_scripts')
<script src="{{ url('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
<script>
     $(document).ready(function(){
        $('#lfm').filemanager('file');
        var data = {!! json_encode($data) !!};
        if(data == null){
            $('#textarea').css('display', 'none');
            $('#image').css('display', 'none');
            $('#icon').css('display', 'none');
        }      
    });

    function changeImage(){
        $('#preview-image').attr('src', $('#holder').attr('src'));
        $('input[name=image]').val($('#holder').attr('src'));
    }

    $('#slType').on('change', function() {
        $('#textbox').css('display', 'none');
        $('#textarea').css('display', 'none');
        $('#image').css('display', 'none');
        $('#icon').css('display', 'none');
        $('#' + this.value).css('display', 'block');
    });

</script>
@stop