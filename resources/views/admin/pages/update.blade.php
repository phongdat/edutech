@extends('admin.root')
@section('header_styles')
<style>
        .form-group input[type="checkbox"] {
            display: none;
        }

        .form-group input[type="checkbox"] + .btn-group > label span {
            width: 20px;
            height: 20px;
        }

        .form-group input[type="checkbox"] + .btn-group > label span:first-child {
            display: none;
        }
        .form-group input[type="checkbox"] + .btn-group > label span:last-child {
            display: inline-block;   
        }

        .form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
            display: inline-block;
        }
        .form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
            display: none;   
        }
    </style>
@stop

@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">Function</h4>
    </div>

  </div>
  <div class="content-body">
<div class="card">
  <div class="col-md-6 offset-md-2 col-xs-10 offset-xs-1 box-shadow-1 p-0">
<div class="card border-grey border-lighten-3 px-2 py-2 m-0">

  <div class="card-body collapse in">
    <div class="card-block">
      <form class="form-horizontal form-simple"
       method="post"
       action="{{url('/admin/functions/save')}}">

       <input type="hidden" name="_token" value="{{csrf_token()}}">
       <input type="hidden" name="id" value="{{ isset($page) ? $page->id : 0 }}" />

        <fieldset class="form-group position-relative has-icon-left mb-1">

          <input type="text" class="form-control form-control-lg input-lg"
           name="name"
          placeholder="Tên chức năng" value="{{ isset($page) ? $page->name : '' }}">

          <div class="form-control-position">
              <i class="ft-briefcase"></i>
          </div>
        </fieldset>
        
        <fieldset class="form-group position-relative has-icon-left mb-1">

          <input type="text" class="form-control form-control-lg input-lg"
           name="slugify"
          placeholder="Đường dẫn"" value="{{ isset($page) ? $page->slugify : '' }}">

          <div class="form-control-position">
              <i class="ft-briefcase"></i>
          </div>
        </fieldset>

        <fieldset class="form-group position-relative has-icon-left mb-1">

          <textarea class="form-control form-control-lg input-lg"
           name="description">{{ isset($page) ? $page->description : '' }}</textarea>

          <div class="form-control-position">
              <i class="ft-briefcase"></i>
          </div>
        </fieldset>

        <div class="row">

        <div class="col-md-12">
            <table class="table table-striped table-bordered table-advance table-hover">
                <thead>
                    <th></th>
                    <th>View</th>
                    <th>Edit</th>
                    <th>Add</th>
                    <th>All</th>
                </thead>
                <tbody>
                        <?php $idxAc = 0; ?>
                    @foreach ($roles as $index => $role)
                    <input type="hidden" name="role_id[]" value="{{ $role->id }}" />
                    <tr>
                        <td>{{$role->name}}</td>
                        @foreach ($controls as $idxControl => $control)
                            <td>
                                <div class="form-group">
                                <input class="role_control_status" type="checkbox" data-resource="" id="role-{{$role->id}}-control-{{$idxControl}}" data-role-id="{{$role->id}}" name="{{ $role->id }}-{{ $control->id }}[]" {{ isset($accessControls) && isset($accessControls[$idxAc]) ? (
                                    $accessControls[$idxAc]->control_id == $control->id && $accessControls[$idxAc]->role_id == $role->id && $accessControls[$idxAc]->status ? 'checked' : ''
                                    ) : '' }}/>
                                    <div class="btn-group">
                                        <label for="role-{{ $role->id}}-control-{{ $idxControl }}" class="btn btn-primary">
                                            <span class="fa fa-check"></span>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </td>
                        <?php $idxAc++; ?>
                        @endforeach
                        <td>
                            <div class="form-group">
                                <input type="checkbox" class="role_control_all" data-resource="" id="role-{{$role->id}}-control-4" name="all[]" data-role-id="{{$role->id}}"/>
                                <div class="btn-group">
                                    <label for="role-{{$role->id}}-control-4" class="btn btn-primary ">
                                        <span class="fa fa-check"></span>
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="col-md-6">
          <fieldset class="form-group position-relative has-icon-left mb-1">
            <select name="parent" class="form-control">
                <option value="0"> không chọn </option>
              @foreach ( $parents as $parent )
                <option value="{{ $parent->id }}" {{ isset($page) && $page->parent_id == $parent->id ? "selected" : ""}}>{{ $parent->name }}></option>
              @endforeach
            </select>
          </fieldset>
        </div>

        <div class="col-md-6">
              <div class="form-control p-0">
                  <input type="text" name="icons" class="form-control social-icon" placeholder="Click để chọn icon">
              </div>
        </div>


        <div class="col-md-6 mt-1" >
          <button type="submit" class="btn btn-primary btn-lg btn-block">
            Next </button>
        </div>

      </div>
          </form>
    </div>
  </div>
</div>
</div>
</div>
  </div>
</div>


<script src="//code.jquery.com/jquery-2.2.1.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{url('js/fontawesome-iconpicker.js')}}"></script>
<script type="text/javascript">
    $('.social-icon').iconpicker();
</script>
@endsection
@section('footer_scripts')
    <script>

    $(".role_control_all").click(function () {
        $(".role_control_status[data-role-id^=" + $(this).data("role-id") + "]").click();
    });

</script>
@stop
