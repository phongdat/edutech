@extends('admin.root')
@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">
        <span> Chức năng </span>
      <a class="btn btn-success" href="{{url('admin/functions/new')}}" >Tạo mới</a>
    </h4>
    </div>

  </div>
  <div class="content-body">
<div class="card">

  <!-- Header -->
  <div id="search" class="card-block" >

      <form class="form" action="{{url('admin/functions/search')}}" method="post">
        <div class="form-actions top clearfix">
          <input type="hidden" name="_token" value="{{csrf_token()}}">

          <div class="row">
            <div class="col-md-4">
                      <div class="form-group">

                        <input name="name" type="text"
                        class="form-control" placeholder="Tên Function">
                      </div>
              </div>

              <div class="col-md-1">
                <button type="submit" class="btn">Search</button>
              </div>
            </div>
              </div>
        </form>

  </div>
<!--  End Header -->



  <!-- Issuers -->
    <div class="card-block" >
        <div class="row">
          <div class="col-md-4 ">
              <div class="card-title">
                <strong>Tên Function</strong>
              </div>
          </div>

          <div class="col-md-2">
              <div class="card-title">
                Đường dẫn
              </div>
          </div>
          
          <div class="col-md-2">
              <div class="card-title">
                Ghi chú
              </div>
          </div>
          
        </div>
    @foreach( $pages as $page)

        <div class="row bottom">

          <div class="col-md-4">
            {{ $page->name }}
          </div>

          <div class="col-md-2">
              <div class="form-group">
                {{$page->slugify}}
              </div>
          </div>
          
          <div class="col-md-2">
              <div class="form-group">
                {{$page->description}}
              </div>
          </div>
          
          <div class="col-md-1">

                  <a href="{{ url('/admin/functions/update/' . $page->id) }}" class="btn btn-success">
                    Update
                  </a>
          </div>
         
          <div class="col-md-1">
            <form class="form" action="{{ url('/admin/functions/delete/' . $page->id) }}" method="get">
                <input type="hidden" name="_token" value="{{csrf_token()}}" >
                <button type="submit" class="btn btn-danger mr-1">Delete</button>
            </form>
          </div>
        </div>
    @endforeach

  </div>
  <!--End Domains -->

</div>
  </div>
</div>

@endsection
