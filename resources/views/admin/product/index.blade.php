@extends('admin.root')
@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">
        <span> Danh sách sản phẩm </span>
        <a class="btn btn-success" href="{{url('admin/product/new')}}" >Tạo mới</a>
    </h4>
    </div>
    <div class="content-header-right col-md-6 col-xs-12 mb-1 " >
      <a class="btn btn-warning" style="float: right;" href="{{url('admin/card-configure')}}" >Cấu hình</a>
    </div>
  </div>
  <div class="content-body">
<div class="card">

  <!-- Header -->
  <div id="search" class="card-block" >

      <form class="form" action="{{url('admin/functions/search')}}" method="post">
        <div class="form-actions top clearfix">
          <input type="hidden" name="_token" value="{{csrf_token()}}">

          <div class="row">
            <div class="col-md-4">
                      <div class="form-group">

                        <input name="name" type="text"
                        class="form-control" placeholder="Tên sản phẩm">
                      </div>
              </div>

              <div class="col-md-1">
                <button type="submit" class="btn">Search</button>
              </div>
            </div>
              </div>
        </form>

  </div>
<!--  End Header -->



  <!-- Issuers -->
    <div class="card-block" >
        <div class="row">
          <div class="col-md-2 ">
              <div class="card-title">
                <strong>Banner</strong>
              </div>
          </div>

          <div class="col-md-2">
              <div class="card-title">
                <strong>Video</strong>
              </div>
          </div>
          
          <div class="col-md-2">
              <div class="card-title">
                Tên khoá học
              </div>
          </div>
          
          <div class="col-md-2">
              <div class="card-title">
                Giảng viên
              </div>
          </div>
          
        </div>
    @if($products && sizeof($products) > 0)
      <div class="infinite-scroll">
        @foreach( $products as $product)

            <div class="row bottom">

              <div class="col-md-2">
                  {{ $product->name }}
              </div>

              <div class="col-md-2">
                  <div class="form-group">
                    
                  </div>
              </div>
              <div class="col-md-2">
                  <div class="form-group">
                  </div>
              </div>
              
              <div class="col-md-2">
                  <div class="form-group">
                  </div>
              </div>
              
              <div class="col-md-1">

                      <a href="{{ url('/admin/product/update/' . $product->id) }}" class="btn btn-success">
                        Update
                      </a>
              </div>
            
              <div class="col-md-1">
                <form class="form" action="issuer/del" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" >
                    <button type="submit" class="btn btn-danger mr-1">Delete</button>
                </form>
              </div>
            </div>
        @endforeach
        {{ $products->links() }}        
      </div>
    @endif

  </div>
  <!--End Domains -->

</div>
  </div>
</div>

@endsection

@section('footer_scripts')

<script type="text/javascript">
    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,            
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });
</script>

@stop