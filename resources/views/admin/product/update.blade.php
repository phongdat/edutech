@extends('admin.root')
@section('header_styles')

@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h4 class="content-header-title">Quản lý sản phẩm</h4>
        </div>
        
    </div>

    <div class="content-body">
        <div class="card">
            <div class="col-md-6 offset-md-2 col-xs-10 offset-xs-1 box-shadow-1 p-0">
                <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
    
                    <div class="card-body collapse in">
                        <div class="card-block">
                            <form class="form-horizontal form-simple" method="post" action="{{url('/admin/product/save')}}">
    
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="id" value="{{ isset($product) ? $product->id : 0 }}" />
                                
                                <fieldset class="form-group position-relative has-icon-left mb-1">
                                
                                    <input type="text" class="form-control form-control-lg input-lg" name="name" placeholder="Tên sản phẩm"
                                        value="{{ isset($product) ? $product->name : '' }}">
                                
                                    <div class="form-control-position">
                                        <i class="ft-briefcase"></i>
                                    </div>
                                </fieldset>
                                
                                <fieldset class="form-group position-relative mb-1">
                                
                                    <select name="course_category_id[]" id="course_id" class="form-control" data-tags="true" multiple="multiple">
                                        <option value="0">Không thuộc danh mục nào</option>
                                        @foreach ($coures as $course)
                                        <option value="{{ $course->id }}"
                                            {{ isset($product) ? ($course->selected ? "selected" : "")  : "" }}>{{ $course->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </fieldset>

                                <fieldset class="form-group position-relative has-label-left mb-1">
                                    <input type="number" class="form-control form-control-lg input-lg" name="quantity" placeholder="Số lượng thẻ" step="{{ $config->value }}" {{ isset($product) ? '' : 'min="10"' }}
                                        value="0">
                                </fieldset>
                                
                                <div class="row">
                                
                                    <div class="col-md-6">
                                        <a class="btn btn-warning" href="{{ url('admin/products') }}">Đóng</a>
                                    </div>
                                
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block">Lưu</button>
                                
                                    </div>
                                </div>
    
    
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</div>

@endsection
@section('footer_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script>
    $(document).ready(function(){
        $('#course_id').select2({
            placeholder: "Type and press enter",
            tags: true
        });
    });
</script>
@stop
