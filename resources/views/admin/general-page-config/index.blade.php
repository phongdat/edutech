@extends('admin.root')
@section('header_styles')

@stop
@section('content')


@if(session('message'))
<div class="alert alert-success">
	{{session('message')}}
</div>
@endif

<form action="{{url('admin/general-page-config/save')}}" method="post" class="form-horizontal form-simple" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
	<input type="hidden" name="id" value="{{ isset($curPageConfig) ? $curPageConfig->id : 0 }}">
	<input type="hidden" name="page-id" value="{{ isset($curPage) ? $curPage->id : 0 }}">
	<div class="lesson content-wrapper">
        <div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h4 class="content-header-title"><a href="#"> {{ isset($curPageConfig) ? $curPageConfig->title : "" }} </a></h4>
			</div>
        </div>
        <div class="content-body">
            <div class="lesson-left col-md-8">
				<div class="card border-grey border-lighten-3 px-2 py-2 m-0 box-shadow-1">
					<input type="text" name="title" class="lesson-title" rows="1" placeholder="Thêm tiêu đề" value="{{ isset($curPageConfig) ? $curPageConfig->title : "" }}">
					<textarea class="lesson-content" rows="1" placeholder="Thêm nội dung" name="content">{{ isset($curPageConfig) ? $curPageConfig->content : "" }}</textarea>
                </div>                
			</div>
			<div class="lesson-right col-md-4">
				<row>
					<div class="col-md-6 pl-0 text-xs-center" style="border: solid 1px #00A5A8; border-radius: 4px;">
						<a class="btn btn-basic" href="{{ url('admin') }}">Đóng</a>
					</div>
					<div class="col-md-6 pr-0">
						<button type="submit" class="btn btn-success" name="save" value="save">Lưu</button>
					</div>
				</row>
			</div>
        </div>
    </div>
</form>

@endsection

@section('footer_scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
	var textarea = document.querySelector('textarea');
	textarea.addEventListener('keydown', autosize);
	
	function autosize(){
		var el = this;
		setTimeout(function(){
			el.style.cssText = 'height:auto';
			// for box-sizing other than "content-box" use:
			// el.style.cssText = '-moz-box-sizing:content-box';
			el.style.cssText = 'height:' + el.scrollHeight + 'px';
		},0);
	}
</script>
<script>

	var editor_config = {
		path_absolute : "/",
		selector: "textarea",
		plugins: [
		"advlist autolink lists image charmap preview hr ",
		"searchreplace wordcount visualblocks visualchars code",
		"insertdatetime nonbreaking table contextmenu directionality",
		"emoticons paste textcolor colorpicker textpattern link"
		],
		menubar: 'file edit view insert format tools table tc help',
		toolbar: 'insertfile undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
		relative_urls: false,
		file_browser_callback : function(field_name, url, type, win) {
			var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
			var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

			var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
			if (type == 'image') {
				cmsURL = cmsURL + "&type=Files";
			} else {
				cmsURL = cmsURL + "&type=Files";
			}

			tinyMCE.activeEditor.windowManager.open({
				file : cmsURL,
				title : 'Quản lý hình ảnh',
				width : x * 0.8,
				height: y * 0.8,
				resizable : "yes",
				close_previous : "no"
			});
		},	
		height: 480	
	};

	tinymce.init(editor_config);
</script>
@stop