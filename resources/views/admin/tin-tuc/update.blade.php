@extends('admin.root')
@section('header_styles')
<link href="https://transloadit.edgly.net/releases/uppy/v1.4.0/uppy.min.css" rel="stylesheet">
@stop
@section('content')


@if(session('message'))
<div class="alert alert-success">
	{{session('message')}}
</div>
@endif
<form action="{{url('admin/newses/save')}}" method="post" class="form-horizontal form-simple" enctype="multipart/form-data">
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<input type="hidden" name="id" value="{{ isset($news) ? $news->id : 0 }}">
	<div class="lesson content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h4 class="content-header-title"><a href="#"> Tin tức </a></h4>
			</div>
		</div>
		<div class="content-body">
			<div class="lesson-left col-md-8">
				<div class="card border-grey border-lighten-3 px-2 py-2 m-0 box-shadow-1">
					<input type="text" name="title" class="lesson-title" rows="1" placeholder="Thêm tiêu đề" value="{{ isset($news) ? $news->title : "" }}">					
					<textarea name="content" rows="100" id="my-editor" class="form-control my-editor">{{ isset($news) ? $news->content : "" }}</textarea>
				</div>
			</div>
			<div class="lesson-right col-md-4">
				<div class="card border-grey border-lighten-3 px-2 py-2 box-shadow-1">
					<h4 class="content-header-title">Banner</h4>
					<div class="input-group">
						
						<span class="input-group-btn">
							<a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
								<i class="fa fa-picture-o"></i> Choose
							</a>
						</span>
						<input id="thumbnail" class="form-control" type="text" name="filepath" >
					</div>
					<img id="holder" style="margin-top:15px;max-height:100px;">
					<img id="preview-image" src="{{ ( isset($news) && isset($news->banner_media_id) ) ? url($news->banner) : "" }}" style="margin-top:15px;max-height:100px;" class="" />                            
				</div>
				
				<div class="card border-grey border-lighten-3 px-2 py-2 box-shadow-1">
					<h4 class="content-header-title">Tóm tắt</h4>
					<div id="drag-drop-area"></div>
					<textarea name="summary" id="summary" cols="30" rows="10">{{ isset($news) ? $news->summary : "" }}</textarea>					
				</div>
				
				<div class="card border-grey border-lighten-3 px-2 py-2 box-shadow-1 mt-1">
					<h4 class="content-header-title"> Tags </h4>
					<select name="tags[]" data-tags="true" id="tags" class="form-control" multiple="multiple">
						<option value=""></option>
						@if ( isset($news) )
                            @foreach ($news->tag_list as $tag)
                                <option value="{{ $tag }}" selected>{{ $tag }}</option>
                            @endforeach
						@endif
					</select>
                </div>
                
				<row>
					<div class="col-md-6 pl-0">
						<a class="btn btn-warning w-100" href="{{ url('admin/newses') }}">Đóng</a>
					</div>
					<div class="col-md-6 pr-0">
						<button type="submit" class="btn btn-success" name="save" value="save">Lưu</button>
					</div>
				</row>
			</div>
		</div>
	</div>
</form>

@endsection

@section('footer_scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
	var textarea = document.querySelector('textarea');
	textarea.addEventListener('keydown', autosize);
	
	function autosize(){
		var el = this;
		setTimeout(function(){
			el.style.cssText = 'height:auto';
			// for box-sizing other than "content-box" use:
			// el.style.cssText = '-moz-box-sizing:content-box';
			el.style.cssText = 'height:' + el.scrollHeight + 'px';
		},0);
	}
</script>
<script>

var editor_config = {
		path_absolute : "/",
		selector: "textarea.my-editor",
		plugins: [
		"advlist autolink lists image charmap preview hr ",
		"searchreplace wordcount visualblocks visualchars code",
		"insertdatetime nonbreaking table contextmenu directionality",
		"emoticons paste textcolor colorpicker textpattern"
		],
		menubar: 'file edit view insert format tools table tc help',
		toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
		relative_urls: false,
		file_browser_callback : function(field_name, url, type, win) {
			var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
			var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

			var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
			if (type == 'image') {
				cmsURL = cmsURL + "&type=Files";
			} else {
				cmsURL = cmsURL + "&type=Files";
			}

			tinyMCE.activeEditor.windowManager.open({
				file : cmsURL,
				title : 'Quản lý hình ảnh',
				width : x * 0.8,
				height: y * 0.8,
				resizable : "yes",
				close_previous : "no"
			});
		},	
		height: 480	
	};

	tinymce.init(editor_config);

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="{{ url('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
<script>
	$(document).ready(function(){
		$('#tags').select2({
			placeholder: "Type and press enter",
			tags: true
		});
		$('.uppy-Dashboard-inner').css("min-height", '100px');
		$('#lfm').filemanager('file');
	});
</script>
@stop