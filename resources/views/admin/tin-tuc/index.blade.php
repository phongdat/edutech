@extends('admin.root')
@section('header_styles')
<style>
.table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 0;
    
}

.table th, .table td {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #fff;
}

</style>
@stop
@section('content')


@if(session('message'))
<div class="alert alert-success">
	{{session('message')}}
</div>
@endif

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h4 class="content-header-title">
                <span> Danh sách tin tức </span>
                <a class="btn btn-success" href="{{url('admin/newses/new')}}" >Tạo mới</a>
            </h4>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-block" >
                <div class="large-table-fake-top-scroll-container-3">
                    <div>&nbsp;</div>
                </div>
                <div class="large-table-container-3">
                <table class="table">
                    <thead>
                        <tr>                       
                            <th class="col-md-2">Banner</th>   
                            <th class="col-md-3">Tiêu đề</th>                                            
                            <th class="col-md-5">Tóm tắt</th>
                            <th class="col-md-2">Thao tác</th>                        
                        </tr>
                    </thead> 
                </table>
                @if($newses && sizeof($newses) > 0)
                    <div class="infinite-scroll">         
                        <table class="table">                                                         
                            <tbody>                       
                                @foreach($newses as $news)
                                    <tr>
                                        <td class="col-md-2 border-0" style="border: transparent;">
                                            @if ($news->banner)
                                                    <img class="img_review" 
                                                        style="height: 150px; width: 150px; object-fit: contain" 
                                                        src="{!! $news->banner !!}" 
                                                        alt="">
                                            @else
                                                <img src="/client_assets/image/noimage.jpg" 
                                                    alt="image"
                                                    style="height: 150px; width: 150px; object-fit: contain" >
                                            @endif
                                        </td>
                                        <td class="col-md-3 border-0" style="border: transparent;"> {{ $news->title }} </td>
                                        <td class="col-md-5 border-0" style="border: transparent;">                       
                                            {{ mb_strlen($news->summary) > 100 ? mb_substr($news->summary,0,100)."..." : $news->summary }}
                                        </td>                                        
                                        <td class="col-md-2 border-0" style="border: transparent;">
                                            <a href="{{url('admin/newses/update/' . $news->id)}}">
                                                <button type="button" 
                                                        class="btn btn-info">
                                                            Cập nhật
                                                </button>
                                            </a>
                                            <a href="{{url('admin/newses/delete/' . $news->id)}}" onclick="return confirmAction()">
                                                <button type="button" 
                                                        class="btn btn-danger">
                                                            Xoá
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                @endforeach
                                {{ $newses->links() }}                                    
                            </tbody>                       
                        </table>
                    </div>
                @endif
                </div>            
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer_scripts')

<script type="text/javascript">
    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,            
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });

    function confirmAction() {
      return confirm('Bạn có chắc rằng muốn xoá dữ liệu này? Dữ liệu bị xoá sẽ không phục hồi được!!!');
    }
</script>

@stop