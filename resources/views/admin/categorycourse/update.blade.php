@extends('admin.root')
@section('header_styles')
<style>
.form-group input[type="checkbox"] {
display: none;
}
.form-group input[type="checkbox"] + .btn-group > label span {
width: 20px;
height: 20px;
}
.form-group input[type="checkbox"] + .btn-group > label span:first-child {
display: none;
}
.form-group input[type="checkbox"] + .btn-group > label span:last-child {
display: inline-block;
}
.form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
display: inline-block;
}
.form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
display: none;
}
</style>
@stop
@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">Function</h4>
    </div>
  </div>
  <div class="content-body">
    <div class="card">
      <div class="col-md-12 col-xs-10 p-0">
        <div class="card px-2 py-2 m-0 bg-transparent">
          <div class="card-body collapse in">
            <div class="card-block box-shadow-1 bg-white mb-2">
              <form class="form-horizontal form-simple mb-0"
                method="post"
                action="{{url('/admin/category/course/save')}}">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="id" value="{{ isset($page) ? $page->id : 0 }}" />
                <input type="hidden" name="category_id" value="{{ isset($category) ? $category->id : 0 }}" />
                <fieldset class="form-group position-relative has-icon-left mb-1">
                  <input type="text" class="form-control form-control-lg input-lg"
                  name="name" placeholder="Tên danh mục" value="{{ isset($category) ? $category->name : '' }}">
                  <div class="form-control-position">
                    <i class="ft-briefcase"></i>
                  </div>
                </fieldset>
                
                <fieldset class="form-group position-relative has-icon-left mb-1">
                  <textarea class="form-control form-control-lg input-lg"
                  name="description" placeholder="Mô tả danh mục khoá học">{{ isset($category) ? $category->description : '' }}</textarea>
                  <div class="form-control-position">
                    <i class="ft-briefcase"></i>
                  </div>
                </fieldset>
                <div class="row">
                  
                  <div class="col-md-4">
                    <fieldset class="form-group position-relative has-icon-left mb-0">
                      <select name="parent" class="form-control">
                        @foreach ( $parents as $parent )
                        <option value="{{ $parent->id }}">{{ $parent->name }}</option>
                        @endforeach
                      </select>
                    </fieldset>
                  </div>
                  @if(isset($category->id) && $category->id != null)
                  <div class="col-md-2 offset-md-4" >
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                    Update </button>
                  </div>
                  <div class="col-md-2" >
                    <a href="{{url('admin/category/course/new')}}" class="btn btn-primary btn-lg btn-block">
                    Back </a>
                  </div>
                  @else
                  <div class="col-md-2 offset-md-6" >
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                    Next </button>
                  </div>
                  @endif
                </div>
              </form>
            </div>
            <!-- Issuers -->
            
            <div class="card-block box-shadow-1 bg-white" >
              <div class="row">
                <div class="col-md-2">
                  <div class="card-title mb-0">
                    <strong>Đối tượng</strong>
                  </div>
                </div>
                <div class="col-md-10">
                  <div class="card-title mb-0">
                    <strong>Mô tả</strong>
                  </div>
                </div>
               
              </div>
              <hr>
              @foreach($categories as $category)
              <div class="row bottom">
                <div class="col-md-2">
                  <div class="form-group tag tag-primary update-tag">
                      {{$category->name}}
                  </div>
                </div>
                <div class="col-md-8">
                  <div class="form-group">
                    <strong> {{$category->description}} </strong>
                  </div>
                </div>
                
                <div class="col-md-1 pr-0">
                  <a href="{{url('/admin/category/course/update/'.$category->id)}}" class="btn btn-success">
                    Update
                  </a>
                </div>
                <div class="col-md-1 pl-0">
                  <a href="{{url('/admin/category/course/delete/'.$category->id)}}" class="btn btn-danger float-xs-right"> Delete </a>
                </div>
              </div>

              <hr>
              @endforeach
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footer_scripts')
<script>
$(".role_control_all").click(function () {
$(".role_control_status[data-role-id^=" + $(this).data("role-id") + "]").click();
});
</script>
@stop