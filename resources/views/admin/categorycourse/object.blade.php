@extends('admin.root')
@section('header_styles')
<style>
.form-group input[type="checkbox"] {
display: none;
}
.form-group input[type="checkbox"] + .btn-group > label span {
width: 20px;
height: 20px;
}
.form-group input[type="checkbox"] + .btn-group > label span:first-child {
display: none;
}
.form-group input[type="checkbox"] + .btn-group > label span:last-child {
display: inline-block;
}
.form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
display: inline-block;
}
.form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
display: none;
}
</style>
@stop
@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">Function</h4>
    </div>
  </div>
  <div class="content-body">
    <div class="card">
      <div class="col-md-12 p-0">
        <div class="card bg-transparent px-2 py-2 m-0">
          <div class="card-body collapse in">
            <div class="card-block box-shadow-1 mb-2 bg-white">
              <form class="form-horizontal form-simple mb-0"
                method="post"
                action="{{url('/admin/object/save')}}">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="id" value="{{ isset($page) ? $page->id : 0 }}" />
                <input type="hidden" name="object_id" value="{{ isset($object) ? $object->id : 0 }}" />
                <fieldset class="form-group position-relative has-icon-left mb-1">
                  <input type="text" class="form-control form-control-lg input-lg"
                  name="name"
                  placeholder="Tên đối tượng" value="{{ isset($object) ? $object->name : '' }}">
                  <div class="form-control-position">
                    <i class="ft-briefcase"></i>
                  </div>
                </fieldset>
                
                <fieldset class="form-group position-relative has-icon-left mb-1">
                  <textarea class="form-control form-control-lg input-lg"
                  name="description" placeholder="Mô tả">{{ isset($object) ? $object->description : '' }}</textarea>
                  <div class="form-control-position">
                    <i class="ft-briefcase"></i>
                  </div>
                </fieldset>

                @if(isset($object->id) && $object->id != null)
                <div class="row">
                  <div class="col-md-2 offset-md-8">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                    Update </button>
                  </div>
                  <div class="col-md-2" >
                    <a href="{{url('admin/object')}}" class="btn btn-primary btn-lg btn-block">Back</a>
                  </div>
                </div>
                @else
                <div class="row">
                  <div class="col-md-2 offset-md-10" >
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                    Next </button>
                  </div>
                </div>
                @endif
              </form>
            </div>
            <!-- Issuers -->
            <div class="card-block box-shadow-1 bg-white" >
              <div class="row">
                <div class="col-md-2">
                  <div class="card-title">
                    <strong>Đối tượng</strong>
                  </div>
                </div>
                <div class="col-md-10">
                  <div class="card-title">
                    <strong>Mô tả</strong>
                  </div>
                </div>
                
              </div>
              @foreach($objects as $object )
              <div class="row bottom">
                <div class="col-md-2">
                  <div class="form-group">
                    <div class="tag tag-primary">
                      <strong> {{$object->name}}  </strong>
                    </div>
                  </div>
                </div>
                <div class="col-md-8">
                  <div class="form-group">
                    {{$object->description}}
                  </div>
                </div>
                
                <div class="col-md-1 pr-0">
                  <a href="{{url('admin/object/'.$object->id)}}" class="btn btn-success">
                    Update
                  </a>
                </div>
                <div class="col-md-1 pl-0">
                  <a href="{{url('admin/object/delete/'.$object->id)}}" class="btn btn-danger float-xs-right"> Delete </a>
                </div>
              </div>
              <hr>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
  @section('footer_scripts')
  <script>
  $(".role_control_all").click(function () {
  $(".role_control_status[data-role-id^=" + $(this).data("role-id") + "]").click();
  });
  </script>
  @stop