@extends('admin.root')
@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">
        <span> Bài giảng </span>
      <a class="btn btn-success" href="{{url('admin/lessons/new')}}" >Tạo mới</a>
    </h4>
    </div>

  </div>
  <div class="content-body">
<div class="card">

  <!-- Header -->
  <div id="search" class="card-block" >

      <form class="form" action="{{url('admin/lessons/search')}}" method="post">
        <div class="form-actions top clearfix">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <div class="row">
            <div class="col-md-4">
                      <div class="form-group">

                        <input name="name" type="text"
                        class="form-control" placeholder="Tên bài giảng" value="{{ isset($name) ? $name : "" }}">
                      </div>
              </div>
              <div class="col-md-1">
                <button type="submit" class="btn">Search</button>
              </div>
            </div>
              </div>
        </form>
        <form class="form" action="{{url('admin/lessons/filter')}}" method="post">
        <div class="form-actions top clearfix">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <div class="row">
            <div class="col-md-3">
              <h4 class="content-header-title"> Khoá học </h4>
              <select name="category" id="category" class="form-control"> 
                <option value="0">Danh mục khoá học</option>
                @foreach ($categories as $category)
                  <option value="{{ $category->id }}" {{ isset($sl_category) ? ($sl_category == $category->id ? "selected" : "")  : "" }}>{{ $category->title }} -- [{{ $category->category->name }}]</option>
                @endforeach               
              </select>
            </div>
            <div class="col-md-3">
              <h4 class="content-header-title"> Đối tượng </h4>
              <select name="object" id="object" class="form-control"> 
                <option value="0">Danh mục đối tượng</option>
                @foreach ($objects as $object)
                  <option value="{{ $object->id }}" {{ isset($sl_object) ? ($sl_object == $object->id ? "selected" : "")  : "" }}>{{ $object->name }}</option>
                @endforeach
              </select>   
            </div>
            <div class="col-md-3">
              <h4 class="content-header-title"> Lớp học </h4>
              <select name="classes" id="classes" class="form-control"> 
                <option value="0">Danh mục lớp học</option>
                @foreach ($classes as $class)
                  <option value="{{ $class->id }}" {{ isset($sl_class) ? ($sl_class == $class->id ? "selected" : "")  : "" }}>{{ $class->name }}</option>
                @endforeach
              </select>  
            </div>
            <div class="col-md-3">           
            <h4 class="content-header-title"> &nbsp; </h4> 
              <button class="btn btn-info" type="submit">Filter</button>              
            </div>
          </div>
        </div>
      </form>
      <div class="row">        
        <div class="col-md-4">
          <h4 class="content-header-title"> Hiển thị: {{ sizeof( $lessons ) }} / {{ $lessons->total() }} </h4>           
        </div>                
      </div>
  </div>
<!--  End Header -->



  <!-- Issuers -->
    <div class="card-block" >
        <div class="row">
          <div class="col-md-4 ">
              <div class="card-title">
                <strong>Tên bài giảng</strong>
              </div>
          </div>

          <div class="col-md-2">
              <div class="card-title">
                Video
              </div>
          </div>
          
          <div class="col-md-2">
              <div class="card-title">
                Tags
              </div>
          </div>   

          <div class="col-md-1">
              <div class="card-title">
                Status
              </div>
          </div>          
        </div>
        <hr>
    @if($lessons && sizeof($lessons) > 0)
      <div class="infinite-scroll">
        @foreach( $lessons as $lesson)
            <div class="row bottom">
              <div class="col-md-4">
                {{ $lesson->title }}
              </div>

              <div class="col-md-2">
                  @if ( $lesson->video) 
                    <video style="margin-top:15px;max-height:100px;" controls>
                      <source src="{{ isset($lesson->video) ? url($lesson->video) : "" }}" type="video/mp4">
                    </video>
                      @else
                        Không có video nào
                      @endif
              </div>
              
              <div class="col-md-2">
                  <div class="form-group">
                    {{$lesson->tags}}
                  </div>
              </div>

              <div class="col-md-1">
                  <div class="form-group">
                    {{$lesson->status}}
                  </div>
              </div>
              
              <div class="col-md-1">

                      <a href="{{ url('/admin/lessons/update/' . $lesson->id) }}" class="btn btn-success">
                        Update
                      </a>
              </div>
            
              <div class="col-md-1">
                <form class="form" action="{{ url('admin/lessons/del') }}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" >
                    <input type="hidden" name="id" value="{{ $lesson->id }}" >
                    <button type="submit" class="btn btn-danger mr-1">Delete</button>
                </form>
              </div>
            </div>
            <hr>
        @endforeach
        @if(isset($name))
          {{ $lessons->appends(['value' => $name])->links() }}        
        @elseif(isset($filter))
          {{ $lessons->appends(['cate' => $sl_category, 'obj' => $sl_object, 'cla' => $sl_class])->links() }} 
        @else
          {{ $lessons->links() }}        
        @endif     
      </div>
    @endif
  </div>
  <!--End Domains -->

</div>
  </div>
</div>

@endsection

@section('footer_scripts')

<script type="text/javascript">
    // $('ul.pagination').hide();
    // $(function() {
    //     $('.infinite-scroll').jscroll({
    //         autoTrigger: true,            
    //         padding: 0,
    //         nextSelector: '.pagination li.active + li a',
    //         contentSelector: 'div.infinite-scroll',
    //         callback: function() {
    //             $('ul.pagination').remove();
    //         }
    //     });
    // });

    $(document).ready(function(){
      $('#category').select2({
        placeholder: "Danh mục khoá học",
        tags: true
      });        
      $('#object').select2({
        placeholder: "Danh mục đối tượng",
        tags: true
      });        
      $('#classes').select2({
        placeholder: "Danh mục lớp học",
        tags: true
      });   
    });

</script>

@stop
