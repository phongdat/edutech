@extends('admin.root')
@section('header_styles')
<link href="https://transloadit.edgly.net/releases/uppy/v1.4.0/uppy.min.css" rel="stylesheet">
@stop
@section('content')


@if(session('message'))
<div class="alert alert-success">
	{{session('message')}}
</div>
@endif
<form action="{{url('admin/lessons/save')}}" method="post" class="form-horizontal form-simple" enctype="multipart/form-data">
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<input type="hidden" name="id" value="{{ isset($lesson) ? $lesson->id : 0 }}">
	<div class="lesson content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h4 class="content-header-title"><a href="#"> Bài giảng </a></h4>
			</div>
		</div>
		<div class="content-body">
			<div class="lesson-left col-md-8">
				<div class="card border-grey border-lighten-3 px-2 py-2 m-0 box-shadow-1">
					<input type="text" name="title" class="lesson-title" rows="1" placeholder="Thêm tiêu đề" value="{{ isset($lesson) ? $lesson->title : "" }}">
					<textarea class="lesson-content" rows="1" placeholder="Thêm nội dung" name="content">{{ isset($lesson) ? $lesson->content : "" }}</textarea>
				</div>
			</div>
			<div class="lesson-right col-md-4">
				<div class="card border-grey border-lighten-3 px-2 py-2 box-shadow-1">
					<h4 class="content-header-title">Banner</h4>
					<div class="input-group">
						
						<span class="input-group-btn">
							<a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
								<i class="fa fa-picture-o"></i> Chọn hình
							</a>
						</span>
						<input id="thumbnail" class="form-control" type="text" name="filepath" >
					</div>
					<img id="holder" style="margin-top:15px;max-height:100px;">
					<img id="preview-image" src="{{ isset($lesson) ? url($lesson->banner) : "" }}" style="margin-top:15px;max-height:100px;" class="" />                            
				</div>
				
				<div class="card border-grey border-lighten-3 px-2 py-2 box-shadow-1">
					<h4 class="content-header-title">Video</h4>
					<input type="hidden" id="lesson_video" name="video">
					<input type="hidden" id="lesson_video_extension" name="video_extension">
					<div id="drag-drop-area"></div>
					@if(isset($lesson->video))
					<video  style="margin-top:15px;max-height:100px;" controls>
						<source src="{{ isset($lesson->video) ? url($lesson->video) : "" }}" type="video/mp4">
					</video>
					@endif
				</div>
				
				<div class="card border-grey border-lighten-3 px-2 py-2 box-shadow-1 mt-1">
					<h4 class="content-header-title"> Tags </h4>
					<select name="tags[]" data-tags="true" id="tags" class="form-control" multiple="multiple">
						<option value=""></option>
						@if ( isset($lesson) )
						@foreach ($lesson->tag_list as $tag)
						<option value="{{ $tag }}" selected>{{ $tag }}</option>
						@endforeach
						@endif
					</select>
				</div>
				
				<div class="card border-grey border-lighten-3 px-2 py-2 box-shadow-1 mt-1">
					<h4 class="content-header-title">Khoá học</h4>
					<div class="form-check">
						<select name="course_id" id="course_id" class="form-control">
							<option value="0">Không thuộc danh mục nào</option>
							@foreach ($courses as $course)
								<option value="{{ $course->id }}" {{ isset($lesson) ? ($lesson->course_id == $course->id ? "selected" : "")  : "" }}>{{ $course->title }}</option>
							@endforeach
						</select>
					</div>
				</div>
				
				<div class="card border-grey border-lighten-3 px-2 py-2 box-shadow-1 mt-1">
					<h4 class="content-header-title"> Giảng viên </h4>
					<select name="teacher" id="teacher" class="form-control">
						@foreach ($teachers as $teacher)
							<option value="{{ $teacher->id }}" {{ isset($lesson) ? ($lesson->user_id == $teacher->id ? "selected" : "")  : "" }}>{{ $teacher->full_name }}</option>
						@endforeach
					</select>
				</div>

				<div class="card border-grey border-lighten-3 px-2 py-2 box-shadow-1 mt-1">
					<h4 class="content-header-title"> Giảng viên duyệt bài </h4>
					<select name="approvers[]" id="approvers" class="form-control" multiple="multiple">						
						@foreach ( $teachers as $teacher )

							@php( $check_selected = false )
							@if( isset( $rating_members ) )
								@foreach ( $rating_members as $rating_member )

									@if( $rating_member->member_id == $teacher->id )

										{{ $check_selected = true }}
										@break

									@endif
									
								@endforeach
							@endif

							<option value="{{ $teacher->id }}" {{ $check_selected == true ?  "selected" : "" }}>{{ $teacher->full_name }}</option>
						@endforeach
					</select>
				</div>

				<row>
					<div class="col-md-6 pl-0">
						<a class="btn btn-warning w-100" href="{{ url('admin/lessons') }}">Đóng</a>
					</div>
					<div class="col-md-6 pr-0">
						<button type="submit" class="btn btn-success" name="save" value="save">Lưu</button>
					</div>
				</row>
			</div>
		</div>
	</div>
</form>

@endsection

@section('footer_scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
	var textarea = document.querySelector('textarea');
	textarea.addEventListener('keydown', autosize);
	
	function autosize(){
		var el = this;
		setTimeout(function(){
			el.style.cssText = 'height:auto';
			// for box-sizing other than "content-box" use:
			// el.style.cssText = '-moz-box-sizing:content-box';
			el.style.cssText = 'height:' + el.scrollHeight + 'px';
		},0);
	}
</script>
<script>
	tinymce.init({
		selector: 'textarea',
		plugins: 'link',
		plugins: "image",
		menubar: true,
		height: 500,
		resize: true
	});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="{{ url('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
<script src="https://transloadit.edgly.net/releases/uppy/v1.4.0/uppy.min.js"></script>
<script>
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	var uppy = Uppy.Core()
        .use(Uppy.Dashboard, {
			inline: true,
			target: '#drag-drop-area',
			height: 170,
        })
        .use(Uppy.Tus, {
			endpoint: 'http://edu.quanly.io:3111/files/',
			// endpoint: 'http://localhost:3111/files/',
			chunkSize: 500000,
		})

      uppy.on('complete', (result) => {
		$("#lesson_video_extension").val(result.successful[0].type);
		$("#lesson_video").val(result.successful[0].uploadURL);
      })
</script>
<script>
	$(document).ready(function(){
		$('#tags').select2({
			placeholder: "Đánh chữ và nhấn Enter",
			tags: true
		});

		$('#approvers').select2({
			placeholder: "Xin hãy chọn giảng viên phê duyệt",
		});
		
		$('#teacher').select2();
		$('#course_id').select2();

		$('.uppy-Dashboard-inner').css("min-height", '100px');
		$('#lfm').filemanager('file');
	});
</script>
@stop