@extends('admin.root')

@section('header_styles')
<style>
  legend{
    width: auto !important;
    font-size: 16px;
    font-weight: bold;
    color: blue;    
  }

  fieldset {
    border: 1px solid #c0c0c0 !important;
    margin: 0 2px !important;
    padding: 0.35em 0.625em 0.75em !important;
    border-radius: 10px;
  }

  fieldset .scheduler-border {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
  }

  legend .scheduler-border {
    font-size: 1.2em !important;
    font-weight: bold !important;
    text-align: left !important;
    width:auto !important;
    padding:0 10px !important;
    border-bottom:none !important;
  }
</style>
@endsection

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h4 class="content-header-title">
        <span> Danh sách bài giảng chờ phê duyệt </span>
      <!-- <a class="btn btn-success" href="{{url('admin/lessons/new')}}" >Tạo mới</a> -->
    </h4>
        </div>
    </div>
    <div class="content-body">
        <div class="card">

            <!-- Header -->
            <div id="search" class="card-block">
                <form class="form" action="{{url('admin/lessons/search')}}" method="post">
                    <div class="form-actions top clearfix">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input name="name" type="text" class="form-control" placeholder="Tên bài giảng" value="{{ isset($name) ? $name : " " }}">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <button type="submit" class="btn">Search</button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-4">
                        <h4 class="content-header-title"> Hiển thị: </h4>
                    </div>
                </div>
            </div>
            <!--  End Header -->

            <!-- Issuers -->
            <div class="card-block">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card-title">
                            <strong>Bài giảng</strong>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="card-title">
                            <strong>Đánh giá</strong>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card-title">
                            <strong>Danh sách phản hồi</strong>
                        </div>
                    </div>
                </div>
                <hr> @if( $lessons && sizeof($lessons) > 0 )
                <div class="infinite-scroll">
                    @foreach( $lessons as $lesson)
                    <div class="row bottom">
                        <div class="col-md-4">
                            <p>
                                <b><u>Banner:</u>  </b> @if ($lesson->banner)
                                <br>
                                <img id="preview-image" src="{{ isset($lesson) ? url($lesson->banner) : " " }}" style="max-height:150px;" class="" /> @else Không có banner @endif
                            </p>

                            <p>
                                <b><u>Tiêu đề:</u>  </b> {{ $lesson->title }}
                            </p>

                            <p>
                                <b><u>Gíao viên giảng dạy:</u>  </b> {{ $lesson->teacher_name }}
                            </p>

                            <p>
                                <b><u>Tags:</u>  </b> {{ $lesson->tags }}
                            </p>

                            <p><b><u>Nội dung:</u></b> {!! $lesson->content !!}
                            </p>

                            <p><b><u>Video:</u></b> @if ( $lesson->video)
                                <br>
                                <video style="max-height:100px;" controls>
                                    <source src="{{ isset($lesson->video) ? url($lesson->video) : " " }}" type="video/mp4">
                                </video>
                                @else Không có video nào @endif
                            </p>
                        </div>
                        <div class="col-md-2">
                          <span style="color:red">{{$lesson->rating}}</span> sao
                        </div>
                        <div class="col-md-4">
                          @foreach( $lesson->rating_lessons as $rating_lesson)
                            <fieldset>
                              <legend>{{ $rating_lesson->teacher_name }}</legend>
                              <b>Đánh giá: </b> <span style="color:red">{{ isset( $rating_lesson->rating ) ? $rating_lesson->rating : 0 }}</span> sao <br/> 
                              <b>Nội dung: </b> <span style="color:green">{{ $rating_lesson->content }}</span>
                            </fieldset>
                          @endforeach                          
                        </div>
                        <div class="col-md-2">
                          <form class="form" action="{{ url('admin/lessons/approved') }}" method="post">
                              <input type="hidden" name="_token" value="{{csrf_token()}}">
                              <input type="hidden" name="id" value="{{ $lesson->id }}">
                              <button type="submit" class="btn btn-success">Duyệt</button>
                          </form>
                          <p></p>
                          <form class="form" action="{{ url('admin/lessons/no-approved') }}" method="post">
                              <input type="hidden" name="_token" value="{{csrf_token()}}">
                              <input type="hidden" name="id" value="{{ $lesson->id }}">
                              <button type="submit" class="btn btn-danger mr-1">Không duyệt</button>
                          </form>
                        </div>
                    </div>
                    <hr> @endforeach {{ $lessons->links() }}
                </div>
                @endif
            </div>
            <!--End Domains -->
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')

@stop
