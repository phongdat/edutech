@extends('admin.root')
@section('content')
<input type="hidden" value="{{ $monthNewStudent }}" id="monthNewStudent">
<input type="hidden" value="{{ $monthOnline }}" id="monthOnline">
<input type="hidden" value="{{ $monthActive }}" id="monthActive">
<div class="app-content container center-layout mt-2">
	<div class="content-wrapper">
		<div class="content-body"><!-- Stats -->

		<div class="row">
			<div class="col-xl-3 col-lg-6 col-xs-12">
				<div class="card">
					<div class="card-body">
						<div class="media">
							<div class="p-2 text-xs-center bg-primary bg-darken-2 media-left media-middle">
								<i class="icon-camera font-large-2 white"></i>
							</div>
							<div class="p-2 bg-gradient-x-primary white media-body">
								<h5>Học viên mới</h5>
								<h5 class="text-bold-400"><i class="ft-arrow-up"></i>{{$members->count}}</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-6 col-xs-12">
				<div class="card">
					<div class="card-body">
						<div class="media">
							<div class="p-2 text-xs-center bg-danger bg-darken-2 media-left media-middle">
								<i class="icon-user font-large-2 white"></i>
							</div>
							<div class="p-2 bg-gradient-x-danger white media-body">
								<h5>Thẻ kích hoạt</h5>
								<h5 class="text-bold-400"><i class="ft-arrow-up"></i>{{$active}}</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-6 col-xs-12">
				<div class="card">
					<div class="card-body">
						<div class="media">
							<div class="p-2 text-xs-center bg-warning bg-darken-2 media-left media-middle">
								<i class="icon-basket-loaded font-large-2 white"></i>
							</div>
							<div class="p-2 bg-gradient-x-warning white media-body">
								<h5>Thẻ tồn kho</h5>
								<h5 class="text-bold-400"><i class="ft-arrow-down"></i> {{$stock}}</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-6 col-xs-12">
				<div class="card">
					<div class="card-body">
						<div class="media">
							<div class="p-2 text-xs-center bg-success bg-darken-2 media-left media-middle">
								<i class="icon-wallet font-large-2 white"></i>
							</div>
							<div class="p-2 bg-gradient-x-success white media-body">
								<h5>Thẻ chuyển</h5>
								<h5 class="text-bold-400"><i class="ft-arrow-up"></i> {{$countstrans}}</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row match-height">
			<!--Product sale-->
			<div class="col-xl-8 col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Thống kê học viên theo tháng</h4>
						<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
						<div class="heading-elements">
							<ul class="list-inline mb-0">
								<li><a href="{{url('/admin')}}"><i class="ft-rotate-cw"></i></a></li>
								<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="card-body collapse in">
						<div class="card-block">
							<div id="products-sales" class="height-300"></div>
						</div>
					</div>
				</div>
			</div>
			<!--/////////////////////////////////////////////////////////-->

			<!--Recent Buyers -->
			<div class="col-xl-4 col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Thẻ đã chuyển</h4>
						<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
						<div class="heading-elements">
							<ul class="list-inline mb-0">
								<li><a href="{{url('/admin')}}"><i class="ft-rotate-cw"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="card-body px-1">
						<div id="recent-buyers" class="list-group height-300 position-relative">
							<div class="clearfix">
								<div class="col-md-5 p-0 text-xs-center">
									<span class="avatar avatar-md avatar-online w-100"> <strong>Đại lý</strong> 
									</span>
								</div>
								<div class="col-md-4 p-0 text-xs-center">
									<span class="list-group-item-heading"> <strong>Đã chuyển</strong> </span>
								</div>
								<div class="col-md-3 p-0 text-xs-center">
									<span class="list-group-item-heading"><strong>Đã nhận</strong></span>
								</div>
							</div>
							<hr>
							@foreach($saleareas as $salearea)
							<div class="">
								<a href="#" class="list-group-item list-group-item-action media no-border pl-0 pr-0">
									<div class="col-md-5 p-0 text-xs-center">
										<span class="avatar avatar-md avatar-online w-100"> {{$salearea->full_name}}
										</span>
									</div>
									<div class="col-md-4 p-0 text-xs-center">
										<span class="list-group-item-heading"> {{$salearea->total_sending_order}} </span>
									</div>
									<div class="col-md-3 p-0 text-xs-center">
										<span class="list-group-item-heading"> {{$salearea->total_sended_order}} </span>
									</div>
								</a>
							</div>
							@endforeach
						</div>
						</div>
					</div>
				</div>
			</div>
			<!--/////////////////////////////////////////////////////////-->
		</div>

		<div class="row match-height">
			<!--Recent Orders-->
			<div class="col-xl-8 col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Số lượng thẻ order chưa thanh toán</h4>
						<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
						<div class="heading-elements">
							<ul class="list-inline mb-0">
								<li><a href="{{url('/admin')}}"><i class="ft-rotate-cw"></i></a></li>
								<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="card-body">
						<div class="card-block">
							<p>Đã thanh toán {{$paid}} thẻ, chưa thanh toán {{$unpaid}} thẻ.</p>
						</div>
						<div class="table-responsive vertical-scroll scroll-example height-300 no-border">
							<table id="recent-orders" class="table table-hover mb-0 ps-container ps-theme-default">
								<thead>
									<tr>
										<th>Đại lý</th>
										<th>Thẻ đã chuyển</th>
										<th>Trạng thái</th>
									</tr>
								</thead>

								<tbody >
									@foreach($transaction as $stran)
									<tr>
										<td class="text-truncate">{{$stran->member->full_name}}</td>
										<td class="text-truncate"><a href="#">{{$stran->card->series}}</a></td>
										<td class="text-truncate"><span class="tag tag-default tag-success">{{$stran->status}}</span></td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!--/////////////////////////////////////////////////////////-->

			<!--Total monthly Sales-->
			<div class="col-xl-4 col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="card-block sales-growth-chart">
							<div id="monthly-sales" class="height-250"></div>
						</div>
					</div>
					<div class="card-footer">
						<div class="chart-title mb-1 text-xs-center">
							<h6>Thẻ đã kích hoạt</h6>
						</div>
					</div>
				</div>
			</div>
			<!--/////////////////////////////////////////////////////////-->

		</div>
	</div>
</div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->
@endsection

@section('footer_scripts')
<script src="{{url('js/raphael-min.js')}}"></script>
 <script src="{{url('js/morris.min.js')}}"></script>
 <script src="{{url('js/dashboard-ecommerce.js')}}"></script>
@stop