@extends('admin.root')
@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">
        <span> Users </span>
      <a class="btn btn-success" href="{{url('admin/users/new')}}" >New</a>
    </h4>
    </div>

  </div>
  <div class="content-body">
<div class="card">

  <!-- Header -->
  <div id="search" class="card-block" >

      <form class="form" action="{{url('admin/users/search')}}" method="post">
        <div class="form-actions top clearfix">
          <input type="hidden" name="_token" value="{{csrf_token()}}">

          <div class="row">
            <div class="col-md-4">
                      <div class="form-group">

                        <input name="username" type="text"
                        class="form-control" placeholder="User Name">
                      </div>
              </div>

              <div class="col-md-1">
                <button type="submit" class="btn">Search</button>
              </div>
            </div>
              </div>
        </form>

  </div>
<!--  End Header -->



  <!-- Issuers -->
    <div class="card-block" >
        <div class="row">
          <div class="col-md-4 ">
              <div class="card-title">
                <strong>User</strong>
              </div>
          </div>

          <div class="col-md-2">
              <div class="card-title">
                Created
              </div>
          </div>
          
        </div>
    @foreach( $users as $user)

        <div class="row bottom">

          <div class="col-md-4">
              <div class="form-group">
              <div class="tag tag-primary">

                     <strong> {{ $user->username }}</strong>
              </div>

              <div class="tag tag-success">

                    <strong> {{$user->role_name}}</strong>
              </div>

              <div class="tag tag-warning">

                    <strong> {{$user->status ? "Active" : "DeActive"}}</strong>
              </div>

              </div>
              <div class="form-group">

              </div>

          </div>

          <div class="col-md-2">
              <div class="form-group">
                {{$user->created_at}}
              </div>
          </div>
          
          <div class="col-md-1">

                  <a href="{{ url('/admin/users/update/' . $user->id) }}" class="btn btn-success">
                    Update
                  </a>
          </div>
          <div class="col-md-1">

              <form class="form" action="issuer/del" method="post">
                  <input type="hidden" name="_token" value="{{csrf_token()}}" >
                  <button type="submit" class="btn">
                    {{ $user->status ? 'DeActive' : 'Active' }}
                  </button>
              </form>
          </div>
          <div class="col-md-1">
            <form class="form" action="issuer/del" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}" >
                <button type="submit" class="btn btn-danger mr-1">Delete</button>
            </form>
          </div>
        </div>
    @endforeach

  </div>
  <!--End Domains -->

</div>
  </div>
</div>

@endsection
