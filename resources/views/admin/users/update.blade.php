@extends('admin.root')
@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">User</h4>
    </div>

  </div>
  <div class="content-body">
<div class="card">
  <div class="col-md-6 offset-md-2 col-xs-10 offset-xs-1 box-shadow-1 p-0">
<div class="card border-grey border-lighten-3 px-2 py-2 m-0">

  <div class="card-body collapse in">
    <div class="card-block">
      <form class="form-horizontal form-simple"
       method="post"
       action="{{url('/admin/users/save')}}">
       <input type="hidden" name="_token" value="{{csrf_token()}}">
       <input type="hidden" name="id" value="{{ isset($user) ? $user->id : 0 }}" />

        <fieldset class="form-group position-relative has-icon-left mb-1">

          <input type="text" class="form-control form-control-lg input-lg"
           name="username"
          placeholder="UserName" value="{{ isset($user) ? $user->username : '' }}">

          <div class="form-control-position">
              <i class="ft-briefcase"></i>
          </div>
        </fieldset>
        
        <fieldset class="form-group position-relative has-icon-left mb-1">

          <input type="password" {{ isset($user) ? '' : 'required' }}  class="form-control form-control-lg input-lg"
           name="password"
          placeholder="Password" value="">

          <div class="form-control-position">
              <i class="ft-briefcase"></i>
          </div>
        </fieldset>

        <fieldset class="form-group position-relative has-icon-left mb-1">

          <input type="text" class="form-control form-control-lg input-lg"
           name="email"
          placeholder="abc@gmail.com" value="{{ isset($user) ? $user->email : '' }}">

          <div class="form-control-position">
              <i class="ft-briefcase"></i>
          </div>
        </fieldset>

        <div class="row">

        <div class="col-md-6">
          <fieldset class="form-group position-relative has-icon-left mb-1">
            <select name="role" class="form-control">
              @foreach ( $roles as $role )
                <option value="{{ $role->id }}" {{ isset($user) && $user->role_id == $role->id ? "selected" : ""}}>{{ $role->name }}</option>
              @endforeach
            </select>
          </fieldset>
        </div>

        <div class="col-md-6" >

          <button type="submit" class="btn btn-primary btn-lg btn-block">
            Next </button>

        </div>

      </div>
          </form>
    </div>
  </div>
</div>
</div>
</div>
  </div>
</div>

@endsection
