@extends('admin.root')
@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">
        <span> Giảng viên </span>
      <a class="btn btn-success" href="{{url('admin/teachers/new')}}" >New</a>
    </h4>
    </div>

  </div>
  <div class="content-body">
<div class="card">

  <!-- Issuers -->
    <div class="card-block" >
        <div class="row">
          <div class="col-md-4 ">
              <div class="card-title">
                <strong>Họ và Tên</strong>
              </div>
          </div>

          <div class="col-md-2">
              <div class="card-title">
                Tài khoản
              </div>
          </div>          
        </div>
    @if($teachers && sizeof($teachers) > 0)
      <div class="infinite-scroll">
        @foreach( $teachers as $teacher)

            <div class="row bottom">

              <div class="col-md-4">
                  <div class="form-group">
                  <div class="tag tag-primary">

                        <strong> {{ $teacher->full_name }}</strong>
                  </div>

                  <div class="tag tag-warning">

                        <strong> {{$teacher->status ? "Active" : "DeActive"}}</strong>
                  </div>

                  </div>
                  <div class="form-group">

                  </div>

              </div>

              <div class="col-md-2">
                  <div class="form-group">
                    {{$teacher->username}}
                  </div>
              </div>
              
              <div class="col-md-1">

                      <a href="{{ url('/admin/teachers/update/' . $teacher->id) }}" class="btn btn-success">
                        Update
                      </a>
              </div>
              <div class="col-md-1">

                  <form class="form" action="{{ url('admin/teacher/deactive') }}" method="post">
                      <input type="hidden" name="_token" value="{{ csrf_token()}}" >
                      <input type="hidden" name="id" value="{{ $teacher->id }}" >
                      <button type="submit" class="btn">
                        {{ $teacher->status ? 'DeActive' : 'Active' }}
                      </button>
                  </form>
              </div>
              <div class="col-md-1">
                <form class="form" action="{{ url('admin/teacher/delete') }}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" >
                      <input type="hidden" name="id" value="{{ $teacher->id }}" >
                    <button type="submit" class="btn btn-danger mr-1">Delete</button>
                </form>
              </div>
            </div>
        @endforeach
        {{ $teachers->links() }}
      </div>
    @endif
  </div>
  <!--End Domains -->

</div>
  </div>
</div>

@endsection

@section('footer_scripts')

<script type="text/javascript">
    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,            
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });
</script>

@stop
