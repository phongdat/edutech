@extends('admin.root')
@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">Giảng viên</h4>
    </div>
  </div>
  <div class="content-body">
    <div class="card">
      <div class="col-md-6 offset-md-2 col-xs-10 offset-xs-1 box-shadow-1 p-0">
        <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
          <div class="card-body collapse in">
            <div class="card-block">
              <form class="form-horizontal form-simple"
                method="post"
                action="{{url('/admin/teachers/save')}}">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="id" value="{{ isset($member) ? $member->id : 0 }}" />
                <fieldset class="form-group position-relative has-icon-left mb-1">
                  <input type="text" class="form-control form-control-lg input-lg"
                  name="name"
                  placeholder="Họ và tên" value="{{ isset($member) ? $member->full_name : '' }}">
                  <div class="form-control-position">
                    <i class="ft-briefcase"></i>
                  </div>
                </fieldset>
                
                <fieldset class="form-group position-relative has-icon-left mb-1">
                  <textarea class="form-control form-control-lg input-lg"
                  name="address"
                  placeholder="Địa chỉ">{{ isset($member) ? $member->address : '' }}</textarea>
                  <div class="form-control-position">
                    <i class="ft-briefcase"></i>
                  </div>
                </fieldset>
                <fieldset class="form-group position-relative has-icon-left mb-1">
                  <input type="text" class="form-control form-control-lg input-lg"
                  name="username"
                  placeholder="Tên đăng nhập" value="{{ isset($member) ? $member->username : '' }}">
                  <div class="form-control-position">
                    <i class="ft-briefcase"></i>
                  </div>
                </fieldset>
                <fieldset class="form-group position-relative has-icon-left mb-1">
                  <input type="password" class="form-control form-control-lg input-lg"
                  name="password"
                  placeholder="Mật khẩu" value="">
                  <div class="form-control-position">
                    <i class="ft-briefcase"></i>
                  </div>
                </fieldset>
                <div class="row">
                  <div class="col-md-6">
                    <select name="parent" id="parent" class="form-control">
                      <option value="0">Không có người giới thiệu</option>
                    </select>
                  </div>
                  <div class="col-md-6" >
                    <div class="col-md-6 pl-0">
                      <button type="submit" class="btn btn-primary w-100">Save</button>
                    </div>
                    <div class="col-md-6 pr-0">
                      <a href="{{url('admin/teachers')}}" class="btn btn-danger w-100">Close</a>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection