@extends('admin.root')
@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">
        <span> Danh sách khoá học </span>
      <a class="btn btn-success" href="{{url('admin/course/new')}}" >Tạo mới </a>
    </h4>
    </div>

  </div>
  <div class="content-body">
<div class="card">

  <!-- Header -->
  <div id="search" class="card-block" >
      <form class="form" action="{{url('admin/course/search')}}" method="post">
        <div class="form-actions top clearfix">
          <input type="hidden" name="_token" value="{{csrf_token()}}">

          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <input name="name" type="text"
                class="form-control" placeholder="Tên khoá học" value="{{ isset($name) ? $name : "" }}">
              </div>
            </div>

            <div class="col-md-1">
              <button type="submit" class="btn">Search</button>
            </div>

            <div class="col-md-7 float-right">
              <a href="{{ url('admin/course/export') }}" class="btn btn-info float-right"> Export</a>
            </div>
          </div>          
        </div>
      </form>
      <form class="form" action="{{url('admin/course/filter')}}" method="post">
        <div class="form-actions top clearfix">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <div class="row">
            <div class="col-md-3">
              <h4 class="content-header-title"> Khoá học </h4>
              <select name="category" id="category" class="form-control"> 
                <option value="0">Danh mục khoá học</option>
                @foreach ($categories as $category)
                  <option value="{{ $category->id }}" {{ isset($sl_category) ? ($sl_category == $category->id ? "selected" : "")  : "" }}>{{ $category->title }} -- [{{ $category->category->name }}]</option>
                @endforeach               
              </select>
            </div>
            <div class="col-md-3">
              <h4 class="content-header-title"> Đối tượng </h4>
              <select name="object" id="object" class="form-control"> 
                <option value="0">Danh mục đối tượng</option>
                @foreach ($objects as $object)
                  <option value="{{ $object->id }}" {{ isset($sl_object) ? ($sl_object == $object->id ? "selected" : "")  : "" }}>{{ $object->name }}</option>
                @endforeach
              </select>   
            </div>
            <div class="col-md-3">
              <h4 class="content-header-title"> Lớp học </h4>
              <select name="classes" id="classes" class="form-control"> 
                <option value="0">Danh mục lớp học</option>
                @foreach ($classes as $class)
                  <option value="{{ $class->id }}" {{ isset($sl_class) ? ($sl_class == $class->id ? "selected" : "")  : "" }}>{{ $class->name }}</option>
                @endforeach
              </select>  
            </div>
            <div class="col-md-3">           
            <h4 class="content-header-title"> &nbsp; </h4> 
              <button class="btn btn-info" type="submit">Filter</button>              
            </div>
          </div>
        </div>
      </form>
      <div class="row">        
        <div class="col-md-4">
          <h4 class="content-header-title"> Hiển thị: {{ sizeof( $courses ) }} / {{ $courses->total() }} </h4>           
        </div>                
      </div>
  </div>
<!--  End Header -->



  <!-- Issuers -->
    <div class="card-block" >
        <div class="row">
          <div class="col-md-1">
            <div class="card-title">
              #
            </div>
          </div>
          <div class="col-md-2 ">
              <div class="card-title">
                <strong>Banner</strong>
              </div>
          </div>

          <div class="col-md-2">
              <div class="card-title">
                <strong>Video</strong>
              </div>
          </div>
          
          <div class="col-md-2">
              <div class="card-title">
                Tên khoá học
              </div>
          </div>
          
          <div class="col-md-2">
              <div class="card-title">
                Giảng viên
              </div>
          </div>          
        </div>
    @if($courses && sizeof($courses) > 0)
      <div class="infinite-scroll">              
        @foreach( $courses as $course)
          <div class="row bottom">
          <div class="col-md-1">
            {{ $index += 1 }}
          </div>
          <div class="col-md-2">
            <img src="{{ $course->banner }}" style="max-width: 140px;" alt="">
          </div>

              <div class="col-md-2">
                  <div class="form-group">
                    @if ( $course->video) 
                      <video style="margin-top:15px;max-height:100px;" controls>
                        <source src="{{ isset($course->video) ? url($course->video) : "" }}" type="video/mp4">
                      </video>
                      @else
                        Không có video nào
                      @endif
                  </div>
              </div>
              <div class="col-md-2">
                  <div class="form-group">
                    {{$course->title}}
                  </div>
              </div>
              
              <div class="col-md-2">
                  <div class="form-group">
                    {{ $course->teacher_name }}
                  </div>
              </div>
              
              <div class="col-md-1">

                      <a href="{{ url('/admin/courses/update/' . $course->id) }}" class="btn btn-success">
                        Update
                      </a>
              </div>
            
              <div class="col-md-1">
                <form class="form" action="{{ url('admin/courses/delete') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                    <input type="hidden" name="id" value="{{ $course->id }}" >
                    <button type="submit" class="btn btn-danger mr-1">Delete</button>
                </form>
              </div>
            </div>
            <hr>
        @endforeach
        @if(isset($name))
          {{ $courses->appends(['value' => $name])->links() }}     
        @elseif(isset($filter))
          {{ $courses->appends(['cate' => $sl_category, 'obj' => $sl_object, 'cla' => $sl_class])->links() }}     
        @else
          {{ $courses->links() }}
        @endif
      </div>
    @endif
  </div>
  <!--End Domains -->

</div>
  </div>
</div>

@endsection

@section('footer_scripts')

<script type="text/javascript">
    // $('ul.pagination').hide();
    // $(function() {
    //     $('.infinite-scroll').jscroll({
    //         autoTrigger: true,            
    //         padding: 0,
    //         nextSelector: '.pagination li.active + li a',
    //         contentSelector: 'div.infinite-scroll',
    //         callback: function() {
    //             $('ul.pagination').remove();
    //         }
    //     });
    // });

    $(document).ready(function(){
      $('#category').select2({
        placeholder: "Danh mục khoá học",
        tags: true
      });        
      $('#object').select2({
        placeholder: "Danh mục đối tượng",
        tags: true
      });        
      $('#classes').select2({
        placeholder: "Danh mục lớp học",
        tags: true
      });        

    });
</script>

@stop