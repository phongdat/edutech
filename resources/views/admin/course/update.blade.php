@extends('admin.root')
@section('header_styles')
<link href="https://transloadit.edgly.net/releases/uppy/v1.4.0/uppy.min.css" rel="stylesheet">
@stop
@section('content')
<form action="{{ url('admin/course/save') }}" method="post" class="form-horizontal form-simple" enctype="multipart/form-data">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="id" value="{{ isset($course) ? $course->id : 0 }}">
	<div class="lesson content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h4 class="content-header-title"><a href="#"> Khoá học </a></h4>
			</div>
		</div>
		<div class="content-body">
			<div class="lesson-left col-md-8">
				<div class="card border-grey border-lighten-3 px-2 py-2 m-0 box-shadow-1">
					<input type="text" class="lesson-title" style="width: 100%;" rows="1" placeholder="Thêm tiêu đề" name="title" value="{{ isset($course) ? $course->title : "" }}" />
					<textarea class="lesson-content" rows="1" placeholder="Thêm nội dung" name="content">{{ isset($course) ? $course->content : "" }}</textarea>
				</div>
			</div>
			<div class="lesson-right col-md-4">
				<div class="card border-grey border-lighten-3 px-2 py-2 box-shadow-1">
					<h4 class="content-header-title">Banner</h4>
					 <div class="input-group">

                    <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                        </a>
                    </span>
                    <input id="thumbnail" class="form-control" type="text" name="filepath" >
                    </div>
                    <img id="holder" style="margin-top:15px;max-height:100px;">
					<img id="preview-image" src="{{ isset($course) ? url($course->banner) : "" }}" style="margin-top:15px;max-height:100px;" class="" />                            
                </div>
                
                <div class="card border-grey border-lighten-3 px-2 py-2 box-shadow-1">
					<h4 class="content-header-title">Video</h4>
					<input type="hidden" id="lesson_video" name="video">
					<input type="hidden" id="lesson_video_extension" name="video_extension">
					<div id="drag-drop-area"></div>
					@if(isset($course->video))
					<video  style="margin-top:15px;max-height:100px;" controls>
						<source src="{{ isset($course->video) ? url($course->video) : "" }}" type="video/mp4">
					</video>
					@endif
				</div>

				<div class="card border-grey border-lighten-3 px-2 py-2 box-shadow-1 mt-1">
					<h4 class="content-header-title"> Lớp học </h4>
					<select name="category" id="category" class="form-control">
						@foreach ($categories as $category)
                            <option value="{{ $category->id }}" {{ isset($course) ? ($course->course_category_id == $category->id ? "selected" : "")  : "" }}>{{ $category->name }}</option>
                        @endforeach
					</select>
				</div>

				<div class="card border-grey border-lighten-3 px-2 py-2 box-shadow-1 mt-1">
					<h4 class="content-header-title"> Giảng viên </h4>
					<select name="teacher" id="teacher" class="form-control">
						@foreach ($teachers as $teacher)
                            <option value="{{ $teacher->id }}" {{ isset($course) ? ($course->user_id == $teacher->id ? "selected" : "")  : "" }}>{{ $teacher->full_name }}</option>
                        @endforeach
					</select>
				</div>
				<row>
					<div class="col-md-6 pl-0">
						<a class="btn btn-warning" href="{{url('admin/courses')}}">Đóng</a>
					</div>
					<div class="col-md-6 pr-0">
						<button type="submit" class="btn btn-success" >Lưu</button>
					</div>
				</row>
			</div>
		</div>
	</div>
</form>

@endsection

@section('footer_scripts')
 <script src="{{ url('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script src="https://transloadit.edgly.net/releases/uppy/v1.4.0/uppy.min.js"></script>
<script>
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	var uppy = Uppy.Core()
        .use(Uppy.Dashboard, {
			inline: true,
			target: '#drag-drop-area',
			height: 170,
        })
        .use(Uppy.Tus, {
			endpoint: 'http://edu.quanly.io:3111/files/',
			// endpoint: 'http://localhost:3111/files/',
			chunkSize: 500000,
		})

		uppy.on('complete', (result) => {
		$("#lesson_video_extension").val(result.successful[0].type);
		$("#lesson_video").val(result.successful[0].uploadURL);
		})
</script>
<script>
	
var textarea = document.querySelector('textarea');
textarea.addEventListener('keydown', autosize);

function autosize(){
	var el = this;
	setTimeout(function(){
	el.style.cssText = 'height:auto';
	// for box-sizing other than "content-box" use:
	// el.style.cssText = '-moz-box-sizing:content-box';
	el.style.cssText = 'height:' + el.scrollHeight + 'px';
	},0);
}
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script>
	tinymce.init({
		selector: 'textarea',
		plugins: 'link',
		plugins: "image",
		menubar: true,
		height: 500,
		resize: true
    });

    $(document).ready(function(){
		$('#category').select2({
		placeholder: "Danh mục khoá học",
		tags: true
		});
		$('#lfm').filemanager('file');
    });
 </script>
 @stop
