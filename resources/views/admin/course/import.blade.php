@extends('admin.root')
@section('header_styles')

@stop

@section('content')

<form action="{{ url('admin/course/import') }}" 
    method="post" 
    class="form-horizontal form-simple" 
    enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="lesson content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h4 class="content-header-title"><a href="#"> Import khoá học </a></h4>
			</div>
		</div>
		<div class="content-body">
            <input type="file" name="file" class="form-control">
            <br>
            <button type="submit" class="btn btn-success">Import Data</button>            
		</div>
	</div>

</form>

@endsection

@section('footer_scripts')

@stop
