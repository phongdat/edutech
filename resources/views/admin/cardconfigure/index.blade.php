@extends('admin.root')
@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">
        <span> Quản lý cấu hình </span>
    </h4>
    </div>

  </div>
  <div class="content-body">
<div class="card">

<div class="card-block box-shadow-1 mb-2 bg-white">
              <form class="form-horizontal form-simple"
                method="post"
                action="{{url('/admin/card-config/save')}}">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="config_id" value="{{ isset($config) ? $config->id : 0 }}" />
                <fieldset class="form-group position-relative has-icon-left mb-1">
                  <input type="text" class="form-control form-control-lg input-lg"
                  name="name"
                  placeholder="Tên cấu hình" value="{{ isset($config) ? $config->name : '' }}">
                  <div class="form-control-position">
                    <i class="ft-briefcase"></i>
                  </div>
                </fieldset>
                
                <fieldset class="form-group position-relative has-icon-left mb-1">
                <input type="text" class="form-control form-control-lg input-lg"
                  name="value"
                  placeholder="Giá trị" value="{{ isset($config) ? $config->value : '' }}">
                  <div class="form-control-position">
                    <i class="ft-briefcase"></i>
                  </div>
                </fieldset>

                @if(isset($config))
                <div class="row">
                  <div class="col-md-2 offset-md-8">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                    Update </button>
                  </div>
                  <div class="col-md-2" >
                    <a href="{{url('admin/card-configure')}}" class="btn btn-primary btn-lg btn-block">Trở về</a>
                  </div>
                </div>
                @else
                <div class="row">
                  <div class="col-md-2 offset-md-10" >
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                    Save </button>
                  </div>
                </div>
                @endif
              </form>
            </div>
  <!-- Issuers -->
    <div class="card-block box-shadow-1 mb-2 bg-white" >
        <div class="row">
          <div class="col-md-4">
              <div class="card-title">
                <strong>Tên cấu hình</strong>
              </div>
          </div>

          <div class="col-md-4">
              <div class="card-title">
                <strong>Giá trị</strong>
              </div>
          </div>
          
          <div class="col-md-2">
              <div class="card-title">
                Ngày tạo
              </div>
          </div>
          
        </div>
    @foreach( $configs as $config)

        <div class="row bottom">

          <div class="col-md-4">
            <strong>{{ $config->name }}</strong>
          </div>

          <div class="col-md-4">
             {{ $config->value }}
          </div>
          <div class="col-md-2">
              <div class="form-group">
                {{$config->created_at }}
              </div>
          </div>
          <div class="col-md-1 pl-0">
                  <a href="{{url('admin/card-configure/update/'.$config->id)}}" class="btn btn-success">
                    Update
                  </a>
                </div>
                <div class="col-md-1 pl-0">
                  <a href="{{url('admin/card-configure/delete/'.$config->id)}}" class="btn btn-danger"> Xóa </a>
                </div>
          </div>
          
    @endforeach
    {{ $configs->links() }}

  </div>
  <!--End Domains -->

</div>
  </div>
</div>

@endsection
