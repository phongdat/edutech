@extends('admin.root')
@section('header_styles')
<link href="https://transloadit.edgly.net/releases/uppy/v1.4.0/uppy.min.css" rel="stylesheet">
@stop
@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">
        <span> Danh sách thẻ chưa sử dụng </span>
      <a class="btn btn-success" href="{{url('admin/product/new')}}" >Tạo mới</a>      
    </h4>
    </div>
    <!-- <div class="content-header-right col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">       
        <a class="btn btn-success" style="float: right;" href="{{url('admin/card/generate-group-series')}}" >Tạo số bộ</a>      
    </h4>
    </div> -->
    

  </div>
  <div class="content-body">
<div class="card">

  <!-- Header -->
  <div id="search" class="card-block" >
    <form class="form col-md-6" action="{{url('admin/cards/search')}}" method="post">
      <div class="form-actions top clearfix pb-0 mb-0" style="border: none;">
        <input type="hidden" name="_token" value="{{csrf_token()}}">

        <div class="row">
          <div class="col-md-10 pl-0">
            <div class="form-group">
              <input name="serial" type="text"
              class="form-control" placeholder="Serial thẻ">
            </div>
          </div>

          <div class="col-md-2 p-0">
            <button type="submit" class="btn w-100">Search</button>
          </div>
        </div>
      </div>
    </form>
    @if(count($errors)>0)
      <div class="alert alert-danger">
        @foreach($errors->all() as $err)
          {{$err}}
        @endforeach
      </div>
    @endif
    <form class="col-md-6" method="post" enctype="multipart/form-data" action="{{ url('admin/cards/import') }}">
      <div class="form-actions top clearfix pb-0 mb-0 float-md-right" style="border: none;">
        {{ csrf_field() }}
        <div class="form-group">
           <input type="hidden" id="card_import" name="card_import">
          <input type="hidden" id="card_import_extension" name="card_import_extension">
          <div id="drag-drop-area"></div>
            <input type="submit" name="upload" class="btn btn-primary" value="Import">
        </div>
      </div>
    </form>    
    <div class="clearfix"></div>
    <hr>
  </div>
<!--  End Header -->



  <!-- Issuers -->
    <div class="card-block" >
        <div class="row">
          <div class="col-md-2 ">
              <div class="card-title">
                <strong>Serial</strong>
              </div>
          </div>

          <div class="col-md-2">
              <div class="card-title">
                <strong>Loại</strong>
              </div>
          </div>
          
          <div class="col-md-2">
              <div class="card-title">
                Ngày tạo
              </div>
          </div>

          <div class="col-md-2">
              <div class="card-title">
                Tên bộ
              </div>
          </div>

          <div class="col-md-2">
              <div class="card-title">
                Ngày kích hoạt
              </div>
          </div>

          <div class="col-md-2">
              <div class="card-title">
                Ngày hết hạn
              </div>
          </div>
          
        </div>
    @foreach( $cards as $card)

        <div class="row bottom">

          <div class="col-md-2">
            <strong>{{ $card->series }}</strong>
          </div>

          <div class="col-md-2">
             {{ $card->type }}
          </div>
          <div class="col-md-2">
              <div class="form-group">
                {{$card->created_at }}
              </div>
          </div>
          <div class="col-md-2">
              <div class="form-group">
                {{$card->group_series }}
              </div>
          </div>
          <div class="col-md-2">
              <div class="form-group">
                {{$card->actived_date }}
              </div>
          </div>
          <div class="col-md-2">
              <div class="form-group">
                {{$card->expired_date }}
              </div>
          </div>          
        </div>
          
    @endforeach
    {{ $cards->links() }}

  </div>
  <!--End Domains -->

</div>
  </div>
</div>

@endsection
@section('footer_scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  var textarea = document.querySelector('textarea');
	textarea.addEventListener('keydown', autosize);
	
	function autosize(){
		var el = this;
		setTimeout(function(){
			el.style.cssText = 'height:auto';
			// for box-sizing other than "content-box" use:
			// el.style.cssText = '-moz-box-sizing:content-box';
			el.style.cssText = 'height:' + el.scrollHeight + 'px';
		},0);
	}
</script>
<script>
  tinymce.init({
		selector: 'textarea',
		plugins: 'link',
		plugins: "image",
		menubar: true,
		height: 500,
		resize: true
	});
</script>
<script src="https://transloadit.edgly.net/releases/uppy/v1.4.0/uppy.min.js"></script>
<script>
  $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	var uppy = Uppy.Core()
        .use(Uppy.Dashboard, {
			inline: true,
			target: '#drag-drop-area',
			height: 170,
        })
        .use(Uppy.Tus, {
			endpoint: 'http://edu.quanly.io:3111/files/',
			// endpoint: 'http://localhost:3111/files/',
			chunkSize: 500000,
		})

      uppy.on('complete', (result) => {
        $("#card_import_extension").val(result.successful[0].type);
        $("#card_import").val(result.successful[0].uploadURL);
      })
</script>
<script>
  $(document).ready(function(){
		$('.uppy-Dashboard-inner').css("min-height", '100px');
	});
</script>
@stop