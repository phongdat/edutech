@extends('admin.root')
@section('header_styles')
<style>
        .form-group input[type="checkbox"] {
            display: none;
        }

        .form-group input[type="checkbox"] + .btn-group > label span {
            width: 20px;
            height: 20px;
        }

        .form-group input[type="checkbox"] + .btn-group > label span:first-child {
            display: none;
        }
        .form-group input[type="checkbox"] + .btn-group > label span:last-child {
            display: inline-block;   
        }

        .form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
            display: inline-block;
        }
        .form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
            display: none;   
        }
    </style>
@stop

@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
      <h4 class="content-header-title">Thẻ </h4>
    </div>

  </div>
  <div class="content-body">
<div class="card">
<div class="col-md-6 offset-md-2 col-xs-10 offset-xs-1 box-shadow-1 p-0">
<div class="card border-grey border-lighten-3 px-2 py-2 m-0">

  <div class="card-body collapse in">
    <div class="card-block">
      <form class="form-horizontal form-simple"
       method="post"
       action="{{url('/admin/cards/save')}}">

       <input type="hidden" name="_token" value="{{csrf_token()}}">
       <input type="hidden" name="id" value="{{ isset($page) ? $page->id : 0 }}" />

        <fieldset class="form-group position-relative has-icon-left mb-1">

          <input type="number" min='1' class="form-control form-control-lg input-lg"
           name="quantity" max="200"
          placeholder="Số lượng thẻ cần tạo" value="1">

          <div class="form-control-position">
              <i class="ft-briefcase"></i>
          </div>
        </fieldset>

        <fieldset class="form-group position-relative has-icon-left mb-1">

          <input type="number" class="form-control form-control-lg input-lg quantity"
           name="str_serial" min="0" maxlength="2"
          placeholder="Số đầu serial thẻ (Hiển thị đầu tiên) ">

          <div class="form-control-position">
              <i class="ft-briefcase"></i>
          </div>
        </fieldset>

        <div class="row">

        
        <div class="col-md-6">
          
        </div>
        <div class="col-md-6" >

          <button type="submit" class="btn btn-primary btn-lg btn-block">
            Next </button>

        </div>

      </div>
          </form>
    </div>
  </div>
</div>
</div>
</div>
  </div>
</div>

@endsection
@section('footer_scripts')
    <script>
    var inputQuantity = [];
    $(function() {
      $(".quantity").each(function(i) {
        inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $(".quantity").on("keyup", function (e) {
        var $field = $(this),
            val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the index
//        window.console && console.log($field.is(":invalid"));
          //  $field.is(":invalid") is for Safari, it must be the last to not error in IE8
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
        } 
        if (val.length > Number($field.attr("maxlength"))) {
          val=val.slice(0, $field.attr("maxlength"));
          $field.val(val);
        }
        inputQuantity[$thisIndex]=val;
      });      
    });


</script>
@stop
