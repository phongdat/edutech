@extends('client/layouts/master')

@section('title', (isset($page) ? $page->title : "") )

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/introduce.css">
@endsection

@section('content')

<div class="wrap-slider">
    <div class="container page-container">
        <div class="page-content">
            <div class="page-title">                
                <div class="breadcrumbs">
                    <ul>                        
                        <li>{{ isset($page) ? $page->title : "" }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

{!! isset($page) ? $page->content : "" !!}

@endsection

@section('footer_styles')

@endsection