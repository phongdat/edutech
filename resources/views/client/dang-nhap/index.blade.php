@extends('client/layouts/master')

@section('title', $title)

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/login.css">
@endsection

@section('content')

<div class="wrap-slider">
    <div class="container page-container">
        <div class="page-content">
            <div class="page-title">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="javascript:void(0)">TÀI KHOẢN</a></li>                    
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="flat-row pd-top-93">
    <div class="container pd-left-76">        
        <div class="row">
            <div class="col-md-6 pd-left-0">
                <h3 class="form-title">Đăng nhập</h3>
                 <form action="login" method="POST">
                    @csrf  
                    <div class="form-group row">
                        <label for="email_address" class="col-md-3 col-form-label text-md-right">Email hoặc Số điện thoại</label>
                        <div class="col-md-7">
                            <input type="text" id="email_address" class="form-control" name="username" required autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-md-3 col-form-label text-md-right">Mật khẩu</label>
                        <div class="col-md-7">
                            <input type="password" id="password" class="form-control" name="password" required autofocus>
                        </div>
                    </div>
                    <div class="row control-group">
                        <label class="col-md-3 col-form-label text-md-right"></label>			      
                        <div class="col-md-7">
                            <button class="btn btn-primary btn-login-page">ĐĂNG NHẬP</button>
                        </div>
                    </div>
                    <br>
                    <div class="row control-group">
                        <label class="col-md-3 col-form-label text-md-right"></label>			      
                        <div class="col-md-7">
                            <a href="/quen-mat-khau"> Quên mật khẩu? </a>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="col-md-11 pd-right-0 pd-left-0">
                            <hr>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="fullname" class="col-md-3 col-form-label text-md-right">Đăng nhập bằng</label>
                        <div class="col-md-7">
                            <button type="button" class="btn btn-fb social-face-btn" style="padding: 0;"><i class="fab fa-facebook-f"></i></button>
                            <button type="button" class="btn btn-fb social-googleplus-btn" style="padding: 0;"><i class="fab fa-google-plus-g"></i></button>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="col-md-11 pd-right-0 pd-left-0">
                            <hr>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 wrap-grid">    
                <h3 class="form-title">Đăng ký tài khoản mới</h3>             
                <form action="register" method="POST">
                    @csrf  
                    <div class="form-group row">
                        <label for="fullname" class="col-md-4 col-form-label text-md-right">Họ và tên</label>
                        <div class="col-md-7">
                            <input type="text" id="name" class="form-control" name="name" required autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tel" class="col-md-4 col-form-label text-md-right">Số điện thoại</label>
                        <div class="col-md-7">
                            <input type="text" id="tel" class="form-control" name="username" required autofocus>
                        </div>
                    </div>                   

                    <div class="form-group row">
                        <label for="address" class="col-md-4 col-form-label text-md-right">Địa chỉ</label>
                        <div class="col-md-7">
                            <textarea class="form-control" name="address" rows="4" cols="50"></textarea>                            
                        </div>
                    </div>
                    <div class="row control-group mr-top-20">
                        <label class="col-md-4"></label>			      
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-login-page">ĐĂNG KÝ</button>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="col-md-11 pd-right-0 pd-left-0">
                            <hr>
                        </div>
                    </div>
                </form>   
            </div><!-- col-md-6 -->    
        </div><!-- row -->
    </div><!-- container -->
</section>
<section class="flat-row">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="summary-course">
                    <h2 class="summary-course__h2">Khoá học khác</h2>
                </div>
            </div>
            <div class="col-md-9">
                @for($j = 0; $j < count($newest_data); $j++)
                    @php ($curCourse = $newest_data[$j])
                    @if($j == 0)
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif

                    @if($j != 0 && $j % 3 == 0)
                            </div>
                        </div>
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif
                    
                    @if($j > 2)
                        <div class="item course mr-bottom-0">
                    @else
                        <div class="item course">
                    @endif                    
                        <article class="entry  article-course--height">
                            <div class="featured-post">
                                <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                    @if (file_exists(public_path($curCourse->detail->banner)))
                                        <img src="{!! $curCourse->detail->banner !!}" alt="image">
                                    @else
                                        <img src="/client_assets/image/noimage.jpg" alt="image">
                                    @endif                                        
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="entry-post">
                                        <h3 class="entry-title">
                                            <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                                {{ mb_strlen($curCourse->name) > 16 ? mb_substr($curCourse->name,0,16)."..." : $curCourse->name }}
                                            </a>
                                        </h3>
                                        <div class="entry-author">
                                            <span><a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">{{ $curCourse->detail->teacher_name }}</a></span>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="row">
                                <form action="/like-course" method="POST">
                                    <input id="form_id_{{ $curCourse->id }}" type="hidden" name="courseId" value="{{ $curCourse->id }}">                                        
                                    {{ csrf_field() }}                                        
                                    <fieldset class="rating">                                            
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star5" name="rating" value="5" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating == 5 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star5" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star4" name="rating" value="4" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 4 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star4" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star3" name="rating" value="3" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 3 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star3" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star2" name="rating" value="2" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 2 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star2" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star1" name="rating" value="1" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 1 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star1" title="Rocks!"></label>
                                    </fieldset>
                                </form>
                            </div>
                        </article>
                    </div>
                @endfor                    
            </div>
        </div>
    </div>
</section>
<section class="flat-row pd-90 flat-none"></section>
@endsection

@section('footer_styles')

@endsection