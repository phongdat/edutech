@extends('client/layouts/master')

@section('title', $title)

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/account.css">
@endsection

@section('content')
<div class="wrap-slider">
    <div class="container page-container">
        <div class="page-content">
            <div class="page-title">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="javascript:void(0)">QUÊN MẬT KHẨU</a></li>                    
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="flat-row pd-bottom-0">
    <div class="container">
        <div class="row mr-bottom-48">  
            <div class="col-md-3"></div>          
            <div class="col-md-6 pd-top-20 content div-center">                           
                <div class="group-content-container">
                    <form action="/submit-forgot-password" method="POST">
                    @csrf
                        <div id="div-email" class="form-group row">
                            <label for="username" class="col-md-3 col-form-label text-md-right"></label>
                            <div class="col-md-7">
                                <input type="text" id="username" placeholder="Nhập email hoặc số điện thoại" class="form-control" name="username" required autofocus>
                            </div>
                        </div>
                        <div class="row control-group">
                            <label class="col-md-3 col-form-label text-md-right"></label>			      
                            <div class="col-md-7">
                                <button type="submit" class="btn btn-primary"> Tiếp tục </button>
                            </div>
                        </div>
                    </form>
                </div>                                
            </div>
        </div>
    </div>
</section>
<section class="flat-row pd-90 flat-none"></section>
@endsection

@section('footer_styles')

@endsection