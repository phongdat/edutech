@extends('client/layouts/master')

@section('title', $title)

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/account.css">
@endsection

@section('content')
<div class="wrap-slider">
    <div class="container page-container">
        <div class="page-content">
            <div class="page-title">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="javascript:void(0)">TÀI KHOẢN</a></li>                    
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="flat-row pd-bottom-0">
    <div class="container">
        <div class="row mr-bottom-48">
            <div class="col-md-3">
                <ul class="left-list-container">
                    <li class="left-list-item"><a href="/account"><b>Trang chính</b></a></li>
                    <li class="left-list-item"><a href="/danh-gia">Đánh giá</a></li>
                    <li class="left-list-item"> <a href="/lich-su-giao-dich">Lịch sử giao dịch</a></li>
                    <li class="left-list-item"> <a href="/chinh-sua-ho-so">Chỉnh sửa hồ sơ</a></li>
                    <li class="left-list-item"> <a href="/doi-mat-khau">Đổi mật khẩu</a></li>
                    <li class="left-list-item"> <a href="/logout">Đăng xuất</a></li>
                </ul>
            </div>
            <div class="col-md-8 pd-top-20">                           
                <div class="group-content-container">
                    <h2>Lịch sử giao dịch</h2>
                    @if($member->member_type_id == 1) 

                        <div class="group-content">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>Stt</td>                                
                                        <td>Ngày sử dụng</td>                                    
                                        <td>Điểm sử dụng</td>                                                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($data['transactions'] && sizeof($data['transactions']) > 0)
                                            
                                        @for($i = 0; $i < count($data['transactions']); $i++)
                                            <tr>
                                                <td>{{ $i + 1 }}</td>                                        
                                                <td>{{ date('d-m-Y', strtotime($data['transactions'][$i]->created_at)) }}</td>
                                                <td>{{ $data['transactions'][$i]->used_point }}</td>                                        
                                            </tr>
                                        @endfor 

                                    @else

                                        <tr>                                        
                                            <td colspan="5" style="text-align: center;">Bạn chưa có giao dịch nào</td>
                                        </tr>
                                        
                                    @endif
                                </tbody>
                            </table>
                        </div>

                    @else

                        <div class="group-content">
                            <h4><b>Bảng giao dịch đến</b></h4>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Stt</th>                                
                                        <th>Ngày nhận</th>
                                        <th>Người gửi</th>                                    
                                        <th>Số lượng thẻ</th>                                                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($data['receiving_transactions'] && sizeof($data['receiving_transactions']) > 0)
                                            
                                        @for($i = 0; $i < count($data['receiving_transactions']); $i++)
                                            <tr>
                                                <td>{{ $i + 1 }}</td>                                        
                                                <td>{{ date('d-m-Y', strtotime($data['receiving_transactions'][$i]->created_at)) }}</td>
                                                <td>{{ $data['receiving_transactions'][$i]->sender }}</td>   
                                                <td>{{ $data['receiving_transactions'][$i]->number_of_cards }}</td>                                     
                                            </tr>
                                        @endfor 

                                    @else

                                        <tr>                                        
                                            <td colspan="5" style="text-align: center;">Bạn chưa có giao dịch nào</td>
                                        </tr>
                                        
                                    @endif
                                </tbody>
                            </table>

                            <h4><b>Bảng giao dịch đi</b></h4>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Stt</th>                                
                                        <th>Ngày gửi</th>
                                        <th>Người nhận</th>                                    
                                        <th>Số lượng thẻ</th>                                                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($data['sending_transactions'] && sizeof($data['sending_transactions']) > 0)
                                            
                                        @for($i = 0; $i < count($data['sending_transactions']); $i++)
                                            <tr>
                                                <td>{{ $i + 1 }}</td>                                        
                                                <td>{{ date('d-m-Y', strtotime($data['sending_transactions'][$i]->created_at)) }}</td>
                                                <td>{{ $data['sending_transactions'][$i]->receiver }}</td>   
                                                <td>{{ $data['sending_transactions'][$i]->number_of_cards }}</td>                                     
                                            </tr>
                                        @endfor 

                                    @else

                                        <tr>                                        
                                            <td colspan="5" style="text-align: center;">Bạn chưa có giao dịch nào</td>
                                        </tr>
                                        
                                    @endif
                                </tbody>
                            </table>
                        </div>

                    @endif
                </div>                                
            </div>
        </div>
    </div>
</section>

<section class="flat-row pd-top-0">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="summary-course">
                    <h2 class="summary-course__h2">Khoá học khác</h2>
                </div>
            </div>
            <div class="col-md-9">
                @for($j = 0; $j < count($newest_data); $j++)
                    @php ($curCourse = $newest_data[$j])
                    @if($j == 0)
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif

                    @if($j != 0 && $j % 3 == 0)
                            </div>
                        </div>
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif
                    
                    @if($j > 2)
                        <div class="item course mr-bottom-0">
                    @else
                        <div class="item course">
                    @endif                    
                        <article class="entry  article-course--height">
                            <div class="featured-post">
                                <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                    @if (file_exists(public_path($curCourse->detail->banner)))
                                        <img src="{!! $curCourse->detail->banner !!}" alt="image">
                                    @else
                                        <img src="/client_assets/image/noimage.jpg" alt="image">
                                    @endif                                        
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="entry-post">
                                        <h3 class="entry-title">
                                            <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                                {{ mb_strlen($curCourse->name) > 16 ? mb_substr($curCourse->name,0,16)."..." : $curCourse->name }}
                                            </a>
                                        </h3>
                                        <div class="entry-author">
                                            <span><a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">{{ $curCourse->detail->teacher_name }}</a></span>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="row">
                                <form action="/like-course" method="POST">
                                    <input id="form_id_{{ $curCourse->id }}" type="hidden" name="courseId" value="{{ $curCourse->id }}">                                        
                                    {{ csrf_field() }}                                        
                                    <fieldset class="rating">                                            
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star5" name="rating" value="5" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating == 5 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star5" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star4" name="rating" value="4" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 4 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star4" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star3" name="rating" value="3" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 3 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star3" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star2" name="rating" value="2" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 2 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star2" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star1" name="rating" value="1" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 1 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star1" title="Rocks!"></label>
                                    </fieldset>
                                </form>
                            </div>
                        </article>
                    </div>
                @endfor                    
            </div>
        </div>
    </div>
</section>
<section class="flat-row pd-90 flat-none"></section>
@endsection

@section('footer_styles')

@endsection