@extends('client/layouts/master')

@section('title', $title)

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/level-detail.css">
@endsection

@section('content')

<div class="wrap-slider">
    <div class="container page-container">
        <div class="page-content">
            <div class="page-title">
                <div class="breadcrumbs">
                    <ul>      
                        @for($i = 0; $i < count($breadcrumbs); $i++)
                            @if($i == 0)

                                <li><a href="/hoc-online/{{ $parent }}">
                                    {{ $breadcrumbs[$i] }}
                                    <span class="special-char">&rang;</span></a>
                                </li>
                            
                            @else

                                <li>{{ $breadcrumbs[$i] }}</li>    

                            @endif
                            
                        @endfor                                                                        
                    </ul>
                </div>      
            </div>
        </div>
    </div>
</div>
<section class="flat-row flat-row-title">
    <div class="container">
        <div class="row">
            <div class="col-md-12 pd-left-62">
                <div class="title-section">
                    <div class="col-md-12 pd-left-0">
                        <h1 class="title">{{ $reqCourseCategory->name }} - {{ $data->title }}</h1>     
                    </div>                                       
                </div>
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->           
</section>
<section class="flat-row flat-row-detail pd-bottom-no">
    <div class="container pd-left-76">        
        <div class="row">
            <div class="col-md-9 col-sm-9 wrap-reponsive pd-left-0">
                <div class="wrap-course-details">
                    <div class="portfolio">
                        <article class="entry no-shadow">
                            <div class="entry-post">                                                                                                          
                                <video class="video-detail" controls="" autoplay="">
                                    <source src="{!! $data->video !!}" type="video/mp4">
                                    Your browser does not support the <video> element.</video>
                                </video>                                                                            
                             
                                <div class="lecture">
                                    <div class="col-md-7 float-left pd-left-0">
                                        <p>Giảng viên &nbsp;&nbsp; <span>{{ $data->teacher_name }}</span></p>
                                    </div>                                    
                                    <div class="col-md-5 float-right">
                                        <form action="/like-course" method="POST">
                                            <input id="form_id_{{ $reqCourseCategory->id }}" type="hidden" name="courseId" value="{{ $reqCourseCategory->id }}">                                        
                                            {{ csrf_field() }}                                        
                                            <fieldset class="rating">                                            
                                                <input type="submit" id="form_id_{{ $reqCourseCategory->id }}_star5" name="rating" value="5" /><label class="{{ isset( $reqCourseCategory->rating ) && $reqCourseCategory->rating == 5 ? 'rating-yellow' : '' }}" for="form_id_{{ $reqCourseCategory->id }}_star5" title="Rocks!"></label>
                                                <input type="submit" id="form_id_{{ $reqCourseCategory->id }}_star4" name="rating" value="4" /><label class="{{ isset( $reqCourseCategory->rating ) && $reqCourseCategory->rating >= 4 ? 'rating-yellow' : '' }}" for="form_id_{{ $reqCourseCategory->id }}_star4" title="Rocks!"></label>
                                                <input type="submit" id="form_id_{{ $reqCourseCategory->id }}_star3" name="rating" value="3" /><label class="{{ isset( $reqCourseCategory->rating ) && $reqCourseCategory->rating >= 3 ? 'rating-yellow' : '' }}" for="form_id_{{ $reqCourseCategory->id }}_star3" title="Rocks!"></label>
                                                <input type="submit" id="form_id_{{ $reqCourseCategory->id }}_star2" name="rating" value="2" /><label class="{{ isset( $reqCourseCategory->rating ) && $reqCourseCategory->rating >= 2 ? 'rating-yellow' : '' }}" for="form_id_{{ $reqCourseCategory->id }}_star2" title="Rocks!"></label>
                                                <input type="submit" id="form_id_{{ $reqCourseCategory->id }}_star1" name="rating" value="1" /><label class="{{ isset( $reqCourseCategory->rating ) && $reqCourseCategory->rating >= 1 ? 'rating-yellow' : '' }}" for="form_id_{{ $reqCourseCategory->id }}_star1" title="Rocks!"></label>
                                            </fieldset>
                                        </form>
                                    </div>    
                                </div>
                                <div class="course-content">
                                    <ul class="nav nav-tabs course-content-type">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#">ĐỀ CƯƠNG</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">BÀI TẬP</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">ĐỀ THI</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">ĐÁP ÁN</a>
                                        </li>
                                    </ul>                            
                                    <div class="row">
                                        @for($i = 0; $i < count($pieces); $i++)
                                            @php ($curCourse = $pieces[$i])
                                            <div class="col-sm-6">
                                                @for($j = 0; $j < count($curCourse); $j++)
                                                    <h4>{{ $curCourse[$j]['title'] }}</h4>
                                                    <p>                                                        
                                                        @for($x = 0; $x < count($curCourse[$j]['lessons']); $x++)
                                                            @php ($curLesson = $curCourse[$j]['lessons'][$x])
                                                            <a href="/hoc-online/{{ $parent }}/{{ $courseCategory }}/{{ $curLesson->slugify }}">
                                                                {{ $curLesson->title }}
                                                            </a> 
                                                            <br>
                                                        @endfor
                                                    </p>
                                                @endfor
                                            </div>
                                        @endfor                                        
                                    </div> 
                                </div>
                            </div>                            
                        </article>                        
                    </div>                    
                </div>                                                              
            </div><!-- col-md-9 -->
            <div class="col-md-3 col-sm-3 pd-left-0">
                <div class="sidebar">                    
                    <div class="widget">                        
                        @if (isset($memCWatcheds))
                            <h3 class="seen-course-title">Khoá học vừa xem</h3>                                                     
                            <div class="portfolio style2">
                                <div class="portfolio-wrap clearfix"> 
                                    <div class="item course item-one">
                                        @for($memCWIdx = 0; $memCWIdx < count($memCWatcheds); $memCWIdx++)
                                            <article class="entry article-course--height border-article">
                                                <div class="featured-post">
                                                    <a href="">
                                                        @if (file_exists(public_path( $memCWatcheds[$memCWIdx]->course->banner )))
                                                            <img src="{{ $memCWatcheds[$memCWIdx]->course->banner }}" alt="image">                                    
                                                        @else
                                                            <img src="/client_assets/image/noimage.jpg" alt="image">
                                                        @endif                                                          
                                                    </a>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 col-xs-12">
                                                        <div class="entry-post">
                                                            <h3 class="entry-title"><a href="javascript:void(0)"> {{ $memCWatcheds[$memCWIdx]->course_category->name }} </a></h3>
                                                            <div class="entry-author">
                                                                <span><a href="javascript:void(0)">{{ $memCWatcheds[$memCWIdx]->course->teacher_name }}</a></span>
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                </div>    
                                                <div class="row">                                                    
                                                    <form action="/like-course" method="POST">
                                                        <input id="form_id_{{ $memCWatcheds[$memCWIdx]->course_category->id }}" type="hidden" name="courseId" value="{{ $memCWatcheds[$memCWIdx]->course_category->id }}">                                        
                                                        {{ csrf_field() }}                                        
                                                        <fieldset class="rating float-left">                                            
                                                            <input type="submit"  id="form_id_{{ $memCWatcheds[$memCWIdx]->course_category->id }}_star5" name="rating" value="5" /><label class="{{ isset( $memCWatcheds[$memCWIdx]->course_category->rating ) && $memCWatcheds[$memCWIdx]->course_category->rating == 5 ? 'rating-yellow' : '' }}" for="form_id_{{ $memCWatcheds[$memCWIdx]->course_category->id }}_star5" title="Rocks!"></label>
                                                            <input type="submit"  id="form_id_{{ $memCWatcheds[$memCWIdx]->course_category->id }}_star4" name="rating" value="4" /><label class="{{ isset( $memCWatcheds[$memCWIdx]->course_category->rating ) && $memCWatcheds[$memCWIdx]->course_category->rating >= 4 ? 'rating-yellow' : '' }}" for="form_id_{{ $memCWatcheds[$memCWIdx]->course_category->id }}_star4" title="Rocks!"></label>
                                                            <input type="submit"  id="form_id_{{ $memCWatcheds[$memCWIdx]->course_category->id }}_star3" name="rating" value="3" /><label class="{{ isset( $memCWatcheds[$memCWIdx]->course_category->rating ) && $memCWatcheds[$memCWIdx]->course_category->rating >= 3 ? 'rating-yellow' : '' }}" for="form_id_{{ $memCWatcheds[$memCWIdx]->course_category->id }}_star3" title="Rocks!"></label>
                                                            <input type="submit"  id="form_id_{{ $memCWatcheds[$memCWIdx]->course_category->id }}_star2" name="rating" value="2" /><label class="{{ isset( $memCWatcheds[$memCWIdx]->course_category->rating ) && $memCWatcheds[$memCWIdx]->course_category->rating >= 2 ? 'rating-yellow' : '' }}" for="form_id_{{ $memCWatcheds[$memCWIdx]->course_category->id }}_star2" title="Rocks!"></label>
                                                            <input type="submit"  id="form_id_{{ $memCWatcheds[$memCWIdx]->course_category->id }}_star1" name="rating" value="1" /><label class="{{ isset( $memCWatcheds[$memCWIdx]->course_category->rating ) && $memCWatcheds[$memCWIdx]->course_category->rating >= 1 ? 'rating-yellow' : '' }}" for="form_id_{{ $memCWatcheds[$memCWIdx]->course_category->id }}_star1" title="Rocks!"></label>
                                                        </fieldset>
                                                    </form>
                                                </div>                                
                                            </article>
                                        @endfor
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>                                    
                </div>
            </div>
        </div>
    </div>
</section>

<section class="flat-row">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="summary-course">
                    <h2 class="summary-course__h2">Khoá học khác</h2>
                </div>
            </div>
            <div class="col-md-9">
                @for($j = 0; $j < count($newest_data); $j++)
                    @php ($curCourse = $newest_data[$j])
                    @if($j == 0)
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif

                    @if($j != 0 && $j % 3 == 0)
                            </div>
                        </div>
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif
                    
                    @if($j > 2)
                        <div class="item course mr-bottom-0">
                    @else
                        <div class="item course">
                    @endif                    
                        <article class="entry  article-course--height">
                            <div class="featured-post">
                                <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                    @if (file_exists(public_path($curCourse->detail->banner)))
                                        <img src="{!! $curCourse->detail->banner !!}" alt="image">
                                    @else
                                        <img src="/client_assets/image/noimage.jpg" alt="image">
                                    @endif                                        
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="entry-post">
                                        <h3 class="entry-title">
                                            <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                                {{ mb_strlen($curCourse->name) > 16 ? mb_substr($curCourse->name,0,16)."..." : $curCourse->name }}
                                            </a>
                                        </h3>
                                        <div class="entry-author">
                                            <span><a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">{{ $curCourse->detail->teacher_name }}</a></span>
                                        </div>
                                    </div>
                                </div>                               
                            </div>
                            <div class="row">
                                <form action="/like-course" method="POST">
                                    <input id="form_id_{{ $curCourse->id }}" type="hidden" name="courseId" value="{{ $curCourse->id }}">                                        
                                    {{ csrf_field() }}                                        
                                    <fieldset class="rating float-left">                                            
                                        <input type="submit" id="form_id_{{ $curCourse->id }}_star5" name="rating" value="5" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating == 5 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star5" title="Rocks!"></label>
                                        <input type="submit" id="form_id_{{ $curCourse->id }}_star4" name="rating" value="4" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 4 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star4" title="Rocks!"></label>
                                        <input type="submit" id="form_id_{{ $curCourse->id }}_star3" name="rating" value="3" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 3 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star3" title="Rocks!"></label>
                                        <input type="submit" id="form_id_{{ $curCourse->id }}_star2" name="rating" value="2" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 2 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star2" title="Rocks!"></label>
                                        <input type="submit" id="form_id_{{ $curCourse->id }}_star1" name="rating" value="1" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 1 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star1" title="Rocks!"></label>
                                    </fieldset>
                                </form>
                            </div>
                        </article>
                    </div>
                @endfor                    
            </div>
        </div>
    </div>
</section>
<section class="flat-row pd-90 flat-none"></section>
@endsection

@section('footer_styles')

@endsection
