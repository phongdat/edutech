@extends('client/layouts/master')

@section('title', $title)

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/level-list.css">
@endsection

@section('content')

<div class="wrap-slider">
    <div class="container page-container">
        <div class="page-content">
            <div class="page-title">                
                <div class="breadcrumbs">
                    <ul>                        
                        <li>Danh sách các khóa học {{ $curCategory->name }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="flat-row flat-course-sidebar pd-top-100 ">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 sidebar-reponsive">
                <div class="sidebar">
                    <div class="widget widget-categories">
                        <h3 class="widget-title">Danh sách khoá học</h3>
                        <ul>   
                            @for ($i = 0; $i < count($categories); $i++)  
                                <li><a href="/hoc-online/{{ $categories[$i]->slugify }}">{{ $categories[$i]->name }}</a></li>
                            @endfor                                                                                                     
                        </ul>
                    </div>                    
                    <div class="widget widget-latest">
                        <h3 class="widget-title">Tags</h3>
                        <div class="tagspopular">
                            <ul>
                                <li> <a href="javascript:void(0)">
                                    Giáo dục 4.0</a>
                                </li>
                                <li> <a href="javascript:void(0)">
                                    Ôn thi tốt nghiệp môn toán</a>
                                </li>
                                <li> <a href="javascript:void(0)">
                                    Ôn thi tốt nghiệp THPT Quốc Gia môn toán</a>
                                </li>
                                <li> <a href="javascript:void(0)">
                                    Bài tập tự luyện</a>
                                </li>
                                <li> <a href="javascript:void(0)">
                                    Học tiếng anh quá dễ</a>
                                </li>
                                <li> <a href="javascript:void(0)">
                                    Toán 9 có gì đặc sắc</a>
                                </li>
                                <li> <a href="javascript:void(0)">
                                    Bạn không muốn học môn lịch sử vì nhàm chán</a>
                                </li>
                                <li> <a href="javascript:void(0)">
                                    Hóa học là môn học mới</a>
                                </li>
                                <li> <a href="javascript:void(0)">
                                    Đề thi tốt nghiệp THPT</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                @for($j = 0; $j < count($allCourses); $j++)
                    @php ($curCourse = $allCourses[$j])
                    @if($j == 0)
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif

                    @if($j != 0 && $j % 3 == 0)
                            </div>
                        </div>
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif
                    
                    <div class="item course">
                        <article class="entry  article-course--height">
                            <div class="featured-post">
                                <a href="/hoc-online/{{ $curCategory->slugify }}/{{ $curCourse->slugify }}">
                                    @if (file_exists(public_path($curCourse->detail->banner)))
                                        <img src="{!! $curCourse->detail->banner !!}" alt="image">
                                    @else
                                        <img src="/client_assets/image/noimage.jpg" alt="image">
                                    @endif                                        
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="entry-post">
                                        <h3 class="entry-title">
                                            <a href="/hoc-online/{{ $curCategory->slugify }}/{{ $curCourse->slugify }}">
                                                {{ mb_strlen($curCourse->name) > 20 ? mb_substr($curCourse->name,0,20)."..." : $curCourse->name }}
                                            </a>
                                        </h3>
                                        <div class="entry-author">
                                            <span><a href="/hoc-online/{{ $curCategory->slugify }}/{{ $curCourse->slugify }}">{{ $curCourse->detail->teacher_name }}</a></span>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="row">
                                <form action="/like-course" method="POST">
                                    <input id="form_id_{{ $curCourse->id }}" type="hidden" name="courseId" value="{{ $curCourse->id }}">                                        
                                    {{ csrf_field() }}                                        
                                    <fieldset class="rating">                                            
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star5" name="rating" value="5" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating == 5 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star5" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star4" name="rating" value="4" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 4 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star4" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star3" name="rating" value="3" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 3 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star3" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star2" name="rating" value="2" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 2 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star2" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star1" name="rating" value="1" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 1 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star1" title="Rocks!"></label>
                                    </fieldset>
                                </form>
                            </div>
                        </article>
                    </div>
                @endfor 
            </div>
        </div>
    </div>
</section>
<section class="flat-row">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="summary-course">
                    <h2 class="summary-course__h2">Khoá học khác</h2>
                </div>
            </div>
            <div class="col-md-9">
                @for($j = 0; $j < count($newest_data); $j++)
                    @php ($curCourse = $newest_data[$j])
                    @if($j == 0)
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif

                    @if($j != 0 && $j % 3 == 0)
                            </div>
                        </div>
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif
                    
                    @if($j > 2)
                        <div class="item course mr-bottom-0">
                    @else
                        <div class="item course">
                    @endif                    
                        <article class="entry  article-course--height">
                            <div class="featured-post">
                                <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                    @if (file_exists(public_path($curCourse->detail->banner)))
                                        <img src="{!! $curCourse->detail->banner !!}" alt="image">
                                    @else
                                        <img src="/client_assets/image/noimage.jpg" alt="image">
                                    @endif                                        
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="entry-post">
                                        <h3 class="entry-title">
                                            <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                                {{ mb_strlen($curCourse->name) > 16 ? mb_substr($curCourse->name,0,16)."..." : $curCourse->name }}
                                            </a>
                                        </h3>
                                        <div class="entry-author">
                                            <span><a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">{{ $curCourse->detail->teacher_name }}</a></span>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="row">
                                <form action="/like-course" method="POST">
                                    <input id="form_id_{{ $curCourse->id }}" type="hidden" name="courseId" value="{{ $curCourse->id }}">                                        
                                    {{ csrf_field() }}                                        
                                    <fieldset class="rating">                                            
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star5" name="rating" value="5" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating == 5 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star5" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star4" name="rating" value="4" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 4 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star4" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star3" name="rating" value="3" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 3 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star3" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star2" name="rating" value="2" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 2 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star2" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star1" name="rating" value="1" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 1 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star1" title="Rocks!"></label>
                                    </fieldset>
                                </form>
                            </div>
                        </article>
                    </div>
                @endfor                    
            </div>
        </div>
    </div>
</section>
<section class="flat-row pd-90 flat-none"></section>
@endsection

@section('footer_styles')

@endsection
