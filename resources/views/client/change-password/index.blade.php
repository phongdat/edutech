@extends('client/layouts/master')

@section('title', $title)

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/account.css">
@endsection

@section('content')
<div class="wrap-slider">
    <div class="container page-container">
        <div class="page-content">
            <div class="page-title">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="javascript:void(0)">TÀI KHOẢN</a></li>                    
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="flat-row pd-bottom-0">
    <div class="container">
        <div class="row mr-bottom-48">
            <div class="col-md-3">
                <ul class="left-list-container">
                    <li class="left-list-item"><a href="/account"><b>Trang chính</b></a></li>
                    <li class="left-list-item"><a href="/danh-gia">Đánh giá</a></li>
                    <li class="left-list-item"> <a href="/lich-su-giao-dich">Lịch sử giao dịch</a></li>
                    <li class="left-list-item"> <a href="/chinh-sua-ho-so">Chỉnh sửa hồ sơ</a></li>
                    <li class="left-list-item"> <a href="/doi-mat-khau">Đổi mật khẩu</a></li>
                    <li class="left-list-item"> <a href="/logout">Đăng xuất</a></li>
                </ul>
            </div>
            <div class="col-md-8 pd-top-20">                           
                <div class="group-content-container">
                    
                    <div class="group-content">
                        <form action="/saveChangePassword" method="POST">
                        @csrf  
                        <div class="form-group row">
                        <h2>Đổi mật khẩu</h2>
                        <br>
                            <label for="fullname" class="col-md-4 col-form-label text-md-right">Mật khẩu</label>
                            <div class="col-md-7">
                                <input type="password" id="password" class="form-control" name="password" required autofocus>
                            </div>
                        </div>                                  
                        <div class="form-group row">
                            <label for="fullname" class="col-md-4 col-form-label text-md-right">Xác nhận mật khẩu</label>
                            <div class="col-md-7">
                                <input type="password" id="password_confirmation" class="form-control" name="password_confirmation" required autofocus>
                            </div>
                        </div>                                  
                        <div class="row control-group mr-top-20">
                            <label class="col-md-4"></label>			      
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-login-page">CHỈNH SỬA</button>
                            </div>
                        </div>                                               
                    </form>
                    </div>
                </div>                                
            </div>
        </div>
    </div>
</section>

<section class="flat-row pd-top-0">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="summary-course">
                    <h2 class="summary-course__h2">Khoá học khác</h2>
                </div>
            </div>
            <div class="col-md-9">
                @for($j = 0; $j < count($newest_data); $j++)
                    @php ($curCourse = $newest_data[$j])
                    @if($j == 0)
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif

                    @if($j != 0 && $j % 3 == 0)
                            </div>
                        </div>
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif
                    
                    @if($j > 2)
                        <div class="item course mr-bottom-0">
                    @else
                        <div class="item course">
                    @endif                    
                        <article class="entry  article-course--height">
                            <div class="featured-post">
                                <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                    @if (file_exists(public_path($curCourse->detail->banner)))
                                        <img src="{!! $curCourse->detail->banner !!}" alt="image">
                                    @else
                                        <img src="/client_assets/image/noimage.jpg" alt="image">
                                    @endif                                        
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-md-10 col-xs-8">
                                    <div class="entry-post">
                                        <h3 class="entry-title">
                                            <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                                {{ mb_strlen($curCourse->name) > 16 ? mb_substr($curCourse->name,0,16)."..." : $curCourse->name }}
                                            </a>
                                        </h3>
                                        <div class="entry-author">
                                            <span><a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">{{ $curCourse->detail->teacher_name }}</a></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">                                                                                    
                                    @if( isset( $curCourse->liked ) )

                                        @if( $curCourse->liked == true )
                                            <span class="heart liked" data-course-id="{{ $curCourse->id }}">
                                                <i class="fas fa-heart fa-heart-red"></i>
                                        @else
                                            <span class="heart" data-course-id="{{ $curCourse->id }}">
                                                <i class="far fa-heart"></i>
                                        @endif

                                    @else
                                        <span class="heart" data-course-id="{{ $curCourse->id }}">
                                            <i class="far fa-heart"></i>
                                    @endif                                            
                                    </span>
                                </div>
                            </div>
                        </article>
                    </div>
                @endfor                    
            </div>
        </div>
    </div>
</section>
<section class="flat-row pd-90 flat-none"></section>

@endsection

@section('footer_styles')

@endsection