@extends('client/layouts/master')

@section('title', $title)

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/list-news.css">
@endsection

@section('content')

<div class="wrap-slider">
    <div class="container page-container">
        <div class="page-content">
            <div class="page-title">                
                <div class="breadcrumbs">
                    <ul>                        
                        <li>Tin tức</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="flat-row flat-course-sidebar pd-top-100 ">
    <div class="container">        
        <div class="row">
            <div class="col-md-8">
                @if( isset($newses) && sizeof($newses) > 0 )
                    <div class="article-general latest-article">
                        <img src="{{ $newses[0]->banner }}" alt="" />
                        <div class="content-wrapper">
                            <span>{{ $newses[0]->tags }}</span>
                            <h6>
                                {{ $newses[0]->title }}
                            </h6>
                            <a href="/tin-tuc/{{ $newses[0]->slugify_url }}">Xem thêm</a>
                        </div>
                    </div>
                @endif                
                @if( isset($newses) && sizeof($newses) > 1 )
                <span class="lifeplus-subtitle news-article">Bài viết mới</span>
                    <div class="articles-wrapper">
                        @for($i = 1; $i < sizeof($newses); $i++)
                            @if($i == 1)
                                <div class="row">
                            @endif

                            @if ($i != 1 && $i % 2 != 0)
                                </div>
                                <div class="row">
                            @endif
                            <div class="col-md-6">
                                <div class="article-general">
                                    <img class="news-img" src="{{ $newses[$i]->banner }}" alt="" />
                                    <div class="content-wrapper">
                                        <span>{{ $newses[$i]->tags }}</span>
                                        <h6>{{ $newses[$i]->title }}</h6>
                                        <p>
                                        {{ mb_strlen($newses[$i]->summary) > 360 ? mb_substr($newses[$i]->summary,0,357)."..." : $newses[$i]->summary }}
                                        </p>
                                        <a href="/tin-tuc/{{ $newses[$i]->slugify_url }}">Xem thêm</a>
                                    </div>
                                </div>
                            </div>
                        @endfor
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-md-4">
                <div class="recommend-articles">
                    @if( isset($newses) && sizeof($newses) > 0 )                 
                    <h3 class="mg-top-0">Bài viết đề xuất</h3>   
                        <div class="recommend-wrapper">                       
                            <a href="/tin-tuc/{{ $newses[0]->slugify_url }}">
                                <div class="recommend">
                                    <img src="{{ $newses[0]->banner }}" alt="" />
                                    <p>
                                        {{ $newses[0]->title }}
                                    </p>
                                </div>
                            </a>                                              
                        </div>
                    @endif
                </div>                
            </div>
        </div>       
    </div>
</section>

<section class="flat-row">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="summary-course">
                    <h2 class="summary-course__h2">Khoá học khác</h2>
                </div>
            </div>
            <div class="col-md-9">
                @for($j = 0; $j < count($newest_data); $j++)
                    @php ($curCourse = $newest_data[$j])
                    @if($j == 0)
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif

                    @if($j != 0 && $j % 3 == 0)
                            </div>
                        </div>
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif
                    
                    @if($j > 2)
                        <div class="item course mr-bottom-0">
                    @else
                        <div class="item course">
                    @endif                    
                        <article class="entry  article-course--height">
                            <div class="featured-post">
                                <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                    @if (file_exists(public_path($curCourse->detail->banner)))
                                        <img src="{!! $curCourse->detail->banner !!}" alt="image">
                                    @else
                                        <img src="/client_assets/image/noimage.jpg" alt="image">
                                    @endif                                        
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="entry-post">
                                        <h3 class="entry-title">
                                            <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                                {{ mb_strlen($curCourse->name) > 16 ? mb_substr($curCourse->name,0,16)."..." : $curCourse->name }}
                                            </a>
                                        </h3>
                                        <div class="entry-author">
                                            <span><a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">{{ $curCourse->detail->teacher_name }}</a></span>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="row">
                                <form action="/like-course" method="POST">
                                    <input id="form_id_{{ $curCourse->id }}" type="hidden" name="courseId" value="{{ $curCourse->id }}">                                        
                                    {{ csrf_field() }}                                        
                                    <fieldset class="rating">                                            
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star5" name="rating" value="5" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating == 5 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star5" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star4" name="rating" value="4" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 4 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star4" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star3" name="rating" value="3" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 3 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star3" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star2" name="rating" value="2" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 2 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star2" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star1" name="rating" value="1" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 1 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star1" title="Rocks!"></label>
                                    </fieldset>
                                </form>
                            </div>
                        </article>
                    </div>
                @endfor                    
            </div>
        </div>
    </div>
</section>
<section class="flat-row pd-90 flat-none"></section>

@endsection

@section('footer_styles')

@endsection