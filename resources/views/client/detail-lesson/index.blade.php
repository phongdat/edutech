@extends('client/layouts/master')

@section('title', $title)

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/account.css">
@endsection

@section('content')
<div class="wrap-slider">
    <div class="container page-container">
        <div class="page-content">
            <div class="page-title">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="javascript:void(0)">TÀI KHOẢN</a></li>                    
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="flat-row pd-bottom-0">
    <div class="container">
        <div class="row mr-bottom-48">
            <div class="col-md-3">
                <ul class="left-list-container">
                    <li class="left-list-item"><a href="/account"><b>Trang chính</b></a></li>
                    <li class="left-list-item"><a href="/danh-gia">Đánh giá</a></li>
                    <li class="left-list-item"> <a href="/lich-su-giao-dich">Lịch sử giao dịch</a></li>
                    <li class="left-list-item"> <a href="/chinh-sua-ho-so">Chỉnh sửa hồ sơ</a></li>
                    <li class="left-list-item"> <a href="/doi-mat-khau">Đổi mật khẩu</a></li>
                    <li class="left-list-item"> <a href="/logout">Đăng xuất</a></li>
                </ul>
            </div>
            <div class="col-md-9 pd-top-20 content">                           
                <div class="group-content-container">
                    <div class="col-md-7">
                        <h2>{{$lesson->title}}</h2>
                        <textarea class="lesson-content" name="content">{{$lesson->content}}</textarea>
                    </div>
                    <div class="col-md-5">
                        <h2>Hình ảnh</h2>
                        <img id="holder" style="margin-top:15px;max-height:100px;">
                        <img id="preview-image" src="{{ isset($lesson) ? url($lesson->banner) : "" }}" style="margin-top:15px;max-width:300px;" class="" />
                        <h2>Video</h2>
                            <input type="hidden" id="lesson_video" name="video">
                            <input type="hidden" id="lesson_video_extension" name="video_extension">
                            @if(isset($lesson->video))
                            <video  style="margin-top:15px;max-width:300px;" controls>
                                <source src="{{ isset($lesson->video) ? url($lesson->video) : "" }}" type="video/mp4">
                            </video>
                            @endif
                        <h2>Tags</h2>
                        @if ( isset($lesson) )
                            @foreach ($lesson->tag_list as $tag)
                            <option value="{{ $tag }}" selected>{{ $tag }}</option>
                            @endforeach
                        @endif
                    </div> 
                </div>                                
            </div>
        </div>
    </div>
</section>
<section class="flat-row pd-90 flat-none"></section>
@endsection

@section('footer_scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    var textarea = document.querySelector('textarea');
    textarea.addEventListener('keydown', autosize);
    
    function autosize(){
        var el = this;
        setTimeout(function(){
            el.style.cssText = 'height:auto';
            // for box-sizing other than "content-box" use:
            // el.style.cssText = '-moz-box-sizing:content-box';
            el.style.cssText = 'height:' + el.scrollHeight + 'px';
        },0);
    }
</script>
<script>
    tinymce.init({
        selector: 'textarea',
        plugins: 'link',
        plugins: "image",
        readonly : true,
        menubar: true,
        height: 500,
        resize: true
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="{{ url('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
<script src="https://transloadit.edgly.net/releases/uppy/v1.4.0/uppy.min.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var uppy = Uppy.Core()
        .use(Uppy.Dashboard, {
            inline: true,
            target: '#drag-drop-area',
            height: 170,
        })
        .use(Uppy.Tus, {
            endpoint: 'http://edu.quanly.io:3111/files/',
            // endpoint: 'http://localhost:3111/files/',
            chunkSize: 500000,
        })

      uppy.on('complete', (result) => {
        $("#lesson_video_extension").val(result.successful[0].type);
        $("#lesson_video").val(result.successful[0].uploadURL);
      })
</script>
<script>
    $(document).ready(function(){
        $('#tags').select2({
            placeholder: "Type and press enter",
            tags: true
        });
        $('.uppy-Dashboard-inner').css("min-height", '100px');
        $('#lfm').filemanager('file');
    });
</script>
@endsection