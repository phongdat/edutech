@extends('client/layouts/master')

@section('title', $title)

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/contact.css">
@endsection

@section('content')

<div class="wrap-slider">
    <div class="container page-container">
        <div class="page-content">
            <div class="page-title">                
                <div class="breadcrumbs">
                    <ul>                        
                        <li>Liên hệ với chúng tôi</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="flat-row bg-theme pd-top-100 pd-bottom-100 contact">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <form id="formcontact" class="form-contact" method="post" action="/submit-contact-form">
                    @csrf
                    <div class="form-contact-title">
                        <h6 class="contact-title">Liên hệ với chúng tôi qua Email!</h6>
                        <p>Mọi thắc mắc và yêu cầu hỗ trợ từ EduTech, vui lòng để lại thông tại đây. Chúng tôi sẽ gửi phản hồi sớm nhất cho bạn.</p>
                    </div>
                    <div class="info-contact">
                        <p class="wrap-input-name">
                            <input type="text" id="name" name="name" value="" tabindex="1" required="required" placeholder="Họ và tên">
                        </p>
                        <p class="wrap-input-phone">
                            <input type="text" id="phone" name="phone" value="" tabindex="2" required="required" placeholder="Số điện thoại">
                        </p>
                        <p class="wrap-input-email">
                            <input type="text" id="email" name="email" value="" tabindex="3" required="required" placeholder="Email">
                        </p>
                        <p class="wrap-input-messages">
                            <textarea id="messages-contact" name="messages" tabindex="4" placeholder="Tin nhắn" required></textarea>
                        </p>
                        <div class="wrap-btn">
                            <button class="bg-color">Hoàn tất</button>
                        </div>
                    </div> 
                </form>
            </div>
            <div class="col-md-8 col-sm-6">
                <div class="wrap-maps">
                    <div class="flat-map" id="map"></div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('footer_styles')

@endsection