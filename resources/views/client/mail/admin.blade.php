<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EduTech Confirmation</title>
    <style>
        p{
            font-size: 15px;
        }
    </style>
</head>
<body>
    <p>Dear <b>Admin,</b></p>
    <p>We have a request from <b>Mr/Mrs {{ $name }}</b>.</p>
    <p>Customer's email: <b> {{ $email }}. </b></p>
    <p>Customer's telephone number: <b> {{ $phone }}. </b></p>
    <p>Customer's message: <b> {{ $msg }}.</b></p>    
    <p>Your sincerely,</p>
    <p><b>EduTech Web Admin</b></p>
</body>
</html>