@extends('client/layouts/master')

@section('title', $title)

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/account.css">
@endsection

@section('content')
<div class="wrap-slider">
    <div class="container page-container">
        <div class="page-content">
            <div class="page-title">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="javascript:void(0)">TÀI KHOẢN</a></li>                    
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="flat-row pd-bottom-0">
    <div class="container">
        <div class="row mr-bottom-48">
            <div class="col-md-3">
                <ul class="left-list-container">
                    <li class="left-list-item"><a href="/account"><b>Trang chính</b></a></li>
                    <li class="left-list-item"><a href="/danh-gia">Đánh giá</a></li>
                    <li class="left-list-item"> <a href="/lich-su-giao-dich">Lịch sử giao dịch</a></li>
                    <li class="left-list-item"> <a href="/chinh-sua-ho-so">Chỉnh sửa hồ sơ</a></li>
                    <li class="left-list-item"> <a href="/doi-mat-khau">Đổi mật khẩu</a></li>
                    <li class="left-list-item"> <a href="/logout">Đăng xuất</a></li>
                </ul>
            </div>
            <div class="col-md-9 pd-top-20 content">                           
                <div class="group-content-container">
                    <h2>Đánh giá</h2>                    
                    <p>Giảng viên: {{ $member->full_name }}</p>                    
                    <div class="">
                        <h4><b>Bài giảng</b></h4>
                        <div class="table-responsive mr-bottom-48" style="overflow: auto;">  
                            <table class="table table-hover mb-0 ps-container ps-theme-default table-fixed css-serial">
                                <thead style="padding-right: 20px">
                                    <tr>
                                        <th class="col-md-1">STT</th>                                
                                        <th class="col-md-5">Tên bài giảng</th>
                                        <th class="col-md-2">Ngày đăng</th>                                    
                                        <th class="col-md-2">Trạng thái</th>
                                        <th class="col-md-1">Đánh giá</th>                                    
                                    </tr>
                                </thead>
                            
                                <tbody class="vertical-scroll scroll-example height-300">
                                    @foreach($lessons as $index => $lesson)
                                    <tr>
                                        <td class="col-md-1">{{ $index }}</td>
                                        <td class="col-md-5" style="text-align: center;">{{ $lesson->title }}</td>
                                        <td class="col-md-2" style="text-align: center;">{{ $lesson->created_at }}</td>
                                        <td class="col-md-2" style="text-align: center;"> {{ $lesson->status }} </td>
                                        <td class="col-md-1">{{ round( $lesson->rating, 2 ) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div> 

                        <h4 style=""><b>Bài giảng chưa đánh giá</b></h4>
                        <div class="table-responsive visible-scroll always-visible scroll-example height-300" style="overflow: auto;">  
                            <table class="table table-hover mb-0 ps-container ps-theme-default css-serial horz-scroll-content">
                                    <thead>
                                        <tr style="display: flex;">
                                            <th style="width:100px;">STT</th>
                                            <th style="width:400px;">Tên bài giảng</th>
                                            <th style="width:200px;">Ngày gửi yêu cầu</th>
                                            <th style="width:300px;">Nội dung</th>   
                                            <th style="width:300px;">Đánh giá</th>                                   
                                        </tr>
                                    </thead>
                                
                                    <tbody>
                                        @foreach($rates as $rate)
                                        <form method="post" action="{{url('/rating')}}">
                                            <input type="hidden" name="rateId" value="{{$rate->id}}">
                                                {{ csrf_field() }}
                                            <tr style="display: flex;">
                                                <td style="width:100px;"></td>
                                                <td style="text-align: center; width:400px;"><a href="{{url('/bai-giang/'.$rate->lesson->id)}}">{{$rate->lesson->title}}</a></td>
                                                <td style="text-align: center; width:200px;">{{$rate->created_at}}</td>
                                                <td style="text-align: center; width:300px;">                                                    
                                                    <textarea style="border: 1px solid;" id="feedback_content" name="feedback_content" rows="4" cols="50"></textarea>
                                                </td>
                                                <td style="width:300px;">
                                                    <fieldset class="rating">
                                                        <input type="submit" id="star10" name="rating" value="10" onclick="return confirmAction()" /><label for="star10" title="Rocks!">10 stars</label>
                                                        <input type="submit" id="star9" name="rating" value="9" onclick="return confirmAction()" /><label for="star9" title="Rocks!">9 stars</label>
                                                        <input type="submit" id="star8" name="rating" value="8" onclick="return confirmAction()" /><label for="star8" title="Rocks!">8 stars</label>
                                                        <input type="submit" id="star7" name="rating" value="7" onclick="return confirmAction()" /><label for="star7" title="Rocks!">7 stars</label>
                                                        <input type="submit" id="star6" name="rating" value="6" onclick="return confirmAction()" /><label for="star6" title="Rocks!">6 stars</label>
                                                        <input type="submit" id="star5" name="rating" value="5" onclick="return confirmAction()" /><label for="star5" title="Rocks!">5 stars</label>
                                                        <input type="submit" id="star4" name="rating" value="4" onclick="return confirmAction()" /><label for="star4" title="Rocks!">4 stars</label>
                                                        <input type="submit" id="star3" name="rating" value="3" onclick="return confirmAction()" /><label for="star3" title="Rocks!">3 stars</label>
                                                        <input type="submit" id="star2" name="rating" value="2" onclick="return confirmAction()" /><label for="star2" title="Rocks!">2 stars</label>
                                                        <input type="submit" id="star1" name="rating" value="1" onclick="return confirmAction()" /><label for="star1" title="Rocks!">1 stars</label>
                                                    </fieldset>
                                                </td>                                                
                                            </tr>
                                        </form>
                                        @endforeach
                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div>                                
            </div>
        </div>
    </div>
</section>
<section class="flat-row pd-90 flat-none"></section>
<script src="http://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script LANGUAGE="JavaScript">
function confirmAction() {
    return confirm("Bạn có chắc chắn với đánh giá này không ?")
}
</script>
@endsection

@section('footer_styles')

@endsection