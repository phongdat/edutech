<footer id="footer" class="footer style2">
    <div class="container pd-left-0 pd-right-0">
        <div class="footer-widget">
            <div class="row">                
                <div class="col-md-4 col-sm-12 pd-left-0">
                    <div class="widget widget-our-services">
                        <div class="left-menu">
                            <ul>
                                <li><a href="/"><b>Trang chủ</b></a></li>
                                <li><a href="/introduce">Giới thiệu</a></li>
                                <li><a href="/tin-tuc">Tin tức</a></li>
                                <li><a href="javascript:void(0)">Thư viện</a></li>
                                <li><a href="/contact">Liên hệ</a></li>
                                <li><a href="/policy">Chính sách bảo mật</a></li>
                                <li><a href="/agreement">Thoả thuận sử dụng</a></li>
                                <li><a href="/nhan-su-va-to-chuc">Nhân sự và Tổ chức</a></li>
                            </ul>
                        </div>
                        <div class="right-menu">
                            <ul>
                                <li><a href="javascript:void(0)"><b>Khoá học</b></a></li>
                                @for ($i = 0; $i < count($categories); $i++)                                     
                                    <li><a href="/hoc-online/{{ $categories[$i]->slugify }}">{{ $categories[$i]->name }}</a></li>
                                @endfor
                            </ul>
                        </div>
                    </div>
                </div>                
                <div class="col-md-4 col-sm-12 pd-left-67">                                        
                    <div class="widget widget-our-services">
                        <div class="logo-footer">
                            <a href="/"><img src="/client_assets/image/logo.png" alt="image"></a>
                        </div>
                        <p class="fz-13 mr-bottom-48">
                            Phone: {!! $configs['so-dien-thoai'] !!} <br/>
                            Fax: {!! $configs['fax'] !!} <br/>
                            Email: {!! $configs['email'] !!} <br/>
                        </p>
                        <ul class="flat-info">
                            <li>
                                <a href="javascript:void(0)">
                                    {!! $configs['icon-twitter'] !!}
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    {!! $configs['icon-google-plus'] !!}
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    {!! $configs['icon-facebook'] !!}
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    {!! $configs['icon-instagram'] !!}
                                </a>
                            </li>  
                            <li class="pd-left-25">
                                <a href="/bo-cong-thuong">
                                    <img style="width:90px;" src="{!! $configs['notification'] !!}" alt="">
                                </a>
                            </li>
                        </ul>                                            
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 pd-right-0 pd-left-107">
                    <div class="widget widget-our-services">
 
                        <h2 class="title">{!! $configs['vien-nghien-cuu-tai-chinh'] !!}</h2>
                        <p class="fz-13">{!! $configs['dia-chi'] !!}</p>
                        <p class="fz-13">{!! $configs['vpgd'] !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="bottom style2">
    <div class="container">
        <div class="copyright">
            <p>{!! $configs['copyright'] !!}</p>
        </div>
    </div>
</div>