<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>    
    <meta charset="utf-8">
    <title>
        @yield('title')
    </title>
    <meta name="author" content="ftcjsc.com">    
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="{{ url('client_assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('css/client-all.css') }}" rel="stylesheet">
    <link href="{{ url('client_assets/image/icon.png') }}" rel="shortcut icon">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.4.0/mapbox-gl.css' rel='stylesheet' />
    <link rel="stylesheet" href="{{ url('css/bootstrap-extended.css')}}">
    <link rel="stylesheet" href="{{ url('css/app.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('header_styles')
</head>

<body>    
    <section class="loading-overlay">
        <div class="Loading-Page">
            <h2 class="loader">Loading</h2>
        </div>
    </section>
    
    <div class="boxed position_form">

        @include('client.layouts.header')

        @yield('content')

        @include('client.layouts.footer')

        <a class="go-top show"></a>
    </div>   
    <div class="modal fade" id="notification-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">  
                <div class="modal-header">                    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>          
            <div class="modal-body">
                <div id="notification-modal-content" class="content">
                    <h4>THÔNG BÁO</h4>
                    <h4>VIỆN NGHIÊN CỨU TÀI CHÍNH, ĐẦU TƯ VÀ HỢP TÁC , THƯƠNG MẠI ĐÔNG NAM Á</h4>
                    <h5>Trân trọng thông báo</h5>
                    <br>
                    <p>Nhằm không ngừng tăng cường và nâng cao chất lượng hệ thống học trực tuyến EDUTECH đáp ứng ngày càng tăng của đông đảo cộng đồng người học. Viện nghiên cứu Tài chính, Đầu tư và Hợp tác, Thương mại Đông Nam Á chủ trương nâng cấp hệ thống học EduTech phiên bản mới EDUTECH V2.0 ( Version 2.0).</p>
                    <p>Trong thời gian nâng cấp phiên bản mới, chúng tôi mong muốn nhận được sự đồng cảm và chia sẽ của cộng đồng nếu có các vấn đề khó khăn, vướng mắc khi sử dụng hệ thống</p>
                    <br>
                    <p>Xem hướng dẫn đăng ký, đăng nhập, lấy lại mật khẩu <a href="/huong-dan">tại đây</a>.</p>                                  
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>                
            </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ url('client_assets/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('client_assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('client_assets/js/imagesloaded.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('client_assets/js/jquery.isotope.min.js') }}"></script>    
    
    <script type="text/javascript" src="{{ url('js/client-all.js') }}"></script>
    <script type="text/javascript" src="{{ url('client_assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script src="https://api.tiles.mapbox.com/mapbox-gl-js/v1.4.0/mapbox-gl.js"></script>

    <div id="fb-root"></div>
    <script>      
        window.fbAsyncInit = function() {
        FB.init({
            xfbml : true,
            version : 'v4.0'
            });
        };
        
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    
    <!-- Your customer chat code -->
    <div class="fb-customerchat" attribution=setup_tool page_id="836462033387563"
        logged_in_greeting="Chào bạn ! EduTech đã sẵn sàng giải đáp mọi thắc mắc của bạn !"
        logged_out_greeting="Chào bạn ! Hy vọng bạn hài lòng với các giải đáp của chúng tôi !">
    </div>

    @if (session('error'))
        <script>
            $(document).ready(function() {
                $.toast({
                    heading: 'Oops...',
                    text: '{{ session("error") }}',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                    
                });
            });
        </script>
    @endif
    @if (session('success'))
        <script>
            $(document).ready(function() {
                $.toast({
                    heading: 'Congratulations...',
                    text: '{{ session("success") }}',
                    position: 'top-right',
                    loaderBg:'#17a2b8',
                    icon: 'success',
                    hideAfter: 3500
                    
                });
            });
        </script>
    @endif
    @if( isset( $notifications ) )
        <script>
            $(document).ready(function() {
                var notifications = {!! json_encode($notifications) !!};                
                $.each(notifications, function(index, value) {
                    $.toast({
                        heading: value.title,
                        text: value.content,
                        position: 'top-right',
                        icon: 'info',                    
                        loaderBg: '#17a2b8',
                        bgColor: '#A8FE11',
                        hideAfter: 5500
                    })
                });                       
            });
    </script>
    @endif
    <script>
        $("img").lazyload();
    </script>  
    <script>
        $(document).ready(function() {
            document.addEventListener("keyup", function (e) {
                var keyCode = e.keyCode ? e.keyCode : e.which;
                        if (keyCode == 44) {
                            stopPrntScr();
                        }
                    });

                $(document).keydown(function(event){
                    if(event.keyCode==123){
                        return false;
                    }                        
                });

            function stopPrntScr() {

                var inpFld = document.createElement("input");
                inpFld.setAttribute("value", ".");
                inpFld.setAttribute("width", "0");
                inpFld.style.height = "0px";
                inpFld.style.width = "0px";
                inpFld.style.border = "0px";
                document.body.appendChild(inpFld);
                inpFld.select();
                document.execCommand("copy");
                inpFld.remove(inpFld);
            }

            function AccessClipboardData() {
                try {
                    window.clipboardData.setData('text', "Access   Restricted");
                } catch (err) {
                }
            }

            setInterval(AccessClipboardData(), 300);        
        
        });      
        function confirmAction() {
            return confirm("Bạn có chắc chắn với đánh giá này không ?")
        } 
    </script>      

    <div class="div-processing"></div>
    @yield('footer_scripts')
</body>

</html>