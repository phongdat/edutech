<div class="wrap-header clearfix">
    <div class="top top-style3">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12 pd-left-0">
                    <ul class="flat-info">
                        <li>
                            <img class="image-tel-icon--h" src="{!! $configs['icon-telephone'] !!}" alt="">
                            {!! $configs['so-dien-thoai'] !!}
                        </li>
                        <li class="pd-right-0-mobile">
                            <img class="image-letter-icon--h" src="{!! $configs['letter-icon'] !!}" alt="">
                            {!! $configs['email'] !!}
                        </li>
                        <li class="only-desktop pd-right-10"><a href="javascript:void(0)">
                            {!! $configs['icon-twitter'] !!}
                        </a></li>
                        <li class="only-desktop pd-right-10"><a href="javascript:void(0)">
                            {!! $configs['icon-google-plus'] !!}
                        </a></li>
                        <li class="only-desktop pd-right-10"><a href="javascript:void(0)">
                            {!! $configs['icon-facebook'] !!}
                        </a></li>
                        <li class="only-desktop pd-right-10"><a href="javascript:void(0)">
                            {!! $configs['icon-instagram'] !!}
                        </a></li>                       
                    </ul>
                </div>
                <div class="col-xs-12 only-mobile">
                    <ul class="flat-info">
                        <li><a href="javascript:void(0)">
                            {!! $configs['icon-twitter'] !!}
                        </a></li>
                        <li><a href="javascript:void(0)">
                            {!! $configs['icon-google-plus'] !!}
                        </a></li>
                        <li><a href="javascript:void(0)">
                            {!! $configs['icon-facebook'] !!}
                        </a></li>
                        <li><a href="javascript:void(0)">
                            {!! $configs['icon-instagram'] !!}
                        </a></li>  
                    </ul>
                </div>
                <div class="col-md-6 col-xs-12 pd-right-0">
                    <ul class="flat-info float-right-desktop">
                        <li class="pd-right-16"><a href="/introduce">Giới thiệu</a></li>
                        <li class="pd-right-16"><a href="/tin-tuc">Tin tức</a></li>
                        <li class="pd-right-16"><a href="/">Thư viện</a></li>
                        <li class="pd-right-16"><a href="/contact">Liên hệ</a></li>
                        @if(isset($user) && !is_null($user))
                            <li class="li--pdr-13 only-desktop">
                                <a href="/account" class="btn btn-success btn-welcome">
                                    Chào, {{ strlen( $user->username ) > 13 ? substr( $user->username ,0, 13 ) . "..." : $user->username  }}
                                </a>
                            </li>
                            <li class="only-desktop">
                                <a href="/logout" class="btn btn-danger btn-logout">
                                    Đăng xuất
                                </a>
                            </li>
                        @else
                            <li class="li--pdr-13 only-desktop">
                                <a href="/register" class="btn btn-warning btn-register">
                                    Đăng ký
                                </a>
                            </li>
                            <li class="only-desktop">
                                <a href="/login" class="btn btn-success btn-login">
                                    Đăng nhập
                                </a>
                            </li>
                        @endif
                        
                    </ul>
                </div>
                <div class="col-xs-12 only-mobile">
                    <ul class="flat-info">
                        @if(isset($user) && !is_null($user))
                            <li class="li--pdr-13">
                                <a href="/account" class="btn btn-success btn-welcome">
                                    Chào, {{ strlen( $user->username ) > 13 ? substr( $user->username ,0, 13 ) . "..." : $user->username  }}
                                </a>
                            </li>
                            <li>
                                <a href="/logout" class="btn btn-danger btn-logout">
                                    Đăng xuất
                                </a>
                            </li>
                        @else
                            <li class="li--pdr-13">
                                <a href="/register" class="btn btn-warning btn-register">Đăng ký</a>
                            </li>
                            <li>
                                <a href="/login" class="btn btn-success btn-login">Đăng nhập</a>
                            </li>
                        @endif                       
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <header id="header" class="header header-style3 styleheader">
        <div class="container pd-right-0">           
        <div class="row">
            <div class="col-md-12 pd-left-0 pd-right-0">
                <div class="logo col-md-3 pd-left-0">
                    <a href="/"><img src="{!! $configs['logo'] !!}" alt="image"></a>
                </div>
                <div class="flat-search">                    
                    <div class="btn-menu">
                        <span></span>
                    </div><!-- //mobile menu button -->
                </div>
                <div class="wrap-nav col-md-9 text-right">
                    <nav id="mainnav" class="mainnav">
                        <ul class="menu">
                            @for ($i = 0; $i < count($categories); $i++)                                     
                                <li>
                                    <a href="/hoc-online/{{ $categories[$i]->slugify }}" title="">{{ $categories[$i]->name }}</a>
                                    @if (count($courses[$categories[$i]->name]) != 0)
                                        @if ($i == (count($categories) - 1))
                                            <ul class="submenu rightsubmenu">
                                        @else
                                            <ul class="submenu">
                                        @endif

                                            @for($j = 0; $j < count($courses[$categories[$i]->name]); $j++)
                                                @php ($curCourse = $courses[$categories[$i]->name][$j])
                                                <li>
                                                    <a href="/hoc-online/{{ $categories[$i]->slugify }}/{{ $curCourse->slugify }}">
                                                        {{ $curCourse->name }}
                                                    </a>
                                                </li>
                                            @endfor     

                                        </ul>
                                    @endif
                                </li>
                            @endfor                                                                
                        </ul>
                    </nav>
                </div>
            </div>   
            </div>          
        </div>
    </header>
</div>