@extends('client/layouts/master')

@section('title',  (isset($page) ? $page->title : "") )

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/policy.css">
@endsection

@section('content')

<div class="wrap-slider">
    <div class="container page-container">
        <div class="page-content">
            <div class="page-title">                
                <div class="breadcrumbs">
                    <ul>                        
                        <li>{{ isset($page) ? $page->title : "" }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="flat-row">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-section color-title">
                    <h1 class="title">{{ isset($page) ? $page->title : "" }}</h1>
                </div>
            </div>
            <!-- col-md-12 -->
        </div>
        <!-- row -->
    </div>
    <!-- container -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="about-us style2 wrap-box pdleft">
                    <div class="text-about-us">
                        {!! isset($page) ? $page->content : "" !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('footer_styles')

@endsection