@extends('client/layouts/master')

@section('title', $title)

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/home.css">
@endsection

@section('content')
<section class="flat-row pd-550 flat-none"></section>

@for ($i = 0; $i < count($categories); $i++)      
    @if($i == 1 || $i % 2 != 0)  
        <section class="flat-row bg-thcs">
    @else
        <section class="flat-row">        
    @endif    
        <div class="container pd-left-0 pd-right-0">
            <div class="row">
                <div class="col-md-3">
                    <div class="summary-course">
                        <h2 class="summary-course__h2">{{ $categories[$i]->name }}</h2>
                    </div>
                    <div class="text-about-us">
                        {!! $categories[$i]->description !!}
                        <div>
                            <a href="/hoc-online/{{ $categories[$i]->slugify }}">XEM TẤT CẢ</a>
                        </div>                        
                    </div>                    
                </div>
                <div class="col-md-9">
                    @for($j = 0; $j < count($courseShow[$categories[$i]->name]); $j++)
                        @php ($curCourse = $courseShow[$categories[$i]->name][$j])
                        @if($j == 0)
                            <div class="portfolio style2">
                                <div class="portfolio-wrap clearfix">   
                        @endif

                        @if($j != 0 && $j % 3 == 0)
                                </div>
                            </div>
                            <div class="portfolio style2">
                                <div class="portfolio-wrap clearfix">   
                        @endif
                        
                        @if($j > 2)
                            <div class="item course mr-bottom-0">
                        @else
                            <div class="item course">
                        @endif 
                        
                        @if($i == 1 || $i % 2 != 0)  
                            <article class="entry  article-course--height bg-thcs">                            
                        @else
                            <article class="entry  article-course--height">     
                        @endif                                
                                <div class="featured-post">
                                    <a href="/hoc-online/{{ $categories[$i]->slugify }}/{{ $curCourse->slugify }}">
                                        @if (file_exists(public_path($curCourse->detail->banner)))
                                            <img src="{!! $curCourse->detail->banner !!}" alt="image">
                                        @else
                                            <img src="/client_assets/image/noimage.jpg" alt="image">
                                        @endif                                        
                                    </a>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="entry-post">
                                            <h3 class="entry-title">
                                                <a href="/hoc-online/{{ $categories[$i]->slugify }}/{{ $curCourse->slugify }}">
                                                {{ mb_strlen($curCourse->name) > 20 ? mb_substr($curCourse->name,0,20)."..." : $curCourse->name }}
                                                </a>
                                            </h3>
                                            <div class="entry-author">
                                                <span><a href="/hoc-online/{{ $categories[$i]->slugify }}/{{ $curCourse->slugify }}">{{ $curCourse->detail->teacher_name }}</a></span>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <form action="/like-course" method="POST">
                                        <input id="form_id_{{ $curCourse->id }}" type="hidden" name="courseId" value="{{ $curCourse->id }}">                                        
                                        {{ csrf_field() }}                                        
                                        <fieldset class="rating">                                            
                                            <input type="submit"  id="form_id_{{ $curCourse->id }}_star5" name="rating" value="5" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating == 5 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star5" title="Rocks!"></label>
                                            <input type="submit"  id="form_id_{{ $curCourse->id }}_star4" name="rating" value="4" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 4 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star4" title="Rocks!"></label>
                                            <input type="submit"  id="form_id_{{ $curCourse->id }}_star3" name="rating" value="3" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 3 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star3" title="Rocks!"></label>
                                            <input type="submit"  id="form_id_{{ $curCourse->id }}_star2" name="rating" value="2" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 2 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star2" title="Rocks!"></label>
                                            <input type="submit"  id="form_id_{{ $curCourse->id }}_star1" name="rating" value="1" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 1 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star1" title="Rocks!"></label>
                                        </fieldset>
                                    </form>
                                </div>
                            </article>
                        </div>
                    @endfor                    
                </div>
            </div>
        </div>
    </section>
@endfor

<section class="flat-row pd-85 flat-register">
    <div class="container container-register-form">
        <form action="" method="post" id="formsubscribe" class="">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <p class="p-register">Đăng ký email của bạn để cập nhật thông tin ưu đãi, khuyến mãi, cũng
                        như các thông tin giáo dục mới nhất từ chúng tôi.</p>
                </div>
                <div class="col-md-6 col-xs-12 pd-left-0 pd-right-0">
                    <div class="col-md-8 col-xs-8 pd-r-4"><input type="email" name="email"
                            class="form-control email-input" email"=""
                            required="required">
                    </div>
                    <div class="col-md-4 col-xs-4 pd-l-4 pd-right-0" style="padding-left: 1px;"><button
                            class="btn btn-green form-control pd-right-0" type="submit">Đăng
                            ký</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<section class="flat-row pd-bottom-0">
    <div class="container container-event">
        <div class="row">
            <div class="col-md-12">
                <div class="title-section title-height46">
                    <h1 class="title">Tin tức & Sự kiện</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container container-event">
        <div class="portfolio style2">
            <div class="portfolio-wrap event-div-height">
                @if($newest_news && sizeof($newest_news))
                    @for($i = 0; $i < sizeof($newest_news); $i++)
                        <div class="item pd-right-38">
                            <article class="entry bg-white">
                                <div class="featured-post">
                                    <a href="/tin-tuc/{{ $newest_news[$i]->slugify_url }}"><img class="event-thumbnail-img-wh" src="{{ $newest_news[$i]->banner }}" alt="image"></a>
                                </div>
                                <div class="entry-post">
                                    <h3 class="entry-title event-fz-16"><a href="/tin-tuc/{{ $newest_news[$i]->slugify_url }}">{{ $newest_news[$i]->title }}</a>
                                    </h3>
                                    <div class="entry-content">
                                        <p class="event-content-fz-13">
                                            {{ mb_strlen($newest_news[$i]->summary) > 360 ? mb_substr($newest_news[$i]->summary,0,357)."..." : $newest_news[$i]->summary }}
                                        </p>
                                    </div>
                                </div>
                            </article> 
                        </div>
                    @endfor
                @endif                            
            </div>
        </div>
        <div class="row">
            <div class="dividers h30">
            </div>
        </div>
    </div>
</section>
<section class="flat-row pd-105 flat-none"></section>
<script src="http://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script>
        $(window).on('load',function(){
            $('#notification-modal').modal('show');
        });
    </script>
@endsection
@section('footer_styles')
@endsection