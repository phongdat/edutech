@extends('client/layouts/master')

@section('title', $title)

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/register.css">
@endsection

@section('content')

<div class="wrap-slider">
    <div class="container page-container">
        <div class="page-content">
            <div class="page-title">
                <h1>Đăng ký tài khoản thành viên</h1>
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="/">Trang chủ</a></li>
                        <li>Đăng ký</li>                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="flat-row no-pd-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form action="" method="POST" id="form-register" class="form-register">
                    <div class="form-register-title">
                        <h3 class="register-title">Đăng ký tài khoản</h3>
                    </div>
                    <div class="info-register">
                        <p class="wrap-input-name">
                            <input type="text" id="name" name="name" value="" required="required" placeholder="Họ và tên *:">
                        </p>
                        <p class="wrap-input-email">
                            <input type="text" id="email" name="email" value="" required="required" placeholder="Email *:">
                        </p>
                        <p class="wrap-input-password">
                            <input type="password" id="password" name="password" value="" required="required" placeholder="Mật khẩu *:">
                        </p>
                        <p class="wrap-input-confirm-password">
                            <input type="password" id="confirm-password" name="confirm-password" value="" required="required" placeholder="Xác nhận mật khẩu *:">
                        </p>
                        <p class="wrap-input-address">
                            <input type="text" id="address" name="address" value="" required="required" placeholder="Địa chỉ *:">
                        </p>
                        <p class="wrap-input-phone">
                            <input type="text" id="phone" name="phone" value="" required="required" placeholder="Phone *:">
                        </p>
                        <div class="wrap-btn">
                            <a class="flat-btn" href="javascript:void(0)">Đăng ký</a>
                        </div>
                    </div> 
                </form>
            </div>                    
        </div>
    </div>
</section>

@endsection

@section('footer_styles')

@endsection