@extends('client/layouts/master')

@section('title', $title)

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/account.css">
@endsection

@section('content')
<div class="wrap-slider">
    <div class="container page-container">
        <div class="page-content">
            <div class="page-title">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="javascript:void(0)">TÀI KHOẢN</a></li>                    
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="flat-row pd-bottom-0">
    <div class="container">
        <div class="row mr-bottom-48">
            <div class="col-md-3">
                <ul class="left-list-container">
                    <li class="left-list-item"><a href="/account"><b>Trang chính</b></a></li>
                    <li class="left-list-item"><a href="/danh-gia">Đánh giá</a></li>
                    <li class="left-list-item"> <a href="/lich-su-giao-dich">Lịch sử giao dịch</a></li>
                    <li class="left-list-item"> <a href="/chinh-sua-ho-so">Chỉnh sửa hồ sơ</a></li>
                    <li class="left-list-item"> <a href="/doi-mat-khau">Đổi mật khẩu</a></li>
                    <li class="left-list-item"> <a href="/logout">Đăng xuất</a></li>
                </ul>
            </div>  
            <?php /* Học viên - Type = 1*/?>        
            
            @if($member->member_type_id == 1) 

            <div class="col-md-8 pd-top-20">
                <p>Chào <b>{{ $member->full_name }}</b>,</p>
                <p>Đây là trang thông tin tài khoản của bạn. Bạn có thể sử dụng các chức năng bên dưới hoặc các mục menu bên trái. <br>
                Chúc bạn một ngày tốt lành
                </p>                
                <div class="group-content-container">
                    <h2>Khóa học của bạn</h2>
                    <div class="group-content">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>Stt</td>
                                    <td style="width: 50%">Tên khóa học</td>
                                    <td>Ngày kích hoạt</td>
                                    <td>Ngày hết hạn</td>
                                    <td>Trạng thái</td>
                                </tr>
                            </thead>
                            <tbody>
                                @if($data['checkVip'] == true)

                                    <tr>                                        
                                        <td colspan="5" style="text-align: center;">Tài khoản của bạn của được hỗ trợ học tất cả các khoá học</td>
                                    </tr>

                                @else

                                    @if($data['stydyingCourses'] && sizeof($data['stydyingCourses']) > 0)

                                        @for($i = 0; $i < sizeof($data['stydyingCourses']); $i++)
                                            @php($cur = $data['stydyingCourses'][$i])
                                            <tr>
                                                <td>{{ $i + 1 }}</td>
                                                <th>{{ $cur->course_data->name }}</th>
                                                <td>{{ date('d-m-Y', strtotime($cur->created_at)) }}</td>
                                                <td></td>
                                                <td>Đang học</td>
                                            </tr>
                                        @endfor 

                                    @else

                                        <tr>                                        
                                            <td colspan="5" style="text-align: center;">Bạn chưa đăng ký khoá học nào</td>
                                        </tr>
                                       
                                    @endif

                                @endif                                                                                           
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <div class="group-content-container">
                    <h2>Nạp thẻ</h2>
                    <form action="/rechargeCard" enctype="multipart/form-data" method="post">
                        @csrf  
                        <input type="hidden" name="user_id" value="{{$user->id}}">
                        <div class="group-content row">
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="serial_number" placeholder="Số serial trên thẻ">
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="code" placeholder="Mã kích hoạt">
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary btn-block card-btn">NẠP THẺ</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="group-content-container">
                    <h2>Điểm tích lũy</h2>
                    <div class="group-content">
                        <p>Điểm tích lũy của bạn là: <font style="font-size: 30px; color: #135091; font-weight: bold">{!! $data['points'] !!}</font> điểm</p>
                        <p>Điểm tích lũy dùng để tham dự các chương trình khuyến mại hoặc đổi quà, thanh toán chi phí học...</p>
                        <p>
                            Để biết chi tiết quá trình tích lũy cũng như sử dụng điểm của bạn, bạn hãy <a href="/lich-su-giao-dich">bấm vào đây</a> hoặc chọn <br>
                            mục <a href="/lich-su-giao-dich">lịch sử giao dịch</a> trên menu trái
                        </p>
                    </div>
                </div>
            </div>

            @else

            <div class="col-md-8 pd-top-20 content">
                <p>Chào <b>{{ $member->full_name }}</b>,</p>
                <p>Thẻ của bạn:</p>
                <div class="group-content-container">
                    <div class="group-content">
                        <table class="table table-account">
                            <thead>
                                <tr>                                    
                                    <th class="col-md-2">Series</th>
                                    <th class="col-md-3">Ngày nhận</th>                                    
                                    <th class="col-md-3"> Số lô</th>                                  
                                </tr>
                            </thead>
                            <tbody>
                                @if(sizeof($data['cards']) > 0)

                                    @foreach( $data['cards'] as $card)
                                        <tr>                                            
                                            <td class="col-md-4">{{ $card->card_data->series }}</td>
                                            <td class="col-md-4">{{ date('d-m-Y', strtotime($card->created_at)) }}</td>                                      
                                            <td class="col-md-4">{{ $card->card_data->group_series }}</td>                   
                                        </tr>                                                                                          
                                    @endforeach
                                    <tr class="border-paid">
                                        <th colspan="2">Thanh toán</th>                                        
                                        <td class="float-right">
                                            <form class="form" action="/account/paid" method="post">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="member_id" value="{{ $member->id }}">    
                                                <input type="hidden" name="card_id" value="{{ $card->card_id }}">                                                                                          
                                                <button type="submit" class="btn btn-danger">Thanh toán</button>
                                            </form>    
                                        </td>
                                    </tr>
                                    <tr class="border-transfer">
                                        <th colspan="2">Chuyển thẻ</th>                                        
                                        <td class="float-right">
                                            <a href="/transfer">
                                                <button type="submit" class="btn btn-warning">Chuyển thẻ</button>
                                            </a>    
                                        </td>
                                    </tr>
                                @else

                                    <tr>                                        
                                        <td colspan="3" style="text-align: center;">Không có thẻ để hiển thị</td>
                                    </tr>

                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</section>
<section class="flat-row pd-top-0">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="summary-course">
                    <h2 class="summary-course__h2">Khoá học khác</h2>
                </div>
            </div>
            <div class="col-md-9">
                @for($j = 0; $j < count($newest_data); $j++)
                    @php ($curCourse = $newest_data[$j])
                    @if($j == 0)
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif

                    @if($j != 0 && $j % 3 == 0)
                            </div>
                        </div>
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif
                    
                    @if($j > 2)
                        <div class="item course mr-bottom-0">
                    @else
                        <div class="item course">
                    @endif                    
                        <article class="entry  article-course--height">
                            <div class="featured-post">
                                <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                    @if (file_exists(public_path($curCourse->detail->banner)))
                                        <img src="{!! $curCourse->detail->banner !!}" alt="image">
                                    @else
                                        <img src="/client_assets/image/noimage.jpg" alt="image">
                                    @endif                                        
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="entry-post">
                                        <h3 class="entry-title">
                                            <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                                {{ mb_strlen($curCourse->name) > 16 ? mb_substr($curCourse->name,0,16)."..." : $curCourse->name }}
                                            </a>
                                        </h3>
                                        <div class="entry-author">
                                            <span><a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">{{ $curCourse->detail->teacher_name }}</a></span>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="row">
                            <form action="/like-course" method="POST">
                                <input id="form_id_{{ $curCourse->id }}" type="hidden" name="courseId" value="{{ $curCourse->id }}">                                        
                                {{ csrf_field() }}                                        
                                <fieldset class="rating">                                            
                                    <input type="submit"  id="form_id_{{ $curCourse->id }}_star5" name="rating" value="5" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating == 5 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star5" title="Rocks!"></label>
                                    <input type="submit"  id="form_id_{{ $curCourse->id }}_star4" name="rating" value="4" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 4 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star4" title="Rocks!"></label>
                                    <input type="submit"  id="form_id_{{ $curCourse->id }}_star3" name="rating" value="3" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 3 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star3" title="Rocks!"></label>
                                    <input type="submit"  id="form_id_{{ $curCourse->id }}_star2" name="rating" value="2" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 2 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star2" title="Rocks!"></label>
                                    <input type="submit"  id="form_id_{{ $curCourse->id }}_star1" name="rating" value="1" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 1 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star1" title="Rocks!"></label>
                                </fieldset>
                            </form>
                        </div>
                        </article>
                    </div>
                @endfor                    
            </div>
        </div>
    </div>
</section>
<section class="flat-row pd-90 flat-none"></section>
@endsection

@section('footer_styles')

@endsection