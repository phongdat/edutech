@extends('client/layouts/master')

@section('title', $title)

@section('header_styles')
<link rel="stylesheet" type="text/css" href="/client_assets/css/page/account.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')

<div class="wrap-slider">
    <div class="container page-container">
        <div class="page-content">
            <div class="page-title">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="javascript:void(0)">CHUYỂN THẺ</a></li>                    
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="flat-row pd-bottom-0">
    <div class="container">
        <div class="row mr-bottom-48">
            <div class="col-md-3">
                <ul class="left-list-container">
                    <li class="left-list-item"><a href="/account">Trang chính</a></li>
                    <li class="left-list-item active"> <a href="/lich-su-giao-dich">Lịch sử giao dịch</a></li>
                    <li class="left-list-item"> <a href="/chinh-sua-ho-so">Chỉnh sửa hồ sơ</a></li>
                    <li class="left-list-item"> <a href="/doi-mat-khau">Đổi mật khẩu</a></li>
                    <li class="left-list-item"> <a href="/logout">Đăng xuất</a></li>
                </ul>
            </div>
            <div class="col-md-8 pd-top-20">                           
                <div class="group-content-container">
                    
                    <div class="group-content">
                        <form action="/saveTransfer" method="POST">
                        @csrf  
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="member_id" value="{{ $member_id }}">                                                                                          
                        <div class="form-group row">

                        <div class="form-group row">                           
                            <div class="form-group row">
                                <h2>Chuyển thẻ</h2>
                                <br>
                                <label for="transfer-card" class="col-md-4 col-form-label text-md-right">Số lượng</label>
                                <div class="col-md-7">
                                    <input type="number" min='10' class="form-control form-control-lg input-lg"
                                    name="quantity" max="{{ $max }}" id="prod_quantity"
                                    placeholder="Số lượng thẻ cần tạo" value="10" step="10">
                                </div>

                            </div>
                            <div class="form-group row">
                                <label for="transfer-card" class="col-md-4 col-form-label text-md-right">Người nhận</label>
                                <div class="col-md-7">
                                    <select name="sl_member" class="js-example-basic-single form-control" name="state">
                                        @for( $i = 0; $i < sizeof($member_types); $i++ )
                                            <optgroup label="{{ $member_types[$i]->name }}">        
                                                @if(isset( $groupMembers[ $member_types[$i]->id ]) )
                                                    @for( $j = 0; $j < sizeof($groupMembers[$member_types[$i]->id]); $j++ )
                                                        @php($cur = $groupMembers[$member_types[$i]->id][$j])
                                                        @if($member_id != $cur->id)
                                                            <option value="{!! $cur->id !!}" >{!! $cur->full_name !!}</option>
                                                        @endif
                                                    @endfor
                                                @endif
                                            </optgroup>
                                        @endfor                                                                          
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group row">
                            <label for="paid" class="col-md-4 col-form-label text-md-right"></label>
                            <div class="col-md-7">                                
                                <input type="checkbox" name="chk-paid" value="paid"> &nbsp; Đã thanh toán<br>
                            </div>
                        </div>                                                                                      
                        <div class="row control-group mr-top-20">
                            <label class="col-md-4"></label>			      
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-login-page">Chuyển thẻ</button>
                            </div>
                        </div>                                               
                    </form>
                    </div>
                </div>                                
            </div>
        </div>
    </div>
</section>

<section class="flat-row pd-top-0">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="summary-course">
                    <h2 class="summary-course__h2">Khoá học khác</h2>
                </div>
            </div>
            <div class="col-md-9">
                @for($j = 0; $j < count($newest_data); $j++)
                    @php ($curCourse = $newest_data[$j])
                    @if($j == 0)
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif

                    @if($j != 0 && $j % 3 == 0)
                            </div>
                        </div>
                        <div class="portfolio style2">
                            <div class="portfolio-wrap clearfix">   
                    @endif
                    
                    @if($j > 2)
                        <div class="item course mr-bottom-0">
                    @else
                        <div class="item course">
                    @endif                    
                        <article class="entry  article-course--height">
                            <div class="featured-post">
                                <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                    @if (file_exists(public_path($curCourse->detail->banner)))
                                        <img src="{!! $curCourse->detail->banner !!}" alt="image">
                                    @else
                                        <img src="/client_assets/image/noimage.jpg" alt="image">
                                    @endif                                        
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="entry-post">
                                        <h3 class="entry-title">
                                            <a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">
                                                {{ mb_strlen($curCourse->name) > 16 ? mb_substr($curCourse->name,0,16)."..." : $curCourse->name }}
                                            </a>
                                        </h3>
                                        <div class="entry-author">
                                            <span><a href="/hoc-online/{{ $curCourse->parentSlugify }}/{{ $curCourse->slugify }}">{{ $curCourse->detail->teacher_name }}</a></span>
                                        </div>
                                    </div>
                                </div>                               
                            </div>
                            <div class="row">
                                <form action="/like-course" method="POST">
                                    <input id="form_id_{{ $curCourse->id }}" type="hidden" name="courseId" value="{{ $curCourse->id }}">                                        
                                    {{ csrf_field() }}                                        
                                    <fieldset class="rating">                                            
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star5" name="rating" value="5" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating == 5 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star5" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star4" name="rating" value="4" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 4 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star4" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star3" name="rating" value="3" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 3 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star3" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star2" name="rating" value="2" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 2 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star2" title="Rocks!"></label>
                                        <input type="submit"  id="form_id_{{ $curCourse->id }}_star1" name="rating" value="1" /><label class="{{ isset( $curCourse->rating ) && $curCourse->rating >= 1 ? 'rating-yellow' : '' }}" for="form_id_{{ $curCourse->id }}_star1" title="Rocks!"></label>
                                    </fieldset>
                                </form>
                            </div>
                        </article>
                    </div>
                @endfor                    
            </div>
        </div>
    </div>
</section>
<section class="flat-row pd-90 flat-none"></section>

@endsection

@section('footer_styles')

@endsection